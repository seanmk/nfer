##############################################################################################
#
#       !!!! Do NOT edit this makefile with an editor which replace tabs by spaces !!!!    
#
##############################################################################################
# 
# On command line:
#
# make all = Create project
#
# make clean = Clean project files.
#
# To rebuild project do "make clean" and "make all".
#
#     nfer - a system for inferring abstractions of event streams
#    Copyright (C) 2017  Sean Kauffman
#
#    This file is part of nfer.
#    nfer is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
##############################################################################################
#

# detect the os
ifeq ($(OS),Windows_NT)
	OPSYS = WIN32
else
	UNAME_S := $(shell uname -s)
	ifeq ($(UNAME_S),Linux)
		OPSYS = LINUX
	endif
	ifeq ($(UNAME_S),Darwin)
		OPSYS = OSX
	endif
	ifeq ($(UNAME_S),SunOS)
		OPSYS = SOLARIS
	endif
endif

XXD	= xxd
LZ4	= lz4
SED	= sed


# Define project name here
PROJECT = nfer
# binary output dir
BINDIR = bin

# source directories
SRCDIR = src
TESTSRCDIR = test/src
SUB_SRCDIRS = dsl dsl/semantics rdc compiler

# these are used for the VPATH definitions
SRC_SUB_SRCDIRS = $(patsubst %,$(SRCDIR)/%,$(SUB_SRCDIRS))
SPACE := $(subst ,, )

ifeq ($(OPSYS),WIN32)
	# mingw on windows doesn't set a symlink to cc
    CC = gcc
	# also, windows uses ; to separate VPATH not :
	VPATH = $(SRCDIR);$(TESTSRCDIR);$(subst $(SPACE),;,$(SRC_SUB_SRCDIRS))
else
	# on non-windows systems, though, go ahead and just use the system C compiler
	CC = cc
	# set VPATH - where make should look for source files
	VPATH = $(SRCDIR):$(TESTSRCDIR):$(subst $(SPACE),:,$(SRC_SUB_SRCDIRS))
endif

# Files that only go in the main binary
# decompress is in the rdc subdirectory
MAIN = main.c \
	   compile.c \
	   lz4.c

# Files for the DSL - separating to have a little more visibility
DSL_SRC = ast.c \
          astutil.c \
		  generate.c \
		  static.c \
		  imports.c \
		  typecheck.c \
		  labels.c \
		  fields.c \
		  literals.c \
		  constants.c

# List C source files here
SRC  = types.c \
       log.c \
       nfer.c \
       dict.c \
       learn.c \
       pool.c \
       file.c \
       map.c \
       analysis.c \
       stack.c \
       expression.c \
       memory.c \
       strings.c \
       debug.c \
	   $(DSL_SRC)

# List all user directories here
UINCDIR = inc
# List the directory to look for the libraries here
ULIBDIR =

# Lexer and Parser source
LEXER     = dsl.l
PARSER    = dsl.y

# List all test source files here
TESTSRC = test.c \
          dsl_test_helper.c \
          testio.c \
          test_dict.c \
          test_pool.c \
          test_map.c \
          test_stack.c \
          test_nfer.c \
          test_expression.c \
          test_memory.c \
          test_strings.c \
          test_semantic.c \
          test_types.c \
          test_ast.c \
		  test_file.c \
          test_learn.c \
		  test_static.c \
		  test_analysis.c \
		  test_constants.c \
		  test_lz4.c \
		  lz4.c  # because it isn't included in the core source!


# List the test include directory
TESTINCDIR = test/inc
# List the directory to look for the libraries here
TESTLIBDIR = 
# functional test runner
FTEST = test/run_functional.sh
# compiler functional test runner
CFTEST = test/run_c_functional.sh

# List the r source file(s) and package dir
RSRC    = rinterface.c
RPACK   = R

# this is how we run the R tests
RCHECK  = check.R

# List the source files used for the compiler output
# these files need to be preprocessed to include in the program
COMPILERSRC = types.c \
              memory.c \
              strings.c \
              dict.c \
              map.c \
              stack.c \
              pool.c \
              expression.c \
              nfer.c \
              file.c \
              log.c \
              target/linux.c

COMPILERINC = types.h \
              memory.h \
              strings.h \
              dict.h \
              map.h \
              stack.h \
              pool.h \
              expression.h \
              file.h \
              log.h \
              nfer.h

# goes at the top, doesn't get comments stripped
TARGETINC   = target.h

# target for the headers and source code
HEADERSFILE = headers.h
SRCCODEFILE = srccode.c

# List all libraries here
LIBS = 

# Define optimisation levels here -O0 -O1 -O2 -Os -03
OPT      = -O3
TESTOPT  = -O0

# Define the build directories
BUILDDIR   = build
MAINBUILD  = $(BUILDDIR)/main
TESTBUILD  = $(BUILDDIR)/test
RBUILD     = $(BUILDDIR)/R
# Define the directories for code generation
CODEGENDIR = gensrc
PARSBUILD  = $(CODEGENDIR)/parser
COMPBUILD  = $(CODEGENDIR)/compiler
# Define any directories needed for the R package
RSRCDIR    = $(RBUILD)/src

#
# End of user defines
##############################################################################################


INCDIR     = $(patsubst %,-I%,$(UINCDIR) $(PARSBUILD))
TESTINC    = $(patsubst %,-I%,$(TESTINCDIR))

LIBDIR     = $(patsubst %,-L%,$(ULIBDIR))
TESTLIBS   = $(patsubst %,-L%,$(TESTLIBDIR))

OBJS       = $(SRC:.c=.o)
MAINOBJS   = $(MAIN:.c=.o)
TESTOBJS   = $(TESTSRC:.c=.o)
# we need a list of the headers for the R package...
INC        = $(notdir $(wildcard $(UINCDIR)/*.h)) dsl.h

LEXERSRC   = $(LEXER:.l=.yy.c)
PARSERSRC  = $(PARSER:.y=.tab.c)
PARSERINC  = $(PARSER:.y=.tab.h)
LPSRC      = $(patsubst %,$(PARSBUILD)/%,$(PARSERSRC) $(LEXERSRC))
LPOBJS     = $(PARSERSRC:.c=.o) $(LEXERSRC:.c=.o)
LPINC      = $(patsubst %,$(PARSBUILD)/%,$(PARSERINC))

SRCCODE    = $(patsubst %,$(COMPBUILD)/%,$(COMPILERSRC))
SRCCODEBLD = $(patsubst %,$(COMPBUILD)/%,$(SRCCODEFILE))
SRCCODESRC = $(SRCCODEBLD:.c=.array.c)
SRCCODEOBJ = $(SRCCODESRC:.c=.o)

HEADERS    = $(patsubst %,$(COMPBUILD)/%,$(COMPILERINC))
HEADERSBLD = $(patsubst %,$(COMPBUILD)/%,$(HEADERSFILE))
HEADERSSRC = $(HEADERSBLD:.h=.array.c)
HEADERSOBJ = $(HEADERSSRC:.c=.o)

RSRCFILES  = $(patsubst %,$(RSRCDIR)/%,$(PARSERSRC) $(LEXERSRC) $(RSRC) $(SRC))
RINCFILES  = $(patsubst %,$(RSRCDIR)/%,$(PARSERINC) $(INC))

MAINO      = $(patsubst %,$(MAINBUILD)/%,$(LPOBJS) $(MAINOBJS) $(OBJS))
TESTO      = $(patsubst %,$(TESTBUILD)/%,$(LPOBJS) $(TESTOBJS) $(OBJS))

TARGET     = $(BINDIR)/$(PROJECT)
TESTTARGET = $(BINDIR)/$(PROJECT)-test

# remove some gcc specific flags for Solaris where we might expect ODS
# also remove linker optimization options that ODS can't use for a dynamic binary
# under windows we also don't want the -Wno-unused-command-line-argument which isn't needed as we specify gcc
ifeq ($(OPSYS),SOLARIS)
	CPFLAGS = -Wall -Wextra -Wpedantic -Wno-format -std=c99 -mtune=native -Werror=implicit-function-declaration
	LDFLAGS = $(LIBDIR) $(LIBS) -Wall -Wno-format -pedantic -mtune=native -Werror=implicit-function-declaration
else ifeq ($(OPSYS),WIN32)
	CPFLAGS = -Wall -Wextra -Wpedantic -Wno-format -std=c99 -mtune=native -fexceptions -Werror=implicit-function-declaration -pipe
	LDFLAGS = $(LIBDIR) $(LIBS) -Wall -Wno-format -pedantic -mtune=native -fexceptions -Werror=implicit-function-declaration
else
	CPFLAGS = -Wall -Wextra -Wpedantic -Wno-unused-command-line-argument -Wno-format -std=c99 -mtune=native -fexceptions -fstack-protector-strong -Werror=implicit-function-declaration -pipe
	LDFLAGS = $(LIBDIR) $(LIBS) -Wall -Wno-format -pedantic -mtune=native -fexceptions -fstack-protector-strong -Werror=implicit-function-declaration
endif

# Generate dependency information
CPFLAGS += -MD -MP -MF .dep/$(@F).d

# Set separate flags for the test binary
# only enable test coverage on linux - xcode removed gcov support...
ifeq ($(OPSYS),LINUX)
	TESTCPFLAGS = $(CPFLAGS) -fverbose-asm -no-pie -fprofile-arcs -ftest-coverage
	TESTLDFLAGS = $(LDFLAGS) -lgcov --coverage
else
	TESTCPFLAGS = $(CPFLAGS) -fverbose-asm -no-pie
	TESTLDFLAGS = $(LDFLAGS)
endif

# Set the binary names of flex and bison for windows separately
ifeq ($(OPSYS),WIN32)
    FLEXBIN = win_flex
	BISONBIN = win_bison
	FIXFORR = py $(RPACK)/fix-for-R.py
else
	FLEXBIN = flex
	BISONBIN = bison
	# this is a script we call to fix a problem reported by R CMD check
	FIXFORR = $(RPACK)/fix-for-R.py
endif

# 
# look for build dependencies
# 
ISXXD := $(shell which $(XXD))
ISSED := $(shell which $(SED))
ISLZ4 := $(shell which $(LZ4))
ISFLEX := $(shell which $(FLEXBIN))
ISBISON := $(shell which $(BISONBIN))


#
# makefile rules
#

.DEFAULT: default
.PHONY: default
default : $(TARGET)

.PHONY: all
all : $(TARGET) $(TESTTARGET)

# 
# rules for build directories
#
$(BUILDDIR) :
	mkdir -p $(BUILDDIR)

$(MAINBUILD) :
	mkdir -p $(BINDIR)
	mkdir -p $(MAINBUILD)
	
$(TESTBUILD) :
	mkdir -p $(BINDIR)
	mkdir -p $(TESTBUILD)
	
$(RBUILD) : $(BUILDDIR)
	cp -r $(RPACK) $(RBUILD)

$(RSRCDIR) : $(RBUILD)

$(PARSBUILD) :
	mkdir -p $(PARSBUILD)

$(COMPBUILD) :
	mkdir -p $(COMPBUILD)
	mkdir -p $(COMPBUILD)/target

.PHONY: unit-test
unit-test: $(TESTTARGET)
	./$(TESTTARGET)

.PHONY: functional-test
functional-test: $(TARGET)
	./$(FTEST) ./$(TARGET)

.PHONY: functional-c-test
functional-c-test: $(TARGET)
	./$(CFTEST) ./$(TARGET)

.PHONY: test
test : unit-test functional-test functional-c-test


#############################################################
# Rules for building the R package
#############################################################

# this is to build the whole directory for the R package
.PHONY: rpackage
rpackage : $(RSRCFILES) $(RINCFILES)

# r test
.PHONY: rtest
rtest : rpackage
	cd $(RBUILD); ./$(RCHECK)
	
# copy over the parser files (and hack up the lexer source)
$(RSRCDIR)/$(LEXERSRC) : $(PARSBUILD)/$(LEXERSRC) $(RSRCDIR)
	cp $< $@
	$(FIXFORR) $@

$(RSRCDIR)/$(PARSERSRC) : $(PARSBUILD)/$(PARSERSRC) $(RSRCDIR)
	cp $< $@
	$(FIXFORR) $@

$(RSRCDIR)/$(PARSERINC) : $(PARSBUILD)/$(PARSERINC) $(RSRCDIR)
	cp $< $@
	$(FIXFORR) $@

# otherwise get source files from src
$(RSRCDIR)/%.c : %.c $(RSRCDIR)
	cp $< $@

# and header files from inc
$(RSRCDIR)/%.h : $(UINCDIR)/%.h $(RSRCDIR)
	cp $< $@

#############################################################
# End of Rules for building the R package
#############################################################

# these lines are here to ensure that the build directories exist
$(MAINO)      : | $(MAINBUILD)
$(TESTO)      : | $(TESTBUILD)
$(LPSRC)      : | $(PARSBUILD)
$(SRCCODE)    : | $(COMPBUILD)
$(HEADERS)    : | $(COMPBUILD)

# generate the lexer code using flex
$(PARSBUILD)/$(LEXERSRC) : $(LEXER)
ifndef ISFLEX
	$(error "Could not find build dependency $(FLEXBIN).")
endif
	$(FLEXBIN) -o $@ $<

# generate the parser code using bison
$(PARSBUILD)/$(PARSERSRC) : $(PARSER)
ifndef ISBISON
	$(error "Could not find build dependency $(BISONBIN).")
endif
	$(BISONBIN) -d -o $@ $<

# this tries to use sed to strip #includes from source files
$(COMPBUILD)/%.c : %.c
ifndef ISSED
	$(error "Could not find build dependency $(SED).")
endif
	$(SED) '/^[[:space:]]*#include/d' $< > $@

# this is also for the compiler, concatenating source files together into one file
$(SRCCODEBLD) : $(SRCCODE)
	cat $^ > $@

# this uses lz4 to compress the source and then xxd to generate a C file from the compressed version
$(SRCCODESRC) : $(SRCCODEBLD)
ifndef ISLZ4
	$(error "Could not find build dependency $(LZ4).")
endif
	$(LZ4) -f -z -9 $< $<.lz4
ifndef ISXXD
	$(error "Could not find build dependency $(XXD).")
endif
	$(XXD) -i $<.lz4 $@

# this tries to use sed to strip #includes from headers
$(COMPBUILD)/%.h : $(UINCDIR)/%.h
ifndef ISSED
	$(error "Could not find build dependency $(SED).")
endif
	$(SED) '/^[[:space:]]*#include/d' $< > $@

# this is also for the compiler, concatenating header files together into one file
$(HEADERSBLD) : $(UINCDIR)/$(TARGETINC) $(HEADERS)
	cat $^ > $@

# this uses rdc to compress the source and then xxd to generate a C file from the compressed version
$(HEADERSSRC) : $(HEADERSBLD)
ifndef ISLZ4
	$(error "Could not find build dependency $(LZ4).")
endif
	$(LZ4) -f -z -9 $< $<.lz4
ifndef ISXXD
	$(error "Could not find build dependency $(XXD).")
endif
	$(XXD) -i $<.lz4 $@

# this is to compile the xxd generated source for the compiler
$(COMPBUILD)/%.o : $(COMPBUILD)/%.c
	$(CC) -c -gdwarf-2 -DNDEBUG $(CPFLAGS) $< -o $@

# this is the main source compilation for the nfer binary
$(MAINBUILD)/%.o : %.c
	$(CC) -c -gdwarf-2 -DCOMPILER -DNDEBUG $(CPFLAGS) -I . $(INCDIR) $< -o $@

# this is to compile the flex/bison generated source for the nfer binary
$(MAINBUILD)/%.o : $(PARSBUILD)/%.c
	$(CC) -c -gdwarf-2 -DCOMPILER -DNDEBUG $(CPFLAGS) -Wno-unused-function -I . $(INCDIR) $< -o $@

# this is the main source compilation for the nfer-test unit test binary
$(TESTBUILD)/%.o : %.c
	$(CC) -c -gdwarf-2 -DTEST $(TESTCPFLAGS) -I . $(INCDIR) $(TESTINC) $< -o $@

# this is to compile the flex/bison generated source for the nfer-test unit test binary
$(TESTBUILD)/%.o : $(PARSBUILD)/%.c
	$(CC) -c -gdwarf-2 -DTEST $(TESTCPFLAGS) -Wno-unused-function -I . $(INCDIR) $< -o $@


$(TARGET) : CPFLAGS += $(OPT)
$(TARGET) : $(MAINO) $(SRCCODEOBJ) $(HEADERSOBJ)
	$(CC) $^ $(LDFLAGS) $(OPT) -o $@

$(TESTTARGET) : CPFLAGS += $(TESTOPT)
$(TESTTARGET) : $(TESTO)
	$(CC) $^ $(TESTLDFLAGS) $(TESTOPT) $(TESTLIBS) -o $@

.PHONY: clean
clean:
	-rm -rf $(BUILDDIR)
	-rm -rf $(CODEGENDIR)
	-rm -rf $(BINDIR)
	-rm -f *.dot
	-rm -fR .dep
	# remove artifacts from python builds too
	-rm -rf NferModule.egg-info
	-rm -rf dist
	-rm -rf wheelhouse

# 
# Include the dependency files, should be the last of the makefile
#
-include $(shell mkdir .dep 2>/dev/null) $(wildcard .dep/*)

# *** EOF ***