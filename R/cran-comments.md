## R CMD check results

There were no ERRORs.  There were no WARNINGs on most tested systems.

There were two WARNINGs that appeared on r-devel that were caused by over-aggressive static analysis.

 * On r-devel, I saw a WARNING that 'exit' and 'stderr' symbols are defined in dsl.yy.o but these symbols are in a static function that is unreferenced and will be removed in any optized compilation.  The static function in question, yy_fatal_error, is defined but unreferenced because it is part of Bison-generated parsing code.
   
 I am following best practices for Bison to ensure that yy_fatal_error is not called but there is no way to configure Bison to not generate the code.  I could write a script to strip the offending code but this feels like a brittle solution to a non-problem.

## Downstream dependencies

There are currently no downstream dependencies for this package.