#!/usr/bin/env python3

# remove two lines from the Flex output that cause problems with
# R CMD check:
#              if ( ! yyout )
#                      yyout = stdout;
# Also, remove #line directives from before #include directives
# these cause problems with the Oracle compiler on Solaris


from argparse import ArgumentParser
from pathlib import Path
from re import match
from shutil import move

# remove line directives before #include directives
def trim_line_directives(lines):
    last_include = 0
    result = []

    # iterate first to find the last #include
    for lnum in range(len(lines)):
        line = lines[lnum]
        if match(r".*#include", line):
            last_include = lnum

    # then remove any #line before that
    for lnum in range(len(lines)):
        line = lines[lnum]
        if lnum <= last_include:
            if match(r".*#line", line):
                continue
        result.append(line)
    return result

# get rid of specific lines that reference stdout
def trim_stdout(lines):
    lastline = None
    result = []

    for line in lines:
        if lastline is not None:
            if match(r".*if \( ! yyout \)", lastline):
                if match(r".*yyout = stdout;", line):
                    lastline = None
                    continue
        
        if lastline is not None:
            result.append(lastline)
        lastline = line
    
    if lastline is not None:
        result.append(lastline)
    return result

def main() -> None:
    parser = ArgumentParser(description="Remove lines from dsl.yy.c that cause problems with R CMD check.")
    parser.add_argument("cfile", type=Path)
    args = parser.parse_args()

    oldfilename = str(args.cfile)
    newfilename = str(args.cfile) + ".new"

    with open(newfilename, "w") as newfile:
        with open(oldfilename, "r") as oldfile:
            lines = oldfile.readlines()
            no_stdout = trim_stdout(lines)
            no_lines = trim_line_directives(no_stdout)
            newfile.writelines(no_lines)

    move(newfilename, oldfilename)


if __name__ == "__main__":
    main()
