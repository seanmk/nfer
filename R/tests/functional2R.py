#!/usr/bin/env python3

# This script generates test_that R tests from nfer functional tests.
# Each test reads a spec and an input, then checks the output.

# The inputs to the script are the functional test directory and test name.
# The output to the script is the test_that test.

# (for now, anyway)

from argparse import ArgumentParser
from pathlib import Path
from glob import glob
import os
from shutil import copyfile


def get_value_string(map_value):
    try:
        # try to parse it as an int
        int(map_value)
        # then try a float
        float(map_value)
        # if these both pass, just return the value
        return map_value
    except ValueError:
        # failure, it's a string, wrap in quotes
        return f'"{map_value}"'

def generate_test_that(path, testname):
    output = ""

    with open(path / f"{testname}.result", "r") as result:
        rows = result.readlines()

        output += f'test_that("nfer correctly handles {testname}", {{\n'
        output += f'  spec   <- nfer::load(system.file("extdata", "{testname}.nfer", package = "nfer"))\n'
        output += f'  input  <- nfer::read(system.file("extdata", "{testname}.events", package = "nfer"))\n'
        output += f'  result <- nfer::apply(spec, input)\n'

        for rownum in range(len(rows)):
            interval = rows[rownum].strip()
            columns = interval.split("|")
            # get the row from the result dataframe
            output += f'  row <- result[{rownum + 1},]\n'
            output += f'  expect_equal(row$name, "{columns[0]}")\n'
            output += f'  expect_equal(row$start, {columns[1]})\n'
            output += f'  expect_equal(row$end, {columns[2]})\n'
            if len(columns) == 5:
                keys   = columns[3].split(';')
                values = columns[4].split(';')
                for keynum in range(len(keys)):
                    output += f'  expect_equal(row${keys[keynum]}, "{values[keynum]}")\n'

        output += '})\n'
        return output

def write_test(testdir, testname, copy_files):
    specname = f'{testname}.nfer'
    specpath = testdir / f'{specname}'

    print(generate_test_that(testdir, testname))

    if copy_files is not None:
        # copy the spec file to the extdata directory
        copyfile(specpath, copy_files / specname)
        copyfile(testdir / f'{testname}.events', copy_files / f'{testname}.events')


def main() -> None:
    parser = ArgumentParser(description="Output an R test_that test for the nfer functional tests.")
    parser.add_argument("testdir", type=Path, help="Where the functional tests are located")
    parser.add_argument("-t", "--testname", type=str, required=False, help="Name of a single test to copy")
    parser.add_argument("-c", "--copy-files", type=Path, required=False, help="Where to copy test files")
    args = parser.parse_args()

    if args.testname is not None:
        write_test(args.testdir, args.testname, args.copy_files)

    else:
        for specpath in glob(str(args.testdir / "*.nfer")): 
            specname = os.path.basename(specpath)
            testname = os.path.splitext(specname)[0]

            write_test(args.testdir, testname, args.copy_files)

if __name__ == "__main__":
    main()
