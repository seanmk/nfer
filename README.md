[![Build Status](https://travis-ci.com/seanmk/nfer.svg?branch=master)](https://travis-ci.com/bitbucket/seanmk/nfer) [![Coverage Status](https://coveralls.io/repos/bitbucket/seanmk/nfer/badge.svg?branch=master)](https://coveralls.io/bitbucket/seanmk/nfer?branch=master)

What is nfer?
=============================================================================
Nfer is a tool that implements the eponymous language.  The tool takes an event stream as an input, such as a program log, and applies a *specification* to the stream to calculate new information in the form of *intervals*.  In nfer, an interval refers to a piece of data with a label (also called an identifier or name) and two timestamps indicating when it began and ended.  Intervals can also carry data, such as integers and strings, in a map.  Events in the input are also considered intervals where the begin and end timestamps are identical.  An nfer specification consists of *rules* that each relate (usually) two intervals to one another.  When two intervals match, a new interval is produced using information from the rule to determine the label, timestamps, and data.

Nfer was developed in collaboration between researchers from the NASA Jet Propulsion Laboratory in the USA and the University of Waterloo in Canada.  It was designed to help operators of the Mars Science Laboratory (Curiosity rover) and other spacecraft better understand these remote systems.  Nfer creates abstractions of event streams, meaning the information produced by applying nfer rules is easier to comprehend than directly accessing the event stream.  Nfer abstractions form a hierarchy of temporal intervals: they can be built up from smaller pieces to find meaningful information.  Applying an nfer specification can also be understood as adding prior knowledge to a trace, transforming it into a more easily analyzed representation.

Citing nfer
=============================================================================
If you use nfer in your project or research, please cite one or both of the following:

> Sean Kauffman, Klaus Havelund, and Rajeev Joshi. nfer -- a notation and system for inferring event stream abstractions. In International Conference on Runtime Verification, pages 235 - 250. Springer, 2016.

> Sean Kauffman, Klaus Havelund, Rajeev Joshi, and Sebastian Fischmeister. Inferring event stream abstractions. Formal Methods in System Design, pages 54 - 82. Springer, Aug 1, 2018.

If you use the R or Python interfaces, please cite this additional work:

> Sean Kauffman. nfer -- A Tool for Event Stream Abstraction In International Conference on Software Engineering and Formal Methods, pages 103 - 109. Springer, 2021.

If you use the nfer rule mining capability, please cite this additional work:

> Sean Kauffman and Sebastian Fischmeister. Mining temporal intervals from real-time system traces. In 6th International Workshop on Software Mining, pages 1-8. IEEE, 2017.

Installing nfer
=============================================================================
There are four interfaces through which you can use nfer:

 * *Command line*
   To install the command line nfer tool, you must build it from source.
   See the Building section below for Linux or the [build instructions by OS](doc/build.md).  Then refer to the [command line interface documentation](doc/shell.md).  See the [event trace documentation](doc/events.md) for how to format event traces for this tool.
 * *Compiled monitor*
   To compile a monitor, first build the command-line interface and use it to generate the monitor source.
   See the [compiling a monitor](doc/compile.md) documentation for details.
 * *R package*
   The nfer R package is available on [CRAN](https://cran.r-project.org/).  
   To install it run `install.packages("nfer")` from within R. For information on the R language API, install the nfer R package and see included documentation with `browseVignettes(package="nfer")`.
 * *Python module*
   The nfer Python module is available on [PyPi](https://pypi.org/).
   To install it, run `python -m pip install NferModule`. For information on the Python language API, see [the Python documentation](python/README.md). For details on the low-level Python API (not recommended for user), see [the Low-Level Python API documentation](doc/pythonapi.md).

Writing Specifications
=============================================================================
For information on the nfer language and how to write specifications, see the [nfer language documentation](doc/nfer.md).


Building the Command Line Interface
=============================================================================
Nfer is not distributed in binary packages as this would require more maintenance time than is presently available.  One must build nfer, but this is very simple due to its relative lack of dependencies.

Nfer builds on (at least) Linux, OS X, Windows, Solaris, and FreeBSD. Below are the Linux build instructions.  For other operating systems, see the [build instructions By OS](doc/build.md). All instructions assume that you have [installed **git**](http://git-scm.com/download/linux) and cloned the nfer source code repository:

```bash
git clone https://bitbucket.org/seanmk/nfer.git
```

Linux Build Instructions
------------------------
There are a small number of compile time dependencies.  These are all available on most Linux distributions out-of-the-box.

* [make](https://www.gnu.org/software/make/) 
* [gcc](https://gcc.gnu.org/)
* [flex](https://github.com/westes/flex)
* [bison](https://www.gnu.org/software/bison/)
* [lz4](http://lz4.github.io/lz4/)
* [xxd](https://www.systutorials.com/docs/linux/man/1-xxd/)
  *In some distributions (Gentoo, for example) **xxd** does not have its own package but is installed alongside **vim** (in Gentoo, **xxd** is in **vim-core**).*


The current build does not use autoconf/automake and includes a Makefile, so building is as simple as running (from within the top-level nfer source directory).

```bash
# this builds the binary at bin/nfer
make
```

The other make targets relevant to a user are:

| Target        | Description                                   |
| ------------- | ----------------------------------------------|
| all           | builds everything                             |
| bin/nfer      | the main executable (default)                 |
| clean         | removes build artifacts                       |
| test          | builds the test executables and runs all tests|


Bugs and Feature Requests
=========================

Nfer is a living project!  If you find what you think is a bug, [please report it](https://bitbucket.org/seanmk/nfer/issues)!  If you would like to use nfer but it lacks support for your precise need (or something it does annoys you), please [file a feature request](https://bitbucket.org/seanmk/nfer/issues)!

