Build Instructions By OS
========================
This page contains instructions to build the nfer command-line interface using various operating systems.
For Linux instructions, see the [main README](../README.md). All instructions assume that you have [installed **git**](http://git-scm.com/download/linux) and cloned the nfer source code repository:

```bash
git clone https://bitbucket.org/seanmk/nfer.git
```


OS X
------------------------
There are a small number of compile time dependencies, most of which are distributed with **Xcode**.  So, the first step is to download and install **Xcode** from the [Mac App Store](https://itunes.apple.com/us/app/xcode/id497799835).  Nfer is tested using Xcode version 12.5 and anything later that should also work.

To install the other dependencies you need **homebrew**.  If you don't have it, install it using the [instructions from their website](https://brew.sh/).

Once **homebrew** is installed you can use it to install the other three dependencies: **lz4**, **flex** and **bison**.

```bash
brew install lz4
brew install flex
brew install bison
```

Once those are installed you can run the build using the included Makefile from inside the nfer source directory.

```bash
# this builds the binary at bin/nfer
make
```

Windows
------------------------
The Windows nfer build relies on the **chocolatey** package manager to install dependencies.  So, the first step for building on windows (after installing **git** and cloning the nfer source) is to download and install chocolatey [using the instructions on their website](https://chocolatey.org/install).

Once **chocolatey** is installed, launch an *administrator* command prompt to install the dependencies:
```bash
choco install -y mingw
choco install -y make
choco install -y winflexbison3
```

**Important:** The rest of the Windows instructions must be run from the *git bash* shell that **git** installs.

The build also needs `lz4` installed and in the PATH, which is not available on chocolatey.  Instead, install it by downloading a zip file with the built program, unpacking it, and then adding it to the PATH.
```
choco install -y wget
wget https://github.com/lz4/lz4/releases/download/v1.9.4/lz4_win64_v1_9_4.zip
mkdir lz4
pushd lz4
unzip ../lz4_win64_v1_9_4.zip
popd
# this sets the PATH in the current shell
export PATH=`pwd`"/lz4:${PATH}"
# this sets the PATH for future shells
echo "PATH=`pwd`/lz4:\${PATH}" >> ~/.profile
```

Then, `cd` into the nfer source directory and build nfer using the provided Makefile:
```bash
# this builds the binary at bin/nfer.exe
make
```

The build has been tested using Windows 10.  Your mileage may vary with other Windows versions.


Solaris
------------------------
For Solaris we will actually assume the use of the Oracle Developer Studio (ODS) compiler.  This is not necessary (it works fine or better with GCC) but nfer should still build with ODS.  It is left as an exercise to the reader to install ODS or another compiler, though.

Solaris 11 comes with some GNU tools installed that we need but to install the others we will use **OpenCSW**.  Follow the instructions for [installing OpenCSW](https://www.opencsw.org/manual/for-administrators/getting-started.html) and then run the following (as root):

```bash
/opt/csw/bin/pkgutil -y -i lz4
/opt/csw/bin/pkgutil -y -i flex
/opt/csw/bin/pkgutil -y -i bison
```

Then you can build nfer with **GNU make** using the included Makefile.  Note that this will spit out *a lot* of warnings but these can be safely ignored.

```bash
# this builds the binary at bin/nfer
gmake
```

This sequence of steps works with Solaris 11 but nfer has been tested successfully on Solaris 10 as well.

FreeBSD
------------------------
We first need to install dependencies using the **pkg** utility.  You will presumably use this to install **git** as well.  Note that, if you try to use **pkg** when it isn't installed you will be automatically prompted to install it.  Then run the following (as root) to install the dependencies:

```bash
pkg install gmake
pkg install flex
pkg install bison
pkg install gcc
pkg install vim
pkg install liblz4
```

Then you can build with **GNU make (gmake)** using the included Makefile:

```bash
# this builds the binary at bin/nfer
gmake
```