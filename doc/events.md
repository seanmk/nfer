Event Traces (Command Line Interface)
======================================
Nfer uses a unique input format for event traces that may be unfamiliar to some users.  There are several reasons for this format, however, that will be briefly covered here.  There are plans to support other event formats but, thus far, no pressing need.  If you have a project that needs a different event format supported, consider [filing an issue](https://bitbucket.org/seanmk/nfer/issues)!

The event traces described here are only used for nfer's command-line interface and monitors compiled for use with a shell (Linux).  For nfer's R or Python interfaces, see their respective documentation for how to provide inputs: for R, install the nfer package and call `browseVignettes(package="nfer")`; for Python, see [the Python documentation](../python/README.md).

Format
------
Event traces are formatted with one event per line.  Lines are delimited with Unix newlines (\\n).

Every event consists of either two or four fields.  The two required fields are *label* and *timestamp*, while *map keys* and *map values* are optional.

| Field | Type | Description | Required |
| ----- | ---- | ----------- | -------- |
| Label (or identifier or name) | String |  A non-unique string that describes what kind of event this is. | required |
| Timestamp | Unsigned 64 bit int | An integer that represents the time when the event occurred.  No unit is assumed, but it is often convenient to use milliseconds since the Unix epoch. | required |
| Map keys | List of strings | A semicolon (`;`) delimited list of strings that give the names of the data carried by the event.  These names may be referred to in rules. | optional |
| Map values| List of *values* | A semicolon delimited list of integers, floats, strings, or Booleans that are named by the map key in the corresponding list. | optional (required if Map keys are given) |

These fields are typically delimited by pipe (`|`) characters, although commas (`,`) may also be used.  Event traces, then, have the following basic structure:

```
Label|Timestamp
Label|Timestamp|Map keys|Map values
```

For example, a trace consisting of the keyboard presses of someone typing this sentence might begin as the following:

```
F|1646442430
o|1646442431
r|1646442431
space|1646442434
e|1646442435
...
```

This example highlights a few things to pay attention to.  First, labels are strings without any extra string delimiters like quotation marks.  Any character (including quotation marks) is valid in a label except for pipe (`|`), or comma (`,`).  There are a few caveats, though!  One is that not all such characters are valid in the labels in rules, which might mean you cannot write a rule to match an event.  Another caveat is that whitespace before and after labels will be discarded, meaning you cannot have a label that is just an ASCII space character.

Two other things are highlighted by this example.  They are that, again, map keys and values are optional, and that timestamps do not need to be sequential *or unique*, but it is a good idea if they increase monotonically.  That is, two or more events may have the same timestamp (even if they have the same label, so long as their data is different), but you should try to keep them in increasing order.  Technically, if a trace is passed as an event file (where it is preloaded into memory) then the order does not matter, but if a trace is passed *online* then events are handled one at a time and the order may affect the results.

Map Data
--------
Events can carry data in the form of a non-nested map (a partial function) from key strings to values.  Data enable events to represent far more complex processes and actions than would be possible without them.  The values in maps can be signed 64-bit integers, doubles, strings, or Booleans (*true* or *false*, all lower case).  Map fields are untyped, however, meaning that a given key is technically allowed to contain different kinds of values.  This is discouraged, though, as it may be hard to organize your rules to avoid type runtime type errors.  Mixing different number types may work, but you might lose precision, so again this is discouraged.

Map keys and map values are given in events as semicolon (`;`) delimited lists.  If one list is present the other must also be and they must be the same length.  The reason is that each key corresponds to a value by its order in the list.  So, the first key names the first value, the second key names the second value, and so on.  Whitespace before and after keys and values is discarded.

For example, suppose a trace consisting of process executions where each process starts, does work, then ends.  Multiple processes may execute concurrently, and the system keeps track of them using a process ID (a pid).  When the process ends, it has a return code.  This trace may look like the following:

```
start|1646442430|pid|22
work|1646442433|pid|22
start|1646442434|pid|33
work|1646442435|pid|33
end|1646442437|pid;return|22;0
end|1646442439|pid;return|33;-127
```

Here, process 22 took seven time units (seconds) to execute and returned zero, while process 33 took five time units to execute and returned -127.  This shows how different events may have different map fields, since only the end events carry the field named `return`.

Reasoning (Why this format?)
----------------------------
While perhaps not perfect, this format has several advantages that make it a good fit for nfer.  These can be summed up as follows:

* Each event is contained on one line, making it possible to have well-formed streaming data.
* Events begin with their label, making them easier for humans to read.  You don't have to parse the line to find the label.
* Each event carries its own map keys, meaning they can be different for each event and they are easier for humans to read.
* No one has preconceived ideas about what the format should support, so they will read the documentation, as you are doing.

A format like JSON, for example, requires a file has matching surrounding delimiters (square or curly braces) meaning that you must either violate the specification by not parsing as you go and assuming the matching brace or you must treat each row as a separate document, which is a bit contrary to the spirit of the format.  JSON also supports arbitrary nesting in maps, which might convince some users that this should be possible in nfer, but it is not.

A format like CSV, on the other hand, treats rows individually but assumes that each column represents a separate data value.  That means that there must be a header that specifies the map keys and that those keys are universal, meaning every event carries every field.  One could treat empty values as not being assigned to an event but it does not solve the problem of the header.

There are, of course, nice things about these formats and other differences that might make sense to adopt.  For example, the advantage of putting the timestamp first in each row instead of the label is that you can then sort the rows easily and this might outweigh the value of not having to visually parse the row to find the label.  A JSON-like key:value list for maps also might be both easier to read and, in some cases, to generate.  CSV files are notoriously efficient (and effectively how the R interface consumes events), so maybe there should be a way to pass a header.

For now, however, this is the format.

Possible Surprises
------------------
Nfer's event format has a few features that may surprise some users.  If you run into unexpected behavior, check this list first to see if it explains what you're seeing.

* An event timestamp outside the bounds of what nfer will accept will instead be interpreted as a zero.  Timestamps are represented as unsigned 64-bit integers, so the minimum acceptable timestamp is `0` and the maximum is `18446744073709551615`.
* An event timestamp that begins with an integer and ends with other characters will be interpreted as the initial integer.  For example, if timestamps are given as real numbers the decimal part of the number will be stripped.  So, `3.14` will be interpreted as `3` and `4th` will be interpreted as `4`.
* The only characters that cannot appear in event names are the two possible column delimiters, comma and pipe.  The only characters that cannot appear in key names or values are those characters (`,` and `|`) along with the key/value delimiter, semicolon (`;`).  There is no way to escape these special characters.
* However, event names cannot *begin* with a pound (`#`), a hyphen (`-`), or a number.
* Column delimiters can be mixed and matched in a single row.  That means, for example, that a comma cannot be used in a key name even when pipes are used consistently as the column delimiter in event input.
* If the number of map keys and values for a row are unequal, then the row will be skipped.  It is considered unsafe to assume that any missing keys or values are necessarily the last ones.  That is, if the keys are `x;y` and there is only one value `10`, it is not considered safe to assume that `x=10` and `y` is missing, when it could be `x` that is missing.
* Whitespace at the beginning or end of event labels, map keys, and map values will be ignored.
* If a map key is repeated no error will be thrown and the last value given for the key will be used.