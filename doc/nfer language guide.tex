\documentclass[a4paper,11pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{lmodern}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage[rounded]{syntax}
\usepackage{acro}
\usepackage{listings}
\usepackage{colonequals}
\usepackage[inline]{enumitem}
\usepackage{xspace}
\usepackage[table]{xcolor}
\usepackage{graphicx}
\usepackage[top=1in, bottom=1in, left=1in, right=1in]{geometry}
\usepackage{caption}

\newcommand{\nfer}{{\tt nfer}\xspace}
\newcommand{\Nfer}{{\tt Nfer}\xspace}

\DeclareAcronym{dsl}{
  short = DSL,
  long = domain-specific language
}

\newcommand{\dsl}{\lstset{language=dsl,backgroundcolor=\color{white}, frame=single}}
\newcommand{\idsl}[1]{\texttt{\textbf{#1}}}
\newcommand{\identifier}[1]{\texttt{#1}}
\newcommand{\nonterm}[1]{$\langle\textit{#1}\rangle$}
\newcommand{\textarrow}{-\resizebox{.5em}{.5em}{\textbf{\textgreater}}}
\newcommand{\lt}{<}
\newcommand{\gt}{>}

\lstdefinelanguage{dsl}{
morekeywords={module,silent,import,also,before,meet,during,start,finish,overlap,slice,coincide,
              begin,end,where,map,this,unless,contain,after,follow,:-}, 
  literate=
      {|->}{{$\mapsto$}}2
  ,
  sensitive=true, 
  morecomment=[l]{//}, 
  morecomment=[s]{/*}{*/},
  escapeinside={(*}{*)},
  stringstyle=\sffamily,
  %aboveskip=1mm,
  %belowskip=1mm,
  showstringspaces=false,
  columns=flexible,
  morestring=[b]",
  %basicstyle={\scriptsize},
  %numbers=left,
  numberstyle=\scriptsize,
  moredelim=[is][\em]{@}{@},
  basicstyle=\ttfamily\fontseries{l}\selectfont,
  keywordstyle=\ttfamily\fontseries{b}\selectfont
} 

\title{Nfer Language Guide}
\author{Sean Kauffman}

\begin{document}

% syntax diagram
% this is necessary to force the specification diagram to stay on one line
\newcommand{\nfersdlengths}{%
  \setlength{\sdstartspace}{.5em minus 15pt}%
  \setlength{\sdendspace}{.5em minus 15pt}%
  \setlength{\sdmidskip}{0.5em plus 0.0001fil}%
  \setlength{\sdtokskip}{0.25em plus 0.0001fil}%
  \setlength{\sdfinalskip}{0.5em plus 10000fil}%
  \setlength{\sdrulewidth}{0.2pt}%
  \setlength{\sdcirclediam}{8pt}%
  \setlength{\sdindent}{0pt}%
}

\maketitle
% \section{Domain Specific Language}
% \label{sec:language}

This guide describes the \ac{dsl} supported by \nfer.
Users must write rules to describe how traces will be abstracted\footnote{The \nfer learning algorithm can also be deployed to mine rules from historical traces.}.
Here, we use syntax diagrams, also known as railroad diagrams, to describe the \nfer syntax in a manner that is easier to understand than a full grammar.
For a complete programming guide, please see the user documentation.

\Nfer specifications are written in \iac{dsl} designed to concisely describe how new intervals are created from existing intervals.
Rules search for one or more intervals that have appeared in the input or have already been produced.
If all the conditions are met to produce a new interval, the rule also describes how that interval is formed.

This section is organized into four parts, each describing a part of the \nfer \ac{dsl} syntax and becoming more detailed as we proceed.
We begin with by describing specifications and rules, then continue to interval expressions and expressions.

% A specification is applied to a trace iteratively until it reaches a fixed point.
% A fixed-point algorithm is necessary because rules can depend on each other and on themselves to form recursive relationships.

\paragraph*{Specification}

Figure~\ref{fig:specification} shows the syntax of a specification in the \nfer \ac{dsl}.
Specifications are made up of rules, but rules may also be separated into modules.
If rules are given in modules, the first module is loaded and others are only included if they are listed in its import statement.
Modules may contain imports and constants, which are shown in Figures~\ref{fig:imports}~and~\ref{fig:constants} and described below.

\begin{figure}[h]
  \begin{syntdiag}[\small\nfersdlengths]
  \begin{stack}
    \begin{stack} \\
      <constants>
    \end{stack} 
    \begin{rep} <rule> \end{rep} \\
    \begin{rep}
    "module" <id> "\{" 
      \begin{stack} \\
        <imports>
      \end{stack} 
      \begin{stack} \\
        <constants>
      \end{stack} 
      \begin{rep} <rule> \end{rep}
      "\}"
    \end{rep}
  \end{stack}
  \end{syntdiag}
  \caption{\nonterm{specification}}
  \label{fig:specification}
\end{figure}

\noindent
\begin{minipage}{.48\textwidth}
% \begin{figure}[h]
  \begin{syntdiag}[\small\nfersdlengths]
  \begin{rep}
    \begin{stack} \\
      "silent"
    \end{stack}
    "import" \begin{rep} <id> \\ "," \end{rep} ";"
  \end{rep}
  \end{syntdiag}
%   \caption{\nonterm{imports}}
  \captionof{figure}{\nonterm{imports}}
  \label{fig:imports}
% \end{figure}
\end{minipage}%
\hspace{.04\textwidth}
\begin{minipage}{.48\textwidth}
% \begin{figure}[h]
  \vspace{.7em}
  \begin{syntdiag}[\small\nfersdlengths]
  \begin{rep}
    <id> "=" <expr>
  \end{rep}
  \end{syntdiag}
  \vspace{1.1em}
%   \caption{\nonterm{imports}}
  \captionof{figure}{\nonterm{constants}}
  \label{fig:constants}
% \end{figure}
\end{minipage}


\vspace{2em}
Figure~\ref{fig:imports} shows the syntax of module imports, which are used to include subsequent modules from the first, automatically imported module.
Imports name the module by its id, and can include the \idsl{silent} keyword to include a module but supress the intervals it generates from the tool's output.

Figure~\ref{fig:constants} shows the syntax of named constants.
Named constants can be included in a module or before rules in a specification without modules.
The scope of a named constant is the module in which it is defined or the specification if it is one without modules.
They name an expression using an id that can be referenced from other expressions in the same scope.
Named constants may reference other named constants that are defined earlier in the same scope.
One name may only be defined as a constant once per scope.
That is, constants may not be redefined.

The following example demonstrates how modules can be used to separate rules logically.
In the example, the \idsl{willLoad} module is loaded first and it imports the \idsl{alsoLoad} module. 
If \idsl{alsoLoad} was not included in the import statement it would not be loaded.
If the the import is specified as a \idsl{silent import} then the intervals produced by rules from the imported module will not appear in the output but can still be referenced by other rules.
Any modules a silently imported module imports will also be considered silently imported.

\dsl
\begin{lstlisting}
module willLoad {
  import alsoLoad;
  // rules go here
}
module alsoLoad {
  // will be loaded
}
\end{lstlisting}

\paragraph*{Rules}

Figure~\ref{fig:rule} shows the syntax of a rule in the \nfer \ac{dsl}.
A rule consists of at least two parts: \begin{enumerate*}
  \item the identifier of the interval to produce and
  \item the interval expression.
\end{enumerate*}
There are also three optional parts: \begin{enumerate*}
  \setcounter{enumi}{2}
  \item a manual constraint,
  \item a map statement, and
  \item an end-point statement.
\end{enumerate*}

\begin{figure}[h]
  \begin{syntdiag}[\small\nfersdlengths]
  <id> ":-" <intervalExpr> 
  \begin{stack} \\
    "where" <expr> \end{stack}
    \begin{stack} \\
    "map \{" \begin{rep} <id> "\textarrow" <expr> \\ "," \end{rep} "\}" \end{stack}
    \begin{stack} \\
    "begin" <expr> "end" <expr> \end{stack}
  \end{syntdiag}
  \caption{\nonterm{rule}}
  \label{fig:rule}
\end{figure}

% The following describes each part of a rule in more detail.
\begin{enumerate}
  \item \emph{Identifier of the interval to produce} -- This is given on the left-hand-side of the \idsl{:-}.  New intervals produced by this rule will be given this identifier.
  \item \emph{Interval expression} -- This gives the identifiers of the intervals to match, applies temporal contraints, and specifies the default timestamps of the intervals produced by this rule.  It is described in more detail later in this section.
  \item \emph{Manual constraint} -- This is an expression, preceded by the \idsl{where} keyword, that further constrains what intervals match this rule.  The expression can refer to the interval identifiers and labels given in the interval expression and must result in a Boolean value.  A true result means the intervals match.
  \item \emph{Map statement} -- This is a list of map keys and their associated expressions, surrounded by \idsl{map \{ ... \}}.  New intervals produced by this rule will have a map computed by this map statement.
  \item \emph{End-point statement} -- This specifies manual overrides for the \idsl{begin} and \idsl{end} timestamps of the intervals produced by this rule.
\end{enumerate}

\paragraph*{Interval Expressions}

Figure~\ref{fig:interval} shows the syntax of interval expressions.
Interval expressions specify at least one interval identifier for the rule to match.
When an interval is found with an identifier listed in the interval expression, manual constraints are tested on it and, if those pass, a new interval is produced.
When more than one interval is specified, they must be related using a temporal operator.
These operators are described in detail in the formal descriptions of the language and in the user manual.
The temporal operators describe both how the two intervals relate to one another and the timestamps that will be used for any new intervals that are produced.
Interval expressions can be nested using parentheses around sub-expressions (there is no natural precedence order).

In expressions, the intervals matched by the interval expression may be referenced by their identifiers.
However, one interval expression may relate more than one interval with the same identifier.
In this case, it is necessary to label the interval by prepending a unique name to the identifier with a \idsl{:}.

\begin{figure}[h]
  \begin{syntdiag}[\small\nfersdlengths]
  \begin{stack} <id> \\
  <id> ":" <id> \\
  "(" <intervalExpr> ")" \\
  <intervalExpr> 
    \begin{stack}
      "also" \\ "before" \\ "meet" \\ "during" \\ "start" \\ "finish" \\ "overlap" \\ "slice" \\ "coincide" \\ "unless" 
      \begin{stack}
        "after" \\ "follow" \\ "contain"
      \end{stack}
    \end{stack}
  <intervalExpr>
  \end{stack}
  \end{syntdiag}
  \caption{\nonterm{intervalExpr}}
  \label{fig:interval}
\end{figure}

In the following example, two rules are shown that demonstrate how interval expressions can be used to refer to intervals from the input and from another rule.
In the first rule, an interval with identifier \identifier{running} is produced when intervals with identifiers \identifier{call} and \identifier{return} are found and where their timestamps meet the constraints of the \idsl{before} operator.
The second rule contains a nested interval expression where it first matches intervals with identifiers \identifier{warning} and \identifier{running} and then matches the produced interval from that interval expression with \identifier{ignore} intervals.
In the second rule, labels are used for \identifier{warning} and \identifier{ignore} to more easily reference them later.
\dsl
\begin{lstlisting}
running :- call before return
problem :- (w:warning during running) unless contain i:ignore
\end{lstlisting}

\paragraph*{Expressions}

Figure~\ref{fig:expr} shows the syntax of expressions.
Expressions are used in manual constraints, map statements, and end-point statements.
They are used to compute the values used for these statements and should be familiar in format and precedence order.
Expressions may refer to named constants defined within the same scope.
The part of expressions that is specific to the \nfer \ac{dsl} is how expressions may refer to the data and timestamps of intervals.
Intervals are referenced by their identifier (when unique) or their label in the interval expression.
The expression can then refer to \idsl{begin} or \idsl{end} timestamps or any map data by its key.

\begin{figure}[h]
  \begin{syntdiag}[\small\nfersdlengths]
  \begin{stack} 
    <integer> \\ <float> \\ <string> \\ <bool> \\ <id> \\
    "(" <expr> ")" \\
    \begin{stack} "!" \\ "-" \end{stack} <expr> \\
    <expr> \begin{stack} "\(*\)" \\ "\(/\)" \\ "\%" \\ "+" \\ "-" 
  \\ "\(\lt\)" \\ "\(\gt\)" \\ "\(\lt=\)" \\ "\(\gt=\)" \\ "!=" \\ "=" \\ "\&" \\ "|" \end{stack} <expr> \\
    <id> "." \begin{stack} <id> \\ "begin" \\ "end" \end{stack}
  \end{stack}
  \end{syntdiag}
  \caption{\nonterm{expr}}
  \label{fig:expr}
\end{figure}

The following example shows a rule with a manual constraint and a map statement that use expressions referring to the matched intervals.
In the manual constraint, the \identifier{address} of the matched \identifier{call} and \identifier{return} intervals are checked for equality.
In the map statement, a key is set that maps the \identifier{args} of the \identifier{call} interval to the \identifier{args} key of a produced interval.
In the map statement, the \identifier{call} interval is referred to by its label instead of its identifier.
\dsl
\begin{lstlisting}
run :- c:call before return 
       where call.address = return.address
       map { args -> c.args }
\end{lstlisting}

% \setlength{\grammarparsep}{0em}
% \begin{grammar}
% <specification> ::= <ruleList> | <moduleList>
%
% <moduleList> ::= 'module' <id> '\{' <imports> <ruleList> '\}' <moduleList> \alt NULL
%
% <imports> ::= 'import' <identList> ';' | NULL
%
% <identList> ::= <id> | <id> ',' <identList>
%
% <ruleList> ::= <rule> <ruleList> | NULL
%
% <rule> ::= ident ':-' <intervalExpr> <whereExpr> <mapExpr> <endPoints>
%
% <intervalExpr> ::= <id> ':' <id> | <id> \alt <intervalExpr> <intervalOp> <intervalExpr> | '(' <intervalExpr> ')'
%
% <whereExpr> ::= 'where' <expr> | NULL
%
% <mapExpr> ::= 'map' '\{' <mapExprList> '\}' | NULL
%
% <mapExprList> ::= <id> '\textarrow' <expr> \alt <id> '\textarrow' <expr> ',' <mapExprList>
%
% <endPoints> ::= 'begin' <expr> 'end' <expr> | NULL
%
% <intervalOp> ::= 'also' | 'before' | 'meet' | 'during' | 'start' \alt 'finish' | 'overlap' | 'slice' | 'coincide' | 'unless' <exclOp>
%
% <exclOp> ::= 'after' | 'follow' | 'contain'
%
% <expr> ::= <INT> | <FLOAT> | <STRING> | <BOOL> 
%   \alt <unaryOp> <expr> | <expr> <binaryOp> <expr> 
%   \alt <id> '.' <id> | <id> '.' 'begin' | <id> '.' 'end' | '(' <expr> ')'
%
% <unaryOp> ::= '!' | '-'
%
% <binaryOp> ::= '\(*\)' | '\(/\)' | '\%' | '+' | '-' 
%   \alt '\(\lt\)' | '\(\gt\)' | '\(\lt=\)' | '\(\gt=\)' | '!=' | '=' | '\&' | '|' 
% \end{grammar}

\end{document}
