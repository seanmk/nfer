#!/usr/bin/env python3

# This is a small script to convert Quantified Boolean Formulae in QDIMACS format
# into nfer.  For right now, we just want to get nfer specs and inputs out of it,
# but later we should also go the other direction and convert the output to QDIMACS.
# Output is the nfer spec on stdout and the corresponding event trace on stderr.

from argparse import ArgumentParser
from pathlib import Path

from lark import Lark, Transformer
from enum import Enum
import sys

# add a function to print to stderr
def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

# this is the QDIMACS grammar, modified to give a nicer parse tree
lark = Lark('''
            input : WS* preamble quant_sets clause_list

            preamble      : [comment_lines] problem_line
            comment_lines : comment_line+
            comment_line  : COMMENT comment EOL
            comment       : WORD*
            problem_line  : PROBLEM CNF PNUM PNUM EOL

            quant_sets : quant_set*
            quant_set  : quantifier atoms lineterm
            quantifier : EXISTS | FORALL
            atoms      : PNUM+

            clause_list : clause+
            clause      : literals lineterm
            literals    : NUM+

            lineterm  : ZERO EOL

            CNF       : "cnf"
            COMMENT   : "c"
            PROBLEM   : "p"
            ZERO      : "0"
            EXISTS    : "e"
            FORALL    : "a"

            NUM       : ["-"] PNUM
            PNUM      : /[1-9][0-9]*/

            %import common.WORD
            %import common.NEWLINE -> EOL
            %import common.WS
            %import common.WS_INLINE
            %ignore WS_INLINE
            ''', start='input')

class QuantifierKind(Enum):
    EXISTENTIAL = 1
    UNIVERSAL   = 2

def prime(i, primes):
    for prime in primes:
        if not (i == prime or i % prime):
            return False
    primes.add(i)
    return i

def gen_primes(n):
    primes = set([2])
    i, p = 2, 0
    while True:
        if prime(i, primes):
            p += 1
            if p == n:
                return primes
        i += 1

class QBF:
    """This is our data structure for representing QBF"""
    def __init__(self, comments, quantifiers, cnf) -> None:
        self.comments = comments
        self.quantifiers = quantifiers
        self.cnf = cnf
        self.handle_dimacs()
        self.apply_primes()

    def handle_dimacs(self) -> None:
        # handle DIMACS no quantifiers
        if not self.quantifiers:
            q = Quantifier(QuantifierKind.EXISTENTIAL)
            q.add(self.cnf.atoms())
            self.quantifiers.append(q)

        else :
            q0 = self.quantifiers[0]
            if q0.kind == QuantifierKind.EXISTENTIAL:
                atoms = self.cnf.atoms() - self.all_atoms()
                for atom in atoms:
                    q0.add(atom)

    def apply_primes(self) -> None:
        # 1. go through the quantifiers, getting all the vars in order
        self.vars = []
        self.var_to_quantifier = {}
        for q in self.quantifiers:
            self.vars += q.vars
            # also store a mapping from var to quantifier
            for v in q.vars:
                self.var_to_quantifier[v] = q
        # vars now is all the variables in the order they appear
        # 2. get primes in a list
        primes = list(gen_primes(len(self.vars)))
        # 3. create an assignment - 1 is what zero index will get
        self.primes = [1] * (len(self.vars) + 1)
        for i in range(len(self.vars)):
            self.primes[self.vars[i]] = primes[i]
    
    def all_atoms(self) -> set:
        all = set()
        for q in self.quantifiers:
            all.update(q.atoms())
        return all
    
    def __str__(self) -> str:
        result = ''
        if self.comments is not None:
            for comment in self.comments:
                result += '// ' + comment + '\n'
        for quant in self.quantifiers:
            result += str(quant) + '\n'
        result += str(self.cnf)
        return result

class Quantifier:
    """This class represents a quantifier expression"""
    def __init__(self, qkind) -> None:
        self.kind = qkind
        self.vars = []
    
    def add(self, variable) -> None:
        self.vars.append(variable)

    def atoms(self) -> set:
        return set(self.vars)

    def __str__(self) -> str:
        return self.kind.name + str(self.vars)

class CNF:
    """This class represents a CNF expression"""
    def __init__(self) -> None:
        self.clauses = []
    
    def add(self, disjunction) -> None:
        self.clauses.append(disjunction)
    
    def atoms(self) -> set:
        result = set()
        for clause in self.clauses:
            for atom in clause.atoms():
                result.add(atom)
        return result

    def __str__(self) -> str:
        return str(self.clauses)
    
    def nfer(self, field, primes) -> str:
        clauses = []
        for clause in self.clauses:
            clauses.append(clause.nfer(field, primes))
        return " & ".join(clauses)

class Disjunction:
    def __init__(self, literals) -> None:
        self.literals = literals
    
    def atoms(self) -> set:
        result = set()
        for lit in self.literals:
            result.add(abs(lit))
        return result

    def __str__(self) -> str:
        return str(self.literals)
    
    def nfer(self, field, primes) -> str:
        result = []
        for literal in self.literals:
            atom = abs(literal)
            if literal < 0:
                result.append(f'({field} % {primes[atom]} != 0)')
            else:
                result.append(f'({field} % {primes[atom]} = 0)')
        return "(" + (" | ".join(result)) + ")"
    
class QDIMACS_to_QBF(Transformer):
    def input(self, components):
        if len(components) == 4:
            ws, comments, quants, cnf = components
        else:
            comments, quants, cnf = components
        return QBF(comments, quants, cnf)

    def preamble(self, components):
        if (len(components) > 1):
            return components[0]
        else:
            return None

    def comment_lines(self, comments):
        return comments

    def comment_line(self, components):
        c, cs, eol = components
        return cs

    def comment(self, words):
        return " ".join(words)

    def quant_sets(self, sets):
        return list(sets)

    def quant_set(self, components):
        quant = Quantifier(components[0])
        for atom in components[1]:
            quant.add(atom)
        return quant

    def quantifier(self, kind):
        if 'e' in kind:
            return QuantifierKind.EXISTENTIAL
        else:
            return QuantifierKind.UNIVERSAL
    
    def atoms(self, children):
        return list(children)
    
    def PNUM(self, number):
        return int(number)
    
    def lineterm(self, child):
        return None

    def clause_list(self, clauses):
        cnf = CNF()
        for c in clauses:
            cnf.add(c)
        return cnf

    def clause(self, children):
        return Disjunction(children[0])

    def literals(self, children):
        return list(children)

    def NUM(self, number):
        return int(number)

def test():
    transformer = QDIMACS_to_QBF()

    test_qbf1 = """
                p cnf 4 2
                e 1 2 3 4 0
                -1  2 0
                2 -3 -4 0
                """
    ast = lark.parse(test_qbf1)
    qbf = transformer.transform(ast)
    print(qbf)

    test_qbf2 = """
                c this one has both ex and fa
                c second comment line
                p cnf 4 2
                a 1 2 0
                e 3 4 0
                -1  2 0
                2 -3 -4 0
                """
    ast = lark.parse(test_qbf2)
    qbf = transformer.transform(ast)
    print(qbf)

    test_dimacs = """
                p cnf 4 2
                -1  2 0
                2 -3 -4 0
                """
    ast = lark.parse(test_dimacs)
    qbf = transformer.transform(ast)
    print(qbf)

if __name__ == '__main__':
    # 0. load the QDIMACS file
    parser = ArgumentParser(description="")
    parser.add_argument("source", type=Path)
    args = parser.parse_args()

    with open(args.source, 'r') as source_file:
        qdimacs = source_file.read()

    ast = lark.parse(qdimacs)
    transformer = QDIMACS_to_QBF()
    qbf = transformer.transform(ast)

    # algorithm
    # 0. Write any comments
    if qbf.comments is not None:
        for comment in qbf.comments:
            print(f'// {comment}')

    # 1. generate all the possible literal combinations
    for i in range(len(qbf.vars)):
        # representation for this atom
        p  = qbf.primes[qbf.vars[i]]

        print(f'g{i+1} :- gx:g{i} coincide gy:g{i} where gx.m = gy.m map {{ m -> gx.m }}')
        print(f'g{i+1} :- gx:g{i} coincide gy:g{i} where gx.m = gy.m map {{ m -> gx.m * {p} }}')
    
    # 2. check the formula
    max_i = len(qbf.vars)
    formula = qbf.cnf.nfer(f'gx.m', qbf.primes)
    print(f'c{max_i} :- gx:g{max_i} coincide gy:g{max_i} where gx.m = gy.m & {formula} map {{ m -> gx.m }}')

    # 3. now walk it back
    # iterate backwards over the variables
    for i in reversed(range(len(qbf.vars))):
        # get a handle to the var
        v = qbf.vars[i]
        # quantifier for this atom
        q = qbf.var_to_quantifier[v]
        # prime representation for this atom
        p = qbf.primes[v]
        
        # operate on the quantifier kind
        if q.kind == QuantifierKind.EXISTENTIAL:
            print(f'c{i} :- cx:c{i+1} coincide cy:c{i+1} where (cx.m = cy.m) & ((cx.m % {p}) > 0) map {{ m -> cx.m }}')
            print(f'c{i} :- cx:c{i+1} coincide cy:c{i+1} where (cx.m = cy.m) & ((cx.m % {p}) = 0) map {{ m -> cx.m / {p} }}')
        else:
            # otherwise it's universal
            print(f'c{i} :- cx:c{i+1} coincide cy:c{i+1} where (cx.m = cy.m * {p}) map {{ m -> cy.m }}')

    # now test for T
    print(f'T :- cx:c0 coincide cy:c0 where cx.m = cy.m & cx.m = 1')

    # 4. write to stderr the initial event to kick things off 
    eprint('g0|0|m|1')