Command-Line Interface
======================
The command-line interface is the most feature-rich way to use nfer.  In addition to the core nfer functionality of applying rules to traces (either offline or online), the program can mine rules from traces, generate compilable monitors, and perform some specification analysis.


Monitoring
----------
The primary use of the tool is to apply [specifications](nfer.md) to [event traces](events.md), also called monitoring.  This can be done either offline or online.  The difference between these is subtle and is addressed later in this document.  In offline monitoring, a finite trace file is loaded into memory and processed, outputting the produced intervals.  In online monitoring, a trace is passed to nfer one event at a time until the process is terminated (or EOF is reached) with nfer outputting intervals as soon as they are produced.

To use the command-line interface for offline monitoring, load a trace using the `--event-file` or `-e` option.  The specification file is given as the first argument after the options.
```bash
# loads a trace my.events and applies the rules in myrules.nfer
./bin/nfer --event-file=my.events myrules.nfer
```

To use the command-line interface for online monitoring, simply give the specification as the first argument and then supply events on `stdin`.
```bash
# applies the rules in myrules.nfer to the events in my.events, one at a time
./bin/nfer myrules.nfer < my.events
```

Logging and diagnostics
-----------------------
Often, you may write some rules and try them and discover that they do not output what you desire.  By default, nfer only outputs warnings and errors, but much more information is available via logging.

To set the log level, use the `--log-level` or `-l` option, where the valid settings are

| Setting | Effect           |
| ------- | ---------------- |
| -1      | Disable warnings |
| 0       | (default) Only warnings and errors |
| 1       | Status (rarely used) |
| 2       | Info             |
| 3       | Debug            |
| 4       | Super debug (only for development) |

You can also pass `--verbose` or `-v` to set the log level to 3.

Note that, if you set log level 4, running nfer may produce files in the local directory.

Semantics and optimization
--------------------------
The command-line interface supports two options that affect the semantics of nfer in different ways.  The first disables something call *minimality* while the other applies an optimization to improve both speed and memory consumption.

Minimality is described in somewhat more detail in the [nfer language documentation](nfer.md), but the idea is that the only intervals nfer will produce by default are ones that do not subsume other intervals from the same rule.  This behavior is optional, though, and can be disabled in the command-line interface using the `--full` or `-f` flag.
```bash
# this may produce exponentially more intervals
./bin/nfer --full myrules < my.events
```

The other way that nfer semantics may be modified is to improve performance.  By default, nfer will keep in memory everything it has seen in the past to ensure it can create all the correct intervals in the future.  However, in many cases users will know that their rules should only ever match intervals in a certain time window.  To tell nfer to only store intervals within such a window, pass the `--window` or `-w` flag, specifying the window size in the time units of your events.
```bash
# discard anything older than 200 time units
./bin/nfer --window=200 myrules < my.events
```

Rule mining
-----------
The command-line interface supports mining rules from historical event traces.  The mining algorithm only finds rules using the *before* operator and does not take into account data.  However, it is still very useful for finding temporal relationships in an event trace, particularly when the event trace represents the execution of a real-time embedded system.  *The algorithm uses heuristics that make sense for this use case, so your mileage may vary if the trace does not represent such a system.*

To run the mining algorithm (the learner), simply load an event trace as you would for offline monitoring using the `--event-file` or `-e` option but do not pass a specification file.  When using the mining algorithm you can specify more than one event file and they will all be loaded.
```bash
# this will output the rules it finds to stdout
./bin/nfer -e my.events
```

Running the learner without any other options will use default parameters for the algorithm, but these can be adjusted.  There are two such parameters:

| Parameter | Command-line option | Effect |
| --------- | ------------------- | ------ |
| Confidence | `--confidence` | Sets the confidence threshold.  A number between 0 and 1 (default 0.9) this sets how sensitive the algorithm is to intervals that violate the mined rule. |
| Support | `--support` | Sets the support threshold.  A positive integer (default 10), this sets the absolute minimum number of intervals that must match the mined rule for it to be considred. |

Online versus offline
---------------------
Online versus offline monitoring only affects the semantics of nfer when `exclusive` rules are present in a specification (see [the nfer language documentation](nfer.md) for details on exclusive rules) and events may appear out-of-order.  Suppose a specification with the following rule:
```
# this rule says to produce an interval labeled "a" if one labeled "b" appears without a "c" coming first
a :- b unless after c
```

Then suppose the following, two event trace (given in [event file format](events.md)).
```
b|10
c|5
```

If these are passed using offline monitoring, no new `a`-labeled interval will be produced because there is a `c` labeled interval that appears before the `b` labeled interval (by timestamp).  However, if this trace is passed one event at a time using online monitoring, then an `a`-labeled interval will be produced because the `b` event will be processed before the `c` event is passed to the tool.
