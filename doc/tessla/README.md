TeSSLa Comparison
=================

This directory contains a [translation](test_ops.tessla) of the [test_ops.nfer specification](../../examples/specs/test_ops.nfer) into [TeSSLa](https://www.tessla.io/).  It also contains a [script](nfer2tessla.py) for converting event files in nfer's preferred input format into what TeSSLa expects.

These files are part of a comparison performed for a paper due to be published at SEFM'21.

To run the specification in TeSSLa for comparison purposes, perform the following steps in your Unix shell:
```
# compile the TeSSLa spec
java -jar tessla-assembly-1.2.2.jar compile test_ops.tessla test_ops.jar
# convert an event file to the TeSSLa format
python nfer2tessla.py ../../examples/logs/test_1k_ops.events > test_1k_ops.input
# run the program, redirecting the output to a file and ensuring you have enough memory
java  -Xss900m -jar test_ops.jar < test_1k_ops.input &> tessla.out