#!/usr/bin/env python3

import sys

if __name__ == '__main__':
    if len(sys.argv) > 1:
        nfer = str(sys.argv[1])

    last = 0
    last_name = ''
    with open(nfer, 'r') as reader:
        for line in reader:
            parts = line.split('|')
            name = parts[0]
            time = int(parts[1])
            if name == last_name and time == last:
                time = time + 1
            last = time
            last_name = name
            vals = parts[3].split(';')
            
            id = vals[0].rstrip()
            success = 'true'
            
            if len(vals) > 1:
                success = vals[1].rstrip()
                print('%d: %s = ("%s",%s)' % (time, name, id, success))                
            else:
                print('%d: %s = "%s"' % (time, name, id))                
            
