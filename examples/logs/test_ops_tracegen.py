#!/usr/bin/env python3

import sys
import random
from uuid import uuid4
import operator

class Event:
    def __init__(self, name, time, data):
        self.name = name
        self.time = time
        self.data = data
    
    def __str__(self):
        return '%s|%d|%s|%s' % (self.name, self.time, ';'.join(self.data.keys()), ';'.join(self.data.values()))

class Operation:
    def __init__(self, start, middle, end, failchance):
        start_end = middle+((end - middle)/2)
        self.on = random.randrange(start, start_end)
        self.off = random.randrange(max(self.on+2, middle), end)
        self.test = random.randrange(self.on+1, self.off)
        self.id = "id{}".format(str(uuid4()))
        self.success = True
        if random.randrange(101) < failchance:
            self.success = False
    
    def get_row(self, time):
        if self.on == time:
            return self.get_on()
        elif self.off == time:
            return self.get_off()
        elif self.test == time:
            return self.get_test()
        else:
            return None
    
    def get_on(self):
        return Event('ON', self.on, {'id': self.id})
    def get_off(self):
        return Event('OFF', self.off, {'id': self.id})
    def get_test(self):
        success = str(self.success).lower()
        return Event('TEST', self.test, {'id': self.id, 'success': success})
    
    def __str__(self):
        return '%s\n%s\n%s' % (self.get_on(), self.get_test(), self.get_off())

if __name__ == '__main__':
    num_ops = 100
    if len(sys.argv) > 1:
        num_ops = int(sys.argv[1])
    
    ops = []
    events = []
    gap    = 1000
    start  = gap
    middle = gap * 2
    end    = middle + gap
    failchance = 5
    
    for i in range(num_ops):
        op = Operation(start, middle, end, failchance)
        # can't start until after the last started
        start = op.on + 1
        # can't end until after the last ended
        middle = op.off + 1
        # end just needs to be somewhat later than middle
        end = middle + gap
        ops.append(op)

    
    for op in ops:
        events.append(op.get_on())
        events.append(op.get_test())
        events.append(op.get_off())

    events.sort(key=operator.attrgetter('time'))

    for e in events:
        print(e)

    # for time in range(1000*num_ops):
    #     for op in ops:
    #         row = op.get_row(time)
    #         if row is not None:
    #             print(row)
    