/*
╞══════════════════════════════════════════════════════════════════════════════╡
│ Copyright 2020 Justine Alexandra Roberts Tunney                              │
│                                                                              │
│ This program is free software; you can redistribute it and/or modify         │
│ it under the terms of the GNU General Public License as published by         │
│ the Free Software Foundation; version 2 of the License.                      │
│                                                                              │
│ This program is distributed in the hope that it will be useful, but          │
│ WITHOUT ANY WARRANTY; without even the implied warranty of                   │
│ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU             │
│ General Public License for more details.                                     │
│                                                                              │
│ You should have received a copy of the GNU General Public License            │
│ along with this program; if not, write to the Free Software                  │
│ Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA                │
│ 02110-1301 USA                                                               │
╚─────────────────────────────────────────────────────────────────────────────*/
#ifndef COSMOPOLITAN_LIBC_KOMPRESSOR_KOMPRESSOR_H_
#define COSMOPOLITAN_LIBC_KOMPRESSOR_KOMPRESSOR_H_

#include <stdio.h>
#include "types.h"

#define LZ4_EOF 0
#define LZ4_VERSION 1
#define LZ4_MAGICNUMBER 0x184D2204
#define LZ4_SKIPPABLE0 0x184D2A50
#define LZ4_SKIPPABLEMASK 0xFFFFFFF0
#define LZ4_MAXHEADERSIZE (MAGICNUMBER_SIZE + 2 + 8 + 4 + 1)
#define LZ4_BLOCKMAXSIZE_64KB 4
#define LZ4_BLOCKMAXSIZE_256KB 5
#define LZ4_BLOCKMAXSIZE_1MB 6
#define LZ4_BLOCKMAXSIZE_4MB 7

#define read16le(P) ((uint16_t)(P)[1] << 010 | (uint16_t)(P)[0])
#define read32le(P)                                       \
  ((uint32_t)(P)[3] << 030 | (uint32_t)(P)[2] << 020 | \
   (uint32_t)(P)[1] << 010 | (uint32_t)(P)[0])
#define read64le(P)                                                   \
  ((uint64_t)(P)[3] << 030 | (uint64_t)(P)[2] << 020 | \
   (uint64_t)(P)[1] << 010 | (uint64_t)(P)[0])

#define _LZ4_FRAME_FLG(FRAME) (*((FRAME) + 4))
#define _LZ4_FRAME_BD(FRAME) (*((FRAME) + 5))
#define LZ4_MAGIC(FRAME) read32le(FRAME)
#define LZ4_FRAME_VERSION(FRAME) ((_LZ4_FRAME_FLG(FRAME) >> 6) & 0b11)
#define LZ4_FRAME_BLOCKINDEPENDENCE(FRAME) ((_LZ4_FRAME_FLG(FRAME) >> 5) & 1)
#define LZ4_FRAME_BLOCKCHECKSUMFLAG(FRAME) ((_LZ4_FRAME_FLG(FRAME) >> 4) & 1)
#define LZ4_FRAME_BLOCKCONTENTSIZEFLAG(FRAME) ((_LZ4_FRAME_FLG(FRAME) >> 3) & 1)
#define LZ4_FRAME_BLOCKCONTENTCHECKSUMFLAG(FRAME) \
  ((_LZ4_FRAME_FLG(FRAME) >> 2) & 1)
#define LZ4_FRAME_DICTIONARYIDFLAG(FRAME) ((_LZ4_FRAME_FLG(FRAME) >> 0) & 1)
#define LZ4_FRAME_BLOCKMAXSIZE(FRAME) ((_LZ4_FRAME_BD(FRAME) >> 4) & 0b111)
#define LZ4_FRAME_RESERVED1(FRAME) ((_LZ4_FRAME_FLG(FRAME) >> 1) & 1)
#define LZ4_FRAME_RESERVED2(FRAME) ((_LZ4_FRAME_BD(FRAME) >> 7) & 1)
#define LZ4_FRAME_RESERVED3(FRAME) ((_LZ4_FRAME_BD(FRAME) >> 0) & 0b1111)
#define LZ4_FRAME_BLOCKCONTENTSIZE(FRAME) \
  (LZ4_FRAME_BLOCKCONTENTSIZEFLAG(FRAME) ? read64le((FRAME) + 4 + 1 + 1) : 0)
#define LZ4_FRAME_DICTIONARYID(FRAME)                          \
  (LZ4_FRAME_DICTIONARYIDFLAG(FRAME)                           \
       ? read32le(((FRAME) + 4 + 1 + 1 +                       \
                   8 * LZ4_FRAME_BLOCKCONTENTSIZEFLAG(FRAME))) \
       : 0)
#define LZ4_FRAME_HEADERCHECKSUM(FRAME)                                \
  (*((FRAME) + 4 + 1 + 1 + 8 * LZ4_FRAME_BLOCKCONTENTSIZEFLAG(FRAME) + \
     4 * LZ4_FRAME_DICTIONARYIDFLAG(FRAME)))
#define LZ4_FRAME_HEADERSIZE(FRAME)                        \
  (4 + 1 + 1 + 8 * LZ4_FRAME_BLOCKCONTENTSIZEFLAG(FRAME) + \
   4 * LZ4_FRAME_DICTIONARYIDFLAG(FRAME) + 1)

#define LZ4_BLOCK_DATA(block) (block + sizeof(uint32_t))
#define LZ4_BLOCK_DATASIZE(block) (read32le(block) & 0x7fffffff)
#define LZ4_BLOCK_ISEOF(block) (read32le(block) == LZ4_EOF)
#define LZ4_BLOCK_ISCOMPRESSED(block) ((read32le(block) & 0x80000000) == 0)
#define LZ4_BLOCK_SIZE(frame, block)              \
  (sizeof(uint32_t) + LZ4_BLOCK_DATASIZE(block) + \
   (LZ4_FRAME_BLOCKCHECKSUMFLAG(frame) ? sizeof(uint8_t) : 0))

uint8_t *lz4cpy(uint8_t *dest, const uint8_t *blockdata, size_t blocksize, size_t limit);
size_t lz4decode(uint8_t *dest, const uint8_t *src, size_t limit);
bool lz4decode_to_file(FILE *file, const uint8_t *src, size_t src_size);

#endif /* COSMOPOLITAN_LIBC_KOMPRESSOR_KOMPRESSOR_H_ */
