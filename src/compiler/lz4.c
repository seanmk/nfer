/*
 * lz4.c
 *
 *  Created on: May 16, 2023
 *  Based on work by: Justine Alexandra Roberts Tunney
 *  Adapted for nfer by: skauffma
 *
 *    nfer - a system for inferring abstractions of event streams
 *   Copyright (C) 2017  Sean Kauffman
 *
 *   This file is part of nfer.
 *   nfer is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include "lz4.h"
#include "memory.h"
#include "log.h"

static void repmovsb(uint8_t **dest, const uint8_t **src, size_t cx) {
    uint8_t *di = (uint8_t *)*dest;
    const uint8_t *si = (const uint8_t *)*src;

    while (cx) {
        *di++ = *si++;
        cx--;
    }
    *dest = di;
    *src = si;
}

/**
 * Decompresses LZ4 block.
 *
 * This is a 103 byte implementation of the LZ4 algorithm. Please note
 * LZ4 files are comprised of multiple frames, which may be decoded
 * together using the wrapper function lz4decode().
 *
 * @see rldecode() for a 16-byte decompressor
 */
uint8_t *lz4cpy(uint8_t *dest, const uint8_t *blockdata, size_t blocksize, size_t limit) {
    const uint8_t *ip, *ipe, *match;
    uint8_t *op;
    uint16_t token, length, fifteen, offset, matchlen;
    size_t copied;

    op = dest;
    ip = blockdata;
    ipe = ip + blocksize;
    // we track how much we have actually copied
    copied = 0;

    while (true) {
        token = *ip++;
        length = token >> 4;
        fifteen = 15;

        if (length == fifteen) {
            do {
              length += *ip;
            } while (*ip++ == 255);
        }

        // check to see if the length being copied will exceed limit
        if (copied + length > limit) {
            // if it will exceed the limit, then bail on copying this block
            return dest;
        }
        // track the amount to be copied
        copied += length;
        repmovsb(&op, &ip, length);
        if (ip >= ipe) {
            break;
        }

        offset = read16le(ip);
        matchlen = token & fifteen;
        ip += 2;
        if (matchlen == fifteen) {
            do {
                matchlen += *ip;
            } while (*ip++ == 255);
        }
        match = op - offset;
        matchlen += 4;
        // check to see if the length being copied will exceed limit
        if (copied + matchlen > limit) {
            // if it will exceed the limit, then bail on copying this block
            return dest;
        }
        // track the amount to be copied
        copied += matchlen;
        repmovsb(&op, &match, matchlen);
    }
    return op;
}

/**
 * Decompresses LZ4 data.
 *
 * We assume (1) the file is mmap()'d or was copied into into memory
 * beforehand; and (2) folks handling untrustworthy data shall place
 * 64kb of guard pages on the ends of each buffer, see mapanon(). We
 * don't intend to support XXHASH; we recommend folks needing checks
 * against data corruption consider crc32c(), or gzip since it's the
 * best at file recovery. Dictionaries are supported; by convention,
 * they are passed in the ≤64kb bytes preceding src.
 * 
 * The limit argument sets the size of the dest buffer.  If the number
 * of bytes that would be copied to dest exceeds this size, then
 * the function will return zero, indicating that the operation should
 * be repeated with a larger buffer.
 *
 * @return the number of bytes written to dest, zero if there wasn't 
 *         enough space.
 * @see mapanon(), lz4check()
 */
size_t lz4decode(uint8_t *dest, const uint8_t *src, size_t limit) {
    const uint8_t *frame, *block;
    uint8_t *next_dest;
    size_t copied;

    frame = (const uint8_t *)src;
    copied = 0;

    for (block = frame + LZ4_FRAME_HEADERSIZE(frame); !LZ4_BLOCK_ISEOF(block);
         block += LZ4_BLOCK_SIZE(frame, block)) {
        if (LZ4_BLOCK_ISCOMPRESSED(block)) {
            next_dest = lz4cpy(dest, LZ4_BLOCK_DATA(block), LZ4_BLOCK_DATASIZE(block), limit - copied);
            // if lz4cpy failed to update the pointer, then we assume it is because the
            // size to copy exceeded the limit.
            if (next_dest == dest) {
                // in this case, we want to return zero - the calling code should repeat
                return 0;
            }
            // otherwise, update copied, then update dest
            copied += (next_dest - dest);
            dest = next_dest;
        } else {
            // in this case we can check the data limit prior to calling copy
            if (copied + LZ4_BLOCK_DATASIZE(block) > limit) {
                // there's not enough space, return zero
                return 0;
            }
            copy_memory(dest, LZ4_BLOCK_DATA(block), LZ4_BLOCK_DATASIZE(block));
            // update copied, update dest
            copied += LZ4_BLOCK_DATASIZE(block);
            dest += LZ4_BLOCK_DATASIZE(block);
        }
    }
    return copied;
}

/**
 * Decompresses LZ4 file.
 * 
 * This actually writes to a file, not just a buffer.
 * The file is assumed to be open and is left open.
 *
 * We assume (1) the file is mmap()'d or was copied into into memory
 * beforehand; and (2) folks handling untrustworthy data shall place
 * 64kb of guard pages on the ends of each buffer, see mapanon(). We
 * don't intend to support XXHASH; we recommend folks needing checks
 * against data corruption consider crc32c(), or gzip since it's the
 * best at file recovery. Dictionaries are supported; by convention,
 * they are passed in the ≤64kb bytes preceding src.
 *
 * @return true on success, false on failure
 * @see mapanon(), lz4check()
 */
bool lz4decode_to_file(FILE *file, const uint8_t *src, size_t src_size) {
    uint8_t *buffer, *new_buffer;
    size_t bufsize, decoded, written;
    unsigned int loop_count;
    bool result;

    // start by trying to allocate double the src size
    bufsize = src_size;
    buffer = NULL;
    decoded = 0;
    loop_count = 0;
    result = true;

    // try to allocate space and decode into it
    // if the space isn't big enough, we double it and try again
    // we cap the number of times we double the space to four, as this
    // means a x16 compression ratio which should never happen
    while (decoded == 0 && loop_count < 4) {
        // try to allocate double the size
        bufsize = bufsize * 2;
        new_buffer = realloc(buffer, bufsize);

        if (new_buffer == NULL) {
            // if we can't alloc, fail
            filter_log_msg(LOG_LEVEL_ERROR, "Couldn't allocate write buffer. Aborting.\n");

            // clean up
            if (buffer != NULL) {
                free(buffer);
            }
            // bail out
            return false;
        }
        buffer = new_buffer;
        decoded = lz4decode(buffer, src, bufsize);
        // ensure we don't go too nuts
        loop_count++;
    }

    // now, if it worked, write the buffer to the file
    if (decoded > 0) {
        written = fwrite(buffer, decoded, 1, file);

        // now check for errors
        if (written != 1) {
            if (feof(file)) {
                filter_log_msg(LOG_LEVEL_ERROR, "Reached EOF writing decompressed data.\n");
            } else if (ferror(file)) {
                filter_log_msg(LOG_LEVEL_ERROR, "An error occurred writing decompressed data.\n");
            } else {
                filter_log_msg(LOG_LEVEL_ERROR, "Wrong number of decompressed bytes written.\n");
            }
            result = false;
        }
    } else {
        // decoded was zero
        result = false;
    }
    // clean up and exit
    free(buffer);
    return result;
}
