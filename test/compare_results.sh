#!/bin/bash

# stdin has the test output
TEST=`cat`

# result file is passed as an arg
RESULT_FILE="${1}"
CORRECT=`cat "${RESULT_FILE}"`

# lines in the correct output
LINES=`echo "${CORRECT}" | grep -c ''`

# hacky to do it this way but we want to canonicalize the line order
# the correct way would be after canonicalizing the lines
TEST=`echo ${TEST} | tr " " "\n" | sort`
CORRECT=`echo ${CORRECT} | tr " " "\n" | sort`

# we are not being ambitious 
# for now, all we want to do is canonicalize the order of data
for i in $(seq 1 $LINES); do
    # get one line at a time to compare them
    # we are not trying to reorder lines at all - just canonicalize lines
    IDEAL=`echo "${CORRECT}" | head -n$i | tail -n1`
    CHECK=`echo "${TEST}" | head -n$i | tail -n1`

    # now break up into fields and compare
    # 1) check the label
    IDEAL_LABEL=`echo "${IDEAL}" | cut -d"|" -f 1`
    CHECK_LABEL=`echo "${CHECK}" | cut -d"|" -f 1`
    if [ "${IDEAL_LABEL}" != "${CHECK_LABEL}" ]; then
      exit 1
    fi
    # 2) check the begin time
    IDEAL_BEGIN=`echo "${IDEAL}" | cut -d"|" -f 2`
    CHECK_BEGIN=`echo "${CHECK}" | cut -d"|" -f 2`
    if [ ${IDEAL_BEGIN} -ne ${CHECK_BEGIN} ]; then
      exit 2
    fi
    # 3) check the end time
    IDEAL_END=`echo "${IDEAL}" | cut -d"|" -f 3`
    CHECK_END=`echo "${CHECK}" | cut -d"|" -f 3`
    if [ ${IDEAL_END} -ne ${CHECK_END} ]; then
      exit 3
    fi
    # 4) check the data - must canonicalize the order
    IDEAL_KEYS=`echo "${IDEAL}" | cut -d"|" -f 4`
    CHECK_KEYS=`echo "${CHECK}" | cut -d"|" -f 4`
    IDEAL_VALS=`echo "${IDEAL}" | cut -d"|" -f 5`
    CHECK_VALS=`echo "${CHECK}" | cut -d"|" -f 5`
    # we have to put the keys and values in canonical order
    # to do this, first combine them key:val, then sort
    # get the number of fields
    FIELD_COUNT=`echo "${IDEAL_KEYS}" | grep -c ';'`
    FIELD_COUNT=$(($FIELD_COUNT + 1))
    ID_FIELD_LIST=""
    CH_FIELD_LIST=""
    NEWLINE=$'\n'
    for f in $(seq 1 $FIELD_COUNT); do
      ID_KEY=`echo "${IDEAL_KEYS}" | cut -d";" -f $f`
      ID_VAL=`echo "${IDEAL_VALS}" | cut -d";" -f $f`
      ID_FIELD_LIST="${ID_FIELD_LIST}${ID_KEY}:${ID_VAL}${NEWLINE}"
      CH_KEY=`echo "${CHECK_KEYS}" | cut -d";" -f $f`
      CH_VAL=`echo "${CHECK_VALS}" | cut -d";" -f $f`
      CH_FIELD_LIST="${CH_FIELD_LIST}${CH_KEY}:${CH_VAL}${NEWLINE}"
    done
    # sort %? in the var removes the last character
    ID_SORTED=`echo "${ID_FIELD_LIST%?}" | sort`
    CH_SORTED=`echo "${CH_FIELD_LIST%?}" | sort`
    # iterate again and check them
    for l in $(seq 1 $FIELD_COUNT); do
      IDEAL_FIELD=`echo "${ID_SORTED}" | head -n$l | tail -n1`
      CHECK_FIELD=`echo "${CH_SORTED}" | head -n$l | tail -n1`
      if [ "${IDEAL_FIELD}" != "${CHECK_FIELD}" ]; then
#        echo "${IDEAL_FIELD} != ${CHECK_FIELD}"
        exit 4
      fi
    done
done

exit 0