#!/bin/sh

DIR="$( cd "$( dirname "${0}" )" && pwd )"

BINARY="${1}"
TEST="${2}"
TMP="${3}"
ARGS=""
TEST_DIR="${DIR}/functional"

TEST_SPEC="${TEST_DIR}/${TEST}.nfer"
TEST_EVENTS="${TEST_DIR}/${TEST}.events"
TEST_RESULTS="${TEST_DIR}/${TEST}.results"
TEST_ARGS="${TEST_DIR}/${TEST}.args"

# 1 means don't do them
DO_FULL=1
DO_WINDOW=1
DO_MOST=1

# look for command line args to use
if [ -f "${TEST_ARGS}" ]; then
  # does it set --full?
  grep -q '\-f' "${TEST_ARGS}"
  DO_FULL=$?
  # does it set a window?
  grep -q '\-w' "${TEST_ARGS}"
  DO_WINDOW=$?
  # does it use most recent? (unused for now)
  grep -q '\-m' "${TEST_ARGS}"
  DO_MOST=$?
fi

# set up all the filenames we will generate in the TMP directory
TMP_BASE="${TMP}/${TEST}"
TMP_SPEC="${TMP_BASE}.nfer"
TMP_C="${TMP_BASE}.c"
TMP_L2_ERR="${TMP_BASE}.l2err"
TMP_MONITOR="${TMP_BASE}.monitor"

# copy the spec to the temp directory
cp "${TEST_SPEC}" "${TMP_SPEC}"
# generate the source - this will go into the tmp dir
"${BINARY}" -c "${TMP_SPEC}"
# now compute the configuration
"${BINARY}" -l2 "${TMP_SPEC}" < "${TEST_EVENTS}" >/dev/null 2>"${TMP_L2_ERR}"
# get just the end part we want
DEFINES=$(tail -n3 "${TMP_L2_ERR}")
# delete the defines from the C file
sed -e '/#define RULE_CACHE_SIZES/d' -e '/#define NEW_INTERVALS_SIZE/d' -e '/#define VALUE_DICTIONARY_SIZE/d' "${TMP_C}" > "${TMP_C}.stripped"
# add the defines to the top
printf "%s\n" "${DEFINES}" > "${TMP_C}"
cat "${TMP_C}.stripped" >> "${TMP_C}"

# now we need to set optimization arguments (full, window, most recent)
# if full is set, set it in the C file
if [ $DO_FULL -eq 0 ]; then
  # can't use the -i flag as it behaves differently between GNU/BSD sed
  sed 's/#define FULL_RESULTS 0/#define FULL_RESULTS 1/' "${TMP_C}" > "${TMP_C}.tmp"
  mv "${TMP_C}.tmp" "${TMP_C}"
fi
# if there is a window, figure it out and then set it
if [ $DO_WINDOW -eq 0 ]; then
  WINDOW=`sed -E -n 's/(-w|--window)[[:space:]]*([0-9]+).*/\2/p' "${TEST_ARGS}"`
  # can't use the -i flag as it behaves differently between GNU/BSD sed
  sed "s/#define WINDOW_SIZE 0/#define WINDOW_SIZE ${WINDOW}/" "${TMP_C}" > "${TMP_C}.tmp"
  mv "${TMP_C}.tmp" "${TMP_C}"
fi

# compile it
gcc -O3 -o "${TMP_MONITOR}" "${TMP_C}"
# now we can actually run the tests
# echo the name of the monitor

echo "${TMP_MONITOR}"