/*
 * test_static.h
 *
 *  Created on: Dec 13, 2021
 *      Author: skauffma
 *
 *   nfer - a system for inferring abstractions of event streams
 *   Copyright (C) 2017  Sean Kauffman
 *
 *   This file is part of nfer.
 *   nfer is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TEST_INC_DSL_TEST_HELPER_H_
#define TEST_INC_DSL_TEST_HELPER_H_

#include "dict.h"
#include "ast.h"

typedef struct _mock_ast {
    const char *a_str, *b_str, *c_str, *d_str, *x1_str, *foo_str, *bar_str, *x_str, *y_str, *boo_str;
    dictionary parser_dict, label_dict, name_dict, key_dict;
    ast_node *root;
    ast_node *d_rule_module, *d_rule_list, *d_rule, *b_rule_module, *b_rule_list, *b_rule;
    ast_node *a_before_x1, *b_before_c, *d_only;
    ast_node *option, *imports;
    ast_node *a_foo_eq_b_foo, *a_foo, *b_foo, *a_bar, *b_begin, *a_bar_eq_b_begin, *b_boo;
    ast_node *a_x, *a_y, *b_y1, *b_y2, *c_y, *b_y_eq_c_y, *a_x_and_eq, *a_y_eq_b_y, *and_and_eq;
    ast_node *a_begin, *b_end, *d_ends;
    word_id a_name, b_name, c_name, d_name, x1_name;
    word_id a_lab, b_lab, c_lab, d_lab;
    word_id foo_key, bar_key, x_key, y_key, boo_key;
} mock_ast;


void mock_parse(mock_ast *);
void mock_determine_labels(mock_ast *);
void mock_set_subfields(mock_ast *);
void mock_determine_fields(mock_ast *);
void setup_mock_ast(mock_ast *);
void teardown_mock_ast(mock_ast *);

#endif /* TEST_INC_DSL_TEST_HELPER_H_ */
