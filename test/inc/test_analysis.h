/*
 * test_analysis.h
 *
 *  Created on: July 27, 2022
 *      Author: skauffma
 *
 *    nfer - a system for inferring abstractions of event streams
 *   Copyright (C) 2022  Sean Kauffman
 *
 *   This file is part of nfer.
 *   nfer is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TEST_ANALYSIS_H_
#define TEST_ANALYSIS_H_

#include "nfer.h"
#include "analysis.h"

void test_compute_sccs(void);
void test_strongly_connected(void);
void test_generate_rule_digraph(void);
void test_setup_rule_order(void);
void test_exclusive_cycle(void);

/* these are for normally static functions in analysis.c */
void strongly_connected(nfer_rule *,
                        unsigned int *,
                        rule_digraph_vertex *, 
                        rule_digraph_edge *, 
                        unsigned int, 
                        rule_digraph_vertex **, 
                        unsigned int *,
                        unsigned int *);

bool generate_rule_digraph(nfer_specification *, 
                           rule_digraph_vertex **,
                           unsigned int *,
                           rule_digraph_edge **,
                           unsigned int *);

#endif /* TEST_ANALYSIS_H_ */
