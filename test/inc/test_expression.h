/*
 * test_eval.h
 *
 *  Created on: May 1, 2017
 *      Author: skauffma
 *
 *    nfer - a system for inferring abstractions of event streams
 *   Copyright (C) 2017  Sean Kauffman
 *
 *   This file is part of nfer.
 *   nfer is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TEST_INC_TEST_EXPRESSION_H_
#define TEST_INC_TEST_EXPRESSION_H_

void test_expression_from_input(void);
void test_expression_from_runtime(void);
void test_evaluate_expression(void);
void test_max_expression_depth(void);
void test_write_expression(void);

#endif /* TEST_INC_TEST_EXPRESSION_H_ */
