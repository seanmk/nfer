/*
 * test_file.h
 *
 *  Created on: June 30, 2021
 *      Author: skauffma
 *
 *    nfer - a system for inferring abstractions of event streams
 *   Copyright (C) 2017  Sean Kauffman
 *
 *   This file is part of nfer.
 *   nfer is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TEST_FILE_H_
#define TEST_FILE_H_

void test_parse_events(void);
void test_hard_timestamps(void);
void test_special_event_names(void);
void test_event_delimiters(void);
void test_parse_data(void);
void test_data_types(void);
void test_filter_events(void);
void test_filter_data(void);
void test_whitespace(void);
void test_value_edge_cases(void);

#endif /* TEST_FILE_H_ */
