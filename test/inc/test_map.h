/*
 * test_map.h
 *
 *  Created on: Apr 3, 2017
 *      Author: skauffma
 *
 *    nfer - a system for inferring abstractions of event streams
 *   Copyright (C) 2017  Sean Kauffman
 *
 *   This file is part of nfer.
 *   nfer is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TEST_INC_TEST_MAP_H_
#define TEST_INC_TEST_MAP_H_

void test_initialize_map(void);
void test_destroy_map(void);
void test_map_set(void);
void test_map_get(void);
void test_map_find(void);
void test_map_has_key(void);
void test_map_unset_key(void);
void test_empty_map_iterator(void);
void test_map_compare(void);

#endif /* TEST_INC_TEST_MAP_H_ */
