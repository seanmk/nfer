/*
 * test_nfer.h
 *
 *  Created on: Jan 26, 2017
 *      Author: skauffma
 *
 *    nfer - a system for inferring abstractions of event streams
 *   Copyright (C) 2017  Sean Kauffman
 *
 *   This file is part of nfer.
 *   nfer is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TEST_NFER_H_
#define TEST_NFER_H_

#include "pool.h"
#include "nfer.h"

void select_minimal(pool *, pool *);
bool interval_match(nfer_rule *, interval *, interval *);
void set_end_times(nfer_rule *, interval *, interval *, interval *);
void set_map(nfer_rule *, interval *, interval *, data_map *);
void discard_older_events(pool *, timestamp);
void write_rule(nfer_rule *, dictionary *, dictionary *, dictionary *, int);


void test_initialize_specification(void);
void test_destroy_specification(void);
void test_add_rule_to_specification(void);
void test_move_rule(void);

void test_apply_rule(void);
void test_run_nfer(void);
void test_minimality(void);
void test_select_minimal(void);
void test_sspss_intervals(void);
void test_prolog_intervals(void);
void test_beaglebone_intervals(void);
void test_nfer_dsl(void);

void test_is_subscribed(void);
void test_is_published(void);
void test_is_mapped(void);

void test_interval_match(void);
void test_set_end_times(void);
void test_set_map(void);
void test_discard_older_events(void);

void test_exclusive_rule(void);
void test_atomic_rules(void);
void test_apply_rule_list(void);

void test_write_rule(void);


#endif /* TEST_NFER_H_ */
