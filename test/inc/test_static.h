/*
 * test_static.h
 *
 *  Created on: Dec 13, 2021
 *      Author: skauffma
 *
 *   nfer - a system for inferring abstractions of event streams
 *   Copyright (C) 2017  Sean Kauffman
 *
 *   This file is part of nfer.
 *   nfer is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TEST_INC_TEST_STATIC_H_
#define TEST_INC_TEST_STATIC_H_

#include "ast.h"
#include "static.h"
#include "nfer.h"

void test_check_computes_ts(void);
void test_fold_constants(void);

/* these are for normally static functions in static.c */


#endif /* TEST_INC_TEST_STATIC_H_ */
