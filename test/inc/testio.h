/*
 * testio.h
 *
 *  Created on: June 29, 2021
 *      Author: skauffma
 *
 *    nfer - a system for inferring abstractions of event streams
 *   Copyright (C) 2017  Sean Kauffman
 *
 *   This file is part of nfer.
 *   nfer is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TESTIO_H_
#define TESTIO_H_

#include <stdio.h>

/**
 * These are implemented in testio.c
 **/
void * test_malloc(size_t);
void * test_realloc(void *, size_t);
void test_free(void *);

/**
 * The purpose of these macros is to overwrite malloc/realloc/free calls to something
 * that can be used in testing.
 **/
#define malloc(x)   test_malloc(x)
#define realloc(x,y)  test_realloc(x,y)
#define free(x)     test_free(x)

#endif /* TESTIO_H_ */
