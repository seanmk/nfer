#!/bin/sh

DIR="$( cd "$( dirname "${0}" )" && pwd )"
BINARY="${1}"
COMPILER="${DIR}/compile_test.sh"
RUNNER="${DIR}/run_c_test.sh"
RESULT=0
OUTPUT="Test	Result\n--------------------	-------\n"
ESC=$(printf '\033')

TMP="${DIR}/tmp"
# make the tmp dir if it doesn't exist
mkdir -p "${TMP}"

for file in "${DIR}"/functional/*.nfer; do
  TEST=`echo $file | sed -e 's/.*\///' -e 's/.nfer$//'`
  MONITOR=`"${COMPILER}" "${BINARY}" "${TEST}" "${TMP}"`
  OUTPUT="${OUTPUT}`"${RUNNER}" "${MONITOR}" "${TEST}"`\n"
  if [ $? != 0 ]; then
    RESULT=1
  fi
done

# clean up
rm -rf "${TMP}"

printf "${OUTPUT}" | column -t -s"	 " | sed "1{N;s/\(.*\)\n/$ESC[1m\1$ESC[0m\n/;}"

exit ${RESULT}
