#!/bin/sh

DIR="$( cd "$( dirname "${0}" )" && pwd )"

BINARY="${1}"
TEST="${2}"

TEST_DIR="${DIR}/functional"

printf "%s" "${TEST}	"

"${BINARY}" < "${TEST_DIR}/${TEST}.events" | "${DIR}/compare_results.sh" "${TEST_DIR}/${TEST}.result"

RETURN=$?

if [ ${RETURN} = 0 ]; then
  echo "\e[32m[PASS]\e[0m"
else
  echo "\e[1;31m[FAIL]\e[0m"
fi

exit ${RETURN}