#!/bin/sh

DIR="$( cd "$( dirname "${0}" )" && pwd )"

BINARY="${1}"
TEST="${2}"
ARGS=""

TEST_DIR="${DIR}/functional"

# look for command line args to use
if [ -f "${TEST_DIR}/${TEST}.args" ]; then
  ARGS=`cat "${TEST_DIR}/${TEST}.args"`
fi

printf "%s" "${TEST}	"

# echo "${BINARY} ${ARGS} ${TEST_DIR}/${TEST}.nfer < ${TEST_DIR}/${TEST}.events | "${DIR}/compare_results.sh" ${TEST_DIR}/${TEST}.result"

"${BINARY}" ${ARGS} "${TEST_DIR}/${TEST}.nfer" < "${TEST_DIR}/${TEST}.events" | "${DIR}/compare_results.sh" "${TEST_DIR}/${TEST}.result"

RETURN_STDIN=$?

"${BINARY}" ${ARGS} -e "${TEST_DIR}/${TEST}.events" "${TEST_DIR}/${TEST}.nfer" | "${DIR}/compare_results.sh" "${TEST_DIR}/${TEST}.result"

RETURN_FILE=$?

RETURN=$(($RETURN_STDIN | $RETURN_FILE))

if [ ${RETURN} = 0 ]; then
  echo "\e[32m[PASS]\e[0m"
else
  echo "\e[1;31m[FAIL]\e[0m"
fi

exit ${RETURN}
