/*
 * dsl_test_helper.c
 *
 *  Created on: December 13, 2021
 *      Author: skauffma
 *
 *   nfer - a system for inferring abstractions of event streams
 *   Copyright (C) 2017  Sean Kauffman
 *
 *   This file is part of nfer.
 *   nfer is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "types.h"
#include "ast.h"
#include "dsl.tab.h"

#include "dsl_test_helper.h"

/**
 * This is mock AST parsing and semantic analysis.
 **/
static location_type EMPTY_LOCATION = {0, 0, 0, 0};

void mock_parse(mock_ast *ast) {
    word_id a_id, b_id, c_id, d_id, foo_id, bar_id, x_id, y_id, boo_id;
    ast_node *a_node, *b_node, *c_node;

    // pretend to be the parser for interval expression a before (b before c)
    initialize_dictionary(&ast->parser_dict);
    a_id = add_word(&ast->parser_dict, ast->a_str);
    b_id = add_word(&ast->parser_dict, ast->b_str);
    c_id = add_word(&ast->parser_dict, ast->c_str);
    d_id = add_word(&ast->parser_dict, ast->d_str);

    a_node = new_atomic_interval_expr(WORD_NOT_FOUND, a_id, &EMPTY_LOCATION, &EMPTY_LOCATION);
    b_node = new_atomic_interval_expr(WORD_NOT_FOUND, b_id, &EMPTY_LOCATION, &EMPTY_LOCATION);
    c_node = new_atomic_interval_expr(WORD_NOT_FOUND, c_id, &EMPTY_LOCATION, &EMPTY_LOCATION);

    ast->b_before_c = new_binary_interval_expr(BEFORE, false, b_node, c_node);
    ast->a_before_x1 = new_binary_interval_expr(BEFORE, false, a_node, ast->b_before_c);
    ast->d_only = new_atomic_interval_expr(WORD_NOT_FOUND, d_id, &EMPTY_LOCATION, &EMPTY_LOCATION);

    // now pretend to be the parser for the expr a.foo = b.foo
    foo_id = add_word(&ast->parser_dict, ast->foo_str);
    ast->a_foo = new_map_field(a_id, foo_id, &EMPTY_LOCATION, &EMPTY_LOCATION);
    ast->b_foo = new_map_field(b_id, foo_id, &EMPTY_LOCATION, &EMPTY_LOCATION);
    ast->a_foo_eq_b_foo = new_binary_expr(EQ, ast->a_foo, ast->b_foo);

    // pretend to parse the expr a.bar = b.begin
    bar_id = add_word(&ast->parser_dict, ast->bar_str);
    ast->a_bar = new_map_field(a_id, bar_id, &EMPTY_LOCATION, &EMPTY_LOCATION);
    ast->b_begin = new_time_field(BEGINTOKEN, b_id, &EMPTY_LOCATION, &EMPTY_LOCATION);
    ast->a_bar_eq_b_begin = new_binary_expr(EQ, ast->a_bar, ast->b_begin);

    // pretend to parse the expr (a.x & (b.y = c.y)) & (a.y = b.y)
    x_id = add_word(&ast->parser_dict, ast->x_str);
    y_id = add_word(&ast->parser_dict, ast->y_str);
    ast->a_x = new_map_field(a_id, x_id, &EMPTY_LOCATION, &EMPTY_LOCATION);
    ast->a_y = new_map_field(a_id, y_id, &EMPTY_LOCATION, &EMPTY_LOCATION);
    ast->b_y1 = new_map_field(b_id, y_id, &EMPTY_LOCATION, &EMPTY_LOCATION);
    ast->b_y2 = new_map_field(b_id, y_id, &EMPTY_LOCATION, &EMPTY_LOCATION);
    ast->c_y = new_map_field(c_id, y_id, &EMPTY_LOCATION, &EMPTY_LOCATION);
    ast->b_y_eq_c_y = new_binary_expr(EQ, ast->b_y1, ast->c_y);
    ast->a_x_and_eq = new_binary_expr(AND, ast->a_x, ast->b_y_eq_c_y);
    ast->a_y_eq_b_y = new_binary_expr(EQ, ast->a_y, ast->b_y2);
    ast->and_and_eq = new_binary_expr(AND, ast->a_x_and_eq, ast->a_y_eq_b_y);

    // pretend to parse the expr b.boo
    boo_id = add_word(&ast->parser_dict, ast->boo_str);
    ast->b_boo = new_map_field(b_id, boo_id, &EMPTY_LOCATION, &EMPTY_LOCATION);

    // first set up a module with only an atomic rule b :- d
    ast->b_rule = new_rule(b_id, ast->d_only, NULL, NULL, NULL, &EMPTY_LOCATION);
    // no other rules
    ast->b_rule_list = new_rule_list(ast->b_rule, NULL);
    // now the module
    ast->b_rule_module = new_module_list(y_id, NULL, NULL, ast->b_rule_list, NULL, &EMPTY_LOCATION);

    // add endpoints 
    ast->a_begin = new_time_field(BEGINTOKEN, a_id, &EMPTY_LOCATION, &EMPTY_LOCATION);
    ast->b_end = new_time_field(ENDTOKEN, b_id, &EMPTY_LOCATION, &EMPTY_LOCATION);
    ast->d_ends = new_end_points(ast->a_begin, ast->b_end, &EMPTY_LOCATION);

    // pretend to be the parser for the rule 
    // d :- a before (b before c) where (a.x & (b.y = c.y)) & (a.y = b.y) begin a.begin end b.end
    // note: no map_expr_list or end_points!
    ast->d_rule = new_rule(d_id, ast->a_before_x1, ast->and_and_eq, NULL, ast->d_ends, &EMPTY_LOCATION);
    // now add it to a rule_list
    ast->d_rule_list = new_rule_list(ast->d_rule, NULL);
    // then add it to a module
    // first make an import list with the other module added
    ast->imports = new_import_list(y_id, NULL, &EMPTY_LOCATION);
    ast->option = new_option_flag(0, ast->imports, &EMPTY_LOCATION);
    ast->d_rule_module = new_module_list(x_id, ast->option, NULL, ast->d_rule_list, NULL, &EMPTY_LOCATION);
    // set the module list now that we have both
    ast->b_rule_module->module_list.tail = ast->d_rule_module;

    // set root, so we don't have to remember
    ast->root = ast->b_rule_module;
}
void mock_determine_labels(mock_ast *ast) {
    map_value bie_value;

    // both modules are imported
    ast->d_rule_module->module_list.imported = true;
    ast->b_rule_module->module_list.imported = true;

    // now set up the other data structures from semantic analysis
    // this one sets up the binary interval expressions
    initialize_dictionary(&ast->label_dict);
    initialize_dictionary(&ast->name_dict);
    ast->a_lab = add_word(&ast->label_dict, ast->a_str);
    ast->b_lab = add_word(&ast->label_dict, ast->b_str);
    ast->c_lab = add_word(&ast->label_dict, ast->c_str);
    ast->d_lab = add_word(&ast->label_dict, ast->d_str);
    ast->a_name = add_word(&ast->name_dict, ast->a_str);
    ast->b_name = add_word(&ast->name_dict, ast->b_str);
    ast->c_name = add_word(&ast->name_dict, ast->c_str);
    ast->d_name = add_word(&ast->name_dict, ast->d_str);
    ast->x1_name = add_word(&ast->name_dict, ast->x1_str);

    ast->d_rule->rule.result_id = ast->d_name;
    ast->b_rule->rule.result_id = ast->b_name;

    initialize_map(&ast->b_before_c->binary_interval_expr.left_label_map);
    initialize_map(&ast->b_before_c->binary_interval_expr.right_label_map);
    initialize_map(&ast->b_before_c->binary_interval_expr.left_field_map);
    initialize_map(&ast->b_before_c->binary_interval_expr.right_field_map);

    initialize_map(&ast->a_before_x1->binary_interval_expr.left_label_map);
    initialize_map(&ast->a_before_x1->binary_interval_expr.right_label_map);
    initialize_map(&ast->a_before_x1->binary_interval_expr.left_field_map);
    initialize_map(&ast->a_before_x1->binary_interval_expr.right_field_map);

    bie_value.type = pointer_type;
    bie_value.value.pointer = ast->b_before_c;
    map_set(&ast->b_before_c->binary_interval_expr.left_label_map, ast->b_lab, &bie_value);
    ast->b_before_c->binary_interval_expr.left->atomic_interval_expr.result_id = ast->b_name;
    ast->b_before_c->binary_interval_expr.left_name = ast->b_name;
    map_set(&ast->b_before_c->binary_interval_expr.right_label_map, ast->c_lab, &bie_value);
    ast->b_before_c->binary_interval_expr.right->atomic_interval_expr.result_id = ast->c_name;
    ast->b_before_c->binary_interval_expr.right_name = ast->c_name;

    bie_value.value.pointer = ast->a_before_x1;
    map_set(&ast->a_before_x1->binary_interval_expr.left_label_map, ast->a_lab, &bie_value);
    ast->a_before_x1->binary_interval_expr.left->atomic_interval_expr.result_id = ast->a_name;
    ast->a_before_x1->binary_interval_expr.left_name = ast->a_name;
    map_set(&ast->a_before_x1->binary_interval_expr.right_label_map, ast->b_lab, &bie_value);
    map_set(&ast->a_before_x1->binary_interval_expr.right_label_map, ast->c_lab, &bie_value);
    ast->a_before_x1->binary_interval_expr.right->atomic_interval_expr.result_id = ast->x1_name;
    ast->a_before_x1->binary_interval_expr.right_name = ast->x1_name;

    ast->d_only->atomic_interval_expr.result_id = ast->d_name;
}
void mock_set_subfields(mock_ast *ast) {
    // set the is_subfield field on map fields

    // first for a.foo = b.foo
    ast->b_foo->map_field.is_non_boolean = true;
    ast->a_foo->map_field.is_non_boolean = true;

    // then for a.bar = b.begin
    ast->a_bar->map_field.is_non_boolean = true;

    // then for (a.x & (b.y = c.y)) & (a.y = b.y)
    ast->a_x->map_field.is_non_boolean = false;
    ast->b_y1->map_field.is_non_boolean = true;
    ast->c_y->map_field.is_non_boolean = true;
    ast->a_y->map_field.is_non_boolean = true;
    ast->b_y2->map_field.is_non_boolean = true;

    // then for b.boo
    ast->b_boo->map_field.is_non_boolean = false;
}
void mock_determine_fields(mock_ast *ast) {
    // again, pretend to do the semantic analysis
    initialize_dictionary(&ast->key_dict);
    // first for a.foo = b.foo
    ast->foo_key = add_word(&ast->key_dict, ast->foo_str);
    ast->b_foo->map_field.resulting_map_key = ast->foo_key;
    ast->b_foo->map_field.interval_expression = ast->b_before_c;
    ast->b_foo->map_field.resulting_label_id = ast->b_lab;
    ast->a_foo->map_field.resulting_map_key = ast->foo_key;
    ast->a_foo->map_field.interval_expression = ast->a_before_x1;
    ast->a_foo->map_field.resulting_label_id = ast->a_lab;

    ast->a_foo_eq_b_foo->binary_expr.belongs_in = ast->a_before_x1;

    // then for a.bar = b.begin
    ast->bar_key = add_word(&ast->key_dict, ast->bar_str);
    ast->b_begin->time_field.interval_expression = ast->b_before_c;
    ast->b_begin->time_field.resulting_label_id = ast->b_lab;
    ast->a_bar->map_field.resulting_map_key = ast->bar_key;
    ast->a_bar->map_field.interval_expression = ast->a_before_x1;
    ast->a_bar->map_field.resulting_label_id = ast->a_lab;

    ast->a_bar_eq_b_begin->binary_expr.belongs_in = ast->a_before_x1;

    // then for (a.x & (b.y = c.y)) & (a.y = b.y)
    ast->x_key = add_word(&ast->key_dict, ast->x_str);
    ast->y_key = add_word(&ast->key_dict, ast->y_str);
    ast->a_x->map_field.resulting_map_key = ast->x_key;
    ast->a_x->map_field.interval_expression = ast->a_before_x1;
    ast->a_x->map_field.resulting_label_id = ast->a_lab;
    ast->a_y->map_field.resulting_map_key = ast->y_key;
    ast->a_y->map_field.interval_expression = ast->a_before_x1;
    ast->a_y->map_field.resulting_label_id = ast->a_lab;
    ast->b_y1->map_field.resulting_map_key = ast->y_key;
    ast->b_y1->map_field.interval_expression = ast->b_before_c;
    ast->b_y1->map_field.resulting_label_id = ast->b_lab;
    ast->b_y2->map_field.resulting_map_key = ast->y_key;
    ast->b_y2->map_field.interval_expression = ast->b_before_c;
    ast->b_y2->map_field.resulting_label_id = ast->b_lab;
    ast->c_y->map_field.resulting_map_key = ast->y_key;
    ast->c_y->map_field.interval_expression = ast->b_before_c;
    ast->c_y->map_field.resulting_label_id = ast->c_lab;

    ast->b_y_eq_c_y->binary_expr.belongs_in = ast->b_before_c;
    ast->a_x_and_eq->binary_expr.belongs_in = ast->a_before_x1;
    ast->a_y_eq_b_y->binary_expr.belongs_in = ast->a_before_x1;
    ast->and_and_eq->binary_expr.belongs_in = NULL;

    // then for b.boo
    ast->boo_key = add_word(&ast->key_dict, ast->boo_str);
    ast->b_boo->map_field.resulting_map_key = ast->boo_key;
    ast->b_boo->map_field.interval_expression = ast->b_before_c;
    ast->b_boo->map_field.resulting_label_id = ast->b_lab;
}


void setup_mock_ast(mock_ast *ast) {
    mock_parse(ast);
    mock_determine_labels(ast);
    mock_set_subfields(ast);
    mock_determine_fields(ast);
}

void teardown_mock_ast(mock_ast *ast) {
    free_node(ast->a_foo_eq_b_foo);
    free_node(ast->a_bar_eq_b_begin);
    free_node(ast->b_boo);

    //free_node(ast->a_before_x1);
    //free_node(ast->and_and_eq);
    free_node(ast->b_rule_module);

    destroy_dictionary(&ast->parser_dict);
    destroy_dictionary(&ast->label_dict);
    destroy_dictionary(&ast->name_dict);
    destroy_dictionary(&ast->key_dict);
}
