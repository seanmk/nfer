/*
 * test.c
 *
 *  Created on: Apr 5, 2012
 *      Author: skauffma
 *
 *    nfer - a system for inferring abstractions of event streams
 *   Copyright (C) 2017  Sean Kauffman
 *
 *   This file is part of nfer.
 *   nfer is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>
#include <inttypes.h>

#include "types.h"
#include "log.h"
#include "test.h"

// test headers
#include "test_dict.h"
#include "test_pool.h"
#include "test_map.h"
#include "test_stack.h"
#include "test_nfer.h"
#include "test_learn.h"
#include "test_expression.h"
#include "test_memory.h"
#include "test_strings.h"
#include "test_semantic.h"
#include "test_types.h"
#include "test_ast.h"
#include "test_file.h"
#include "test_static.h"
#include "test_analysis.h"
#include "test_lz4.h"

// importantly, this file must include headers for all tests

/**
 * The results for all tests.
 */
static int run;
static int passed;
static int failed;
static int asserts_for_test;

/**
 * Control variables for debugging tests
 */
static bool log_pass;

/**
 * Runs a series of tests to verify that the program is working properly.
 * Reports on success or failure to the console.  Halts execution if
 * failures are detected.
 *
 *
 * @example run_tests();
 */
int main(void) {
    // disable buffering on stdout and stderr
    setbuf(stdout, NULL);
    setbuf(stderr, NULL);
    // change log level to error to skip WARN messages in tests
    set_log_level(LOG_LEVEL_ERROR);
    // disable logging passed asserts
    log_pass = false;

    printf("|----------------------------------------------------------------------------|\n");
    printf("| Running Test Suite                                                         |\n");
    printf("|----------------------------------------------------------------------------|\n");

    // test_dictionary
    test_runner(test_initialize_dictionary, "test_initialize_dictionary");
    test_runner(test_destroy_dictionary, "test_destroy_dictionary");
    test_runner(test_add_word, "test_add_word");
    test_runner(test_get_word, "test_get_word");
    test_runner(test_dict_iterator, "test_dict_iterator");
    // test_pool
    test_runner(test_initialize_pool, "test_initialize_pool");
    test_runner(test_destroy_pool, "test_destroy_pool");
    test_runner(test_add_interval, "test_add_interval");
    test_runner(test_copy_pool, "test_copy_pool");
    test_runner(test_sort_pool, "test_sort_pool");
    test_runner(test_large_ts_sort, "test_large_ts_sort");
    test_runner(test_pool_iterator, "test_pool_iterator");
    test_runner(test_purge_pool, "test_purge_pool");
    test_runner(test_output_interval, "test_output_interval");
    test_runner(test_output_pool, "test_output_pool");
    test_runner(test_remove_hidden, "test_remove_hidden");
    test_runner(test_pool_queue_from_end, "test_pool_queue_from_end");
    test_runner(test_pool_queue_from_beginning, "test_pool_queue_from_beginning");
    test_runner(test_purge_length, "test_purge_length");
    test_runner(test_interval_added_after, "test_interval_added_after");
    test_runner(test_compare_intervals, "test_compare_intervals");
    test_runner(test_compare_equivalence, "test_compare_equivalence");
    // test_map
    test_runner(test_initialize_map, "test_initialize_map");
    test_runner(test_destroy_map, "test_destroy_map");
    test_runner(test_map_set, "test_map_set");
    test_runner(test_map_get, "test_map_get");
    test_runner(test_map_find, "test_map_find");
    test_runner(test_map_has_key, "test_map_has_key");
    test_runner(test_map_unset_key, "test_map_unset_key");
    test_runner(test_empty_map_iterator, "test_empty_map_iterator");
    test_runner(test_map_compare, "test_map_compare");
    // test_stack
    test_runner(test_initialize_stack, "test_initialize_stack");
    test_runner(test_destroy_stack, "test_destroy_stack");
    test_runner(test_push, "test_push");
    test_runner(test_pop, "test_pop");
    // test_nfer
    test_runner(test_initialize_specification, "test_initialize_specification");
    test_runner(test_destroy_specification, "test_destroy_specification");
    test_runner(test_add_rule_to_specification, "test_add_rule_to_specification");
    test_runner(test_move_rule, "test_move_rule");
    test_runner(test_apply_rule, "test_apply_rule");
    test_runner(test_run_nfer, "test_run_nfer");
    test_runner(test_minimality, "test_minimality");
    test_runner(test_select_minimal, "test_select_minimal");
    test_runner(test_sspss_intervals, "test_sspss_intervals");
    test_runner(test_prolog_intervals, "test_prolog_intervals");
    test_runner(test_beaglebone_intervals, "test_beaglebone_intervals");
    test_runner(test_nfer_dsl, "test_nfer_dsl");
    test_runner(test_is_subscribed, "test_is_subscribed");
    test_runner(test_is_published, "test_is_published");
    test_runner(test_is_mapped, "test_is_mapped");
    test_runner(test_interval_match, "test_interval_match");
    test_runner(test_set_end_times, "test_set_end_times");
    test_runner(test_set_map, "test_set_map");
    test_runner(test_discard_older_events, "test_discard_older_events");
    test_runner(test_exclusive_rule, "test_exclusive_rule");
    test_runner(test_atomic_rules, "test_atomic_rules");
    test_runner(test_apply_rule_list, "test_apply_rule_list");
    test_runner(test_write_rule, "test_write_rule");

    // test_learn (adding a comment...)
    test_runner(test_initialize_learning, "test_initialize_learning");
    test_runner(test_destroy_learning, "test_destroy_learning");
    test_runner(test_add_learned_rules, "test_add_learned_rules");
    test_runner(test_finish_learning, "test_finish_learning");
    test_runner(test_learn_interval, "test_learn_interval");

    // test_expression
    test_runner(test_expression_from_input, "test_expression_from_input");
    test_runner(test_expression_from_runtime, "test_expression_from_runtime");
    test_runner(test_evaluate_expression, "test_evaluate_expression");
    test_runner(test_max_expression_depth, "test_max_expression_depth");
    test_runner(test_write_expression, "test_write_expression");
    // test_memory
    test_runner(test_set_memory, "test_set_memory");
    test_runner(test_clear_memory, "test_clear_memory");
    test_runner(test_copy_memory, "test_copy_memory");
    // test_strings
    test_runner(test_copy_string, "test_copy_string");
    test_runner(test_string_equals, "test_string_equals");
    test_runner(test_string_length, "test_string_length");
    test_runner(test_string_to_u64, "test_string_to_u64");
    test_runner(test_string_to_i64, "test_string_to_i64");
    test_runner(test_string_to_double, "test_string_to_double");
    // test_semantic
    test_runner(test_set_field_mapping_per_rule, "test_set_field_mapping_per_rule");
    test_runner(test_set_time_mapping_per_rule, "test_set_time_mapping_per_rule");
    test_runner(test_remap_field_or_time_mappings, "test_remap_field_or_time_mappings");
    test_runner(test_expr_references_ie, "test_expr_references_bie");
    test_runner(test_remap_nested_boolean, "test_remap_nested_boolean");
    test_runner(test_remap_field_complex, "test_remap_field_complex");
    test_runner(test_set_map_boolean_type, "test_set_map_boolean_type");
    test_runner(test_set_imported, "test_set_imported");
    test_runner(test_propagate_constants, "test_propagate_constants");
    test_runner(test_populate_constant_map, "test_populate_constant_map");
    test_runner(test_propagate_to_rule_list, "test_propagate_to_rule_list");
    test_runner(test_propagate_to_expr, "test_propagate_to_expr");
    // test_types
    test_runner(test_equals, "test_equals");
    test_runner(test_real_equals, "test_real_equals");
    test_runner(test_compare_typed_values, "test_compare_typed_values");
    // test_ast
    test_runner(test_free_node, "test_free_node");
    test_runner(test_copy_ast, "test_copy_ast");
    // test_file
    test_runner(test_parse_events, "test_parse_events");
    test_runner(test_hard_timestamps, "test_hard_timestamps");
    test_runner(test_special_event_names, "test_special_event_names");
    test_runner(test_event_delimiters, "test_event_delimiters");
    test_runner(test_parse_data, "test_parse_data");
    test_runner(test_data_types, "test_data_types");
    test_runner(test_filter_events, "test_filter_events");
    test_runner(test_filter_data, "test_filter_data");
    test_runner(test_whitespace, "test_whitespace");
    test_runner(test_value_edge_cases, "test_value_edge_cases");
    // test_static
    test_runner(test_check_computes_ts, "test_check_computes_ts");
    test_runner(test_fold_constants, "test_fold_constants");
    // test_analysis
    test_runner(test_compute_sccs, "test_compute_sccs");
    test_runner(test_strongly_connected, "test_strongly_connected");
    test_runner(test_generate_rule_digraph, "test_generate_rule_digraph");
    test_runner(test_setup_rule_order, "test_setup_rule_order");
    test_runner(test_exclusive_cycle, "test_exclusive_cycle");
    // test_lz4
    test_runner(test_lz4decode, "test_lz4decode");

    printf("|----------------------------------------------------------------------------|\n");
    printf("| Run:\t\t%d\n| Passed:\t%d\n| Failed:\t%d\n", run, passed, failed);
    printf("|----------------------------------------------------------------------------|\n");

    return failed;
}

void test_runner(void (*test_func)(void), const char *name) {
    int startRun;
    int startFailed;

    // init assertions for this test
    asserts_for_test = 0;

    // get the starting values of run and failed
    startRun = run;
    startFailed = failed;

    // print the test name, so it is clear what failed if there is an error
    printf("  Test: %34s\t", name);
    // flush immediately since we aren't ending in a newline and we want to know where failures occurred more easily
    fflush(stdout);

    // run the test
    test_func();

    // compare the values and print a message about the success or failure
    if (run <= startRun) {
        printf("\033[33m[SKIP]\033[0m\n");
    } else if (failed == startFailed) {
        printf("\033[32m[PASS]\033[0m (%d)\n", run-startRun);
    } else {
        printf("\033[1;31m[FAIL]\033[0m (%d/%d)\n", failed-startFailed, run-startRun);
    }
}

static bool test(const char *msg, bool val, char *expected, char *found) {
    asserts_for_test = asserts_for_test + 1;

    if (val) {
        passed = passed + 1;
        // if log_pass is set, log a message about the passed assert
        // this is helpful for tests that are failing from crashes
        if (log_pass) {
            printf("\n\033[32mSuccess (%d):\033[0m %s. Value was <%s>.", asserts_for_test,
                    msg, expected);
            // flush immediately since we aren't ending in a newline and we want to know where failures occurred more easily
            fflush(stdout);
        }
    } else {
        failed = failed + 1;
        printf("\n\033[1;31mFailure (%d):\033[0m %s. Expected <%s> but was <%s>.", asserts_for_test,
                msg, expected, found);
        // flush immediately since we aren't ending in a newline and we want to know where failures occurred more easily
        fflush(stdout);
    }

    run = run + 1;
    return val;
}

void assert_true(const char *msg, bool val) {
    test(msg, val, "true", "false");
}

void assert_false(const char *msg, bool val) {
    test(msg, !val, "false", "true");
}

void assert_null(const char *msg, void *val) {
    test(msg, (val == NULL ), "null", "not null");
}

void assert_not_null(const char *msg, void *val) {
    test(msg, (val != NULL ), "not null", "null");
}

void assert_str_equals(const char *msg, char *val0, char *val1) {
    test(msg, (strcmp(val0, val1) == 0), val0, val1);
}

void assert_int_equals(const char *msg, uint64_t val0, uint64_t val1) {
    /* 21 chars max for -9223372036854775808, plus 1 for null terminator */
    #define INT_BUFFER_SIZE 22
    char buf0[INT_BUFFER_SIZE];
    char buf1[INT_BUFFER_SIZE];
    snprintf(&buf0[0], INT_BUFFER_SIZE, "%" PRIu64, val0);
    snprintf(&buf1[0], INT_BUFFER_SIZE, "%" PRIu64, val1);

    test(msg, (val0 == val1), buf0, buf1);
}

void assert_ptr_equals(const char *msg, void *val0, void *val1) {
    /* 18 chars max, plus 2 for the 0x, plus 1 for null terminator */
    #define PTR_BUFFER_SIZE 21
    char buf0[PTR_BUFFER_SIZE];
    char buf1[PTR_BUFFER_SIZE];
    snprintf(&buf0[0], PTR_BUFFER_SIZE, "%p", val0);
    snprintf(&buf1[0], PTR_BUFFER_SIZE, "%p", val1);

    test(msg, (val0 == val1), buf0, buf1);
}

void assert_float_equals(const char *msg, double val0, double val1) {
    #define FLT_BUFFER_SIZE 23
    // more or less equals...
    const double epsilon = 0.00000001;
    /* 20 chars max, plus 2 for 0. maybe?, plus 1 for null terminator */
    char buf0[FLT_BUFFER_SIZE];
    char buf1[FLT_BUFFER_SIZE];
    snprintf(&buf0[0], FLT_BUFFER_SIZE, "%20f", val0);
    snprintf(&buf1[0], FLT_BUFFER_SIZE, "%20f", val1);

    test(msg, ((val0 >= val1 - epsilon) && (val0 <= val1 + epsilon)), buf0, buf1);
}

