/*
 * test_analysis.c
 *
 *  Created on: July 27, 2022
 *      Author: skauffma
 *
 *    nfer - a system for inferring abstractions of event streams
 *   Copyright (C) 2022  Sean Kauffman
 *
 *   This file is part of nfer.
 *   nfer is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdlib.h>

#include "analysis.h"
#include "test_analysis.h"
#include "test.h"

#include "nfer.h"
#include "memory.h"

static rule_digraph_vertex *vertices;
static rule_digraph_edge *edges;
static nfer_specification spec, result_spec;
static nfer_rule *rules, *result;

static label a = 1, b = 2, c = 3, d = 4, e = 5, f = 6, g = 7;

//////////////////////// helper functions to mock graphs

static void allocate_memory(unsigned int vertex_count, unsigned int edge_count) {
    unsigned int i;

    vertices = (rule_digraph_vertex *)malloc(sizeof(rule_digraph_vertex) * vertex_count);
    edges = (rule_digraph_edge *)malloc(sizeof(rule_digraph_edge) * edge_count);
    // use an actual spec to set up the rules space
    initialize_specification(&spec, vertex_count);
    // set rules to point to the spec rules
    rules = spec.rules;
    // set its size, too
    spec.size = vertex_count;
    // also allocate space for the reordered rules
    initialize_specification(&result_spec, vertex_count);
    // set rules to point to the spec rules
    result = result_spec.rules;
    // set its size, too
    result_spec.size = vertex_count;

    // set up the maps
    for (i = 0; i < vertex_count; i++) {
        rules[i].map_expressions = EMPTY_MAP;
        result[i].map_expressions = EMPTY_MAP;
    }
    // result = (nfer_rule *)malloc(sizeof(nfer_rule) * vertex_count);
    // clear_memory(result, sizeof(nfer_rule) * vertex_count);
    // // not checking null in tests, natch
}

// r0 = b :- a x a (depends on r2)
// r1 = d :- a x e (depends on r2)
// r2 = a :- b x c (depends on r0)
#define SIMPLE_VERTEX_COUNT 3
#define SIMPLE_EDGE_COUNT   3
static void mock_simple_digraph(void) {
    int i;

    allocate_memory(SIMPLE_VERTEX_COUNT, SIMPLE_EDGE_COUNT);

    // setup rules/vertices
    i = 2;
    rules[i].result_label = a;
    rules[i].left_label = b;
    rules[i].right_label = c;
    vertices[i].rule = &rules[i];
    vertices[i].visited = false;

    i = 0;
    rules[i].result_label = b;
    rules[i].left_label = a;
    rules[i].right_label = a;
    vertices[i].rule = &rules[i];
    vertices[i].visited = false;

    i = 1;
    rules[i].result_label = d;
    rules[i].left_label = a;
    rules[i].right_label = e;
    vertices[i].rule = &rules[i];
    vertices[i].visited = false;

    // now the edges
    edges[0].from = &vertices[0];
    edges[0].to   = &vertices[2];
    edges[1].from = &vertices[2];
    edges[1].to   = &vertices[0];
    edges[2].from = &vertices[2];
    edges[2].to   = &vertices[1];
}

// r0 = a :- b x c (depends on r2)
// r1 = g :- b x f (depends on r2 and r3)
// r2 = b :- a x d (depends on r0)
// r3 = f :- a x e (depends on r0)
#define LARGER_VERTEX_COUNT 4
#define LARGER_EDGE_COUNT   5
static void mock_larger_digraph(void) {
    int i;

    allocate_memory(LARGER_VERTEX_COUNT, LARGER_EDGE_COUNT);

    // setup rules/vertices
    i = 0;
    rules[i].result_label = a;
    rules[i].left_label = b;
    rules[i].right_label = c;
    vertices[i].rule = &rules[i];
    vertices[i].visited = false;

    i = 2;
    rules[i].result_label = b;
    rules[i].left_label = a;
    rules[i].right_label = d;
    vertices[i].rule = &rules[i];
    vertices[i].visited = false;

    i = 3;
    rules[i].result_label = f;
    rules[i].left_label = a;
    rules[i].right_label = e;
    vertices[i].rule = &rules[i];
    vertices[i].visited = false;

    i = 1;
    rules[i].result_label = g;
    rules[i].left_label = b;
    rules[i].right_label = f;
    vertices[i].rule = &rules[i];
    vertices[i].visited = false;

    // now the edges
    edges[0].from = &vertices[0];
    edges[0].to   = &vertices[2];
    edges[1].from = &vertices[0];
    edges[1].to   = &vertices[3];
    edges[2].from = &vertices[2];
    edges[2].to   = &vertices[0];
    edges[3].from = &vertices[2];
    edges[3].to   = &vertices[1];
    edges[4].from = &vertices[3];
    edges[4].to   = &vertices[1];
}

static void teardown(void) {
    free(vertices);
    free(edges);
    destroy_specification(&spec);
    destroy_specification(&result_spec);
}

void test_compute_sccs(void) {
    bool success;

    // start by checking the simple case
    mock_simple_digraph();

    // call it
    success = compute_sccs(result, vertices, SIMPLE_VERTEX_COUNT, edges, SIMPLE_EDGE_COUNT);
    assert_true("Compute should succeed", success);

    // now check that the result is populated in the correct order
    // just check the result labels to see which one went where
    assert_int_equals("Simple: 0 should be the root of the cycle", b, result[0].result_label);
    assert_int_equals("Simple: 1 should be the other cycle", a, result[1].result_label);
    assert_int_equals("Simple: 2 should be the non-cycle", d, result[2].result_label);
    // check the cycle_roots
    assert_int_equals("Simple: 0 should have cycle_size set", 1, result[0].cycle_size);
    assert_int_equals("Simple: 1 should have cycle_size cleared", 0, result[1].cycle_size);
    assert_int_equals("Simple: 2 should have cycle_size cleared", 0, result[2].cycle_size);

    teardown();

    // now check the larger example
    mock_larger_digraph();

    // call it
    success = compute_sccs(result, vertices, LARGER_VERTEX_COUNT, edges, LARGER_EDGE_COUNT);
    assert_true("Compute should succeed", success);

    // now check that the result is populated in the correct order
    // just check the result labels to see which one went where
    assert_int_equals("Larger: 0 should be the root of the cycle", a, result[0].result_label);
    assert_int_equals("Larger: 1 should be the other cycle", b, result[1].result_label);
    // these two are really checking the topological sort as g depends on f as well as b
    assert_int_equals("Larger: 2 should be first non-cycle", f, result[2].result_label);
    assert_int_equals("Larger: 3 should be second non-cycle", g, result[3].result_label);
    // check the cycle_roots
    assert_int_equals("Larger: 0 should have cycle_size set", 1, result[0].cycle_size);
    assert_int_equals("Larger: 1 should have cycle_size cleared", 0, result[1].cycle_size);
    assert_int_equals("Larger: 2 should have cycle_size cleared", 0, result[2].cycle_size);
    assert_int_equals("Larger: 3 should have cycle_size cleared", 0, result[3].cycle_size);

    teardown();   
}

void test_strongly_connected(void) {
    rule_digraph_vertex **stack;
    unsigned int index, tos, result_index;

    // start with the simple example
    mock_simple_digraph();
    stack = (rule_digraph_vertex **)malloc(sizeof(rule_digraph_vertex *) * SIMPLE_VERTEX_COUNT);

    index = 0;
    tos = 0;
    result_index = SIMPLE_VERTEX_COUNT;

    // check only the setup of the vertex data structure and the SCC output
    // to do this, we only have to skip iterating over the edges by setting
    // the edge count to zero
    strongly_connected(result, &result_index, 
                       &vertices[0],
                       edges, 0,
                       stack, &tos,
                       &index);
    
    // first check that the vertex data structure was set up correctly
    assert_int_equals("Vertex index is wrong", 0, vertices[0].index);
    assert_int_equals("Vertex lowlink is wrong", 0, vertices[0].lowlink);
    assert_false("Vertex onstack should be false", vertices[0].on_stack);
    assert_true("Vertex visited should be true", vertices[0].visited);
    // now check the state variables
    assert_int_equals("Next index is wrong", 1, index);
    assert_int_equals("Stack should be empty", 0, tos);
    assert_int_equals("Result index is wrong", SIMPLE_VERTEX_COUNT - 1, result_index);
    // finally, check the resulting rule
    assert_int_equals("Rule is wrong", vertices[0].rule->result_label, result[SIMPLE_VERTEX_COUNT-1].result_label);
    assert_int_equals("Cycle size should be clear", 0, result[SIMPLE_VERTEX_COUNT-1].cycle_size);

    teardown();

    free(stack);
}

void test_generate_rule_digraph(void) {
    rule_digraph_vertex *simple_vertices, *larger_vertices;
    rule_digraph_edge   *simple_edges, *larger_edges;
    unsigned int simple_vertex_count, larger_vertex_count, simple_edge_count, larger_edge_count;
    bool success;
    unsigned int i;

    // to see if it generates the right digraph, we will just compare to the mock examples
    // that were generated by hand
    mock_simple_digraph();

    // now call the function with our unallocated pointers and the mocked spec
    success = generate_rule_digraph(&spec, &simple_vertices, &simple_vertex_count, &simple_edges, &simple_edge_count);
    assert_true("Generate should succeed", success);
    // first check that the vertices are right
    assert_int_equals("Simple: Wrong number of vertices", SIMPLE_VERTEX_COUNT, simple_vertex_count);
    for (i = 0; i < simple_vertex_count; i++) {
        assert_ptr_equals("Simple: Vertex points to wrong rule", vertices[i].rule, simple_vertices[i].rule);
        assert_false("Simple: visited should be false", simple_vertices[i].visited);
        assert_false("Simple: on_stack should be false", simple_vertices[i].on_stack);
    }
    // then check edges
    assert_int_equals("Simple: Wrong number of edges", SIMPLE_EDGE_COUNT, simple_edge_count);
    for (i = 0; i < simple_edge_count; i++) {
        assert_ptr_equals("Simple: From points to wrong vertex", edges[i].from->rule, simple_edges[i].from->rule);
        assert_ptr_equals("Simple: To points to wrong vertex", edges[i].to->rule, simple_edges[i].to->rule);
    }

    // clean up
    free(simple_vertices);
    free(simple_edges);
    teardown();

    // now check the larger example
    mock_larger_digraph();

    // now call the function with our unallocated pointers and the mocked spec
    success = generate_rule_digraph(&spec, &larger_vertices, &larger_vertex_count, &larger_edges, &larger_edge_count);
    assert_true("Generate should succeed", success);
    // first check that the vertices are right
    assert_int_equals("Larger: Wrong number of vertices", LARGER_VERTEX_COUNT, larger_vertex_count);
    for (i = 0; i < larger_vertex_count; i++) {
        assert_ptr_equals("Larger: Vertex points to wrong rule", vertices[i].rule, larger_vertices[i].rule);
        assert_false("Larger: visited should be false", larger_vertices[i].visited);
        assert_false("Larger: on_stack should be false", larger_vertices[i].on_stack);
    }
    // then check edges
    assert_int_equals("Larger: Wrong number of edges", LARGER_EDGE_COUNT, larger_edge_count);
    for (i = 0; i < larger_edge_count; i++) {
        assert_ptr_equals("LARGER: From points to wrong vertex", edges[i].from->rule, larger_edges[i].from->rule);
        assert_ptr_equals("LARGER: To points to wrong vertex", edges[i].to->rule, larger_edges[i].to->rule);
    }

    // clean up
    free(larger_vertices);
    free(larger_edges);
    destroy_map(&spec.equivalent_labels);
    teardown();
}

void test_setup_rule_order(void) {
    bool success;
    map_key key;
    map_value value, check;

    // pretty much just set up a spec and see if it is correctly modified
    mock_simple_digraph();

    // go ahead and set something in the map, just so we can verify it's preserved
    value.type = string_type;
    value.value.string = 5;
    key = 3;
    map_set(&spec.equivalent_labels, key, &value);

    success = setup_rule_order(&spec);
    assert_true("Setup rules should succeed", success);

    assert_int_equals("Simple: 0 should be the root of the cycle", b, spec.rules[0].result_label);
    assert_int_equals("Simple: 1 should be the other cycle", a, spec.rules[1].result_label);
    assert_int_equals("Simple: 2 should be the non-cycle", d, spec.rules[2].result_label);

    assert_int_equals("Simple: size is wrong", SIMPLE_VERTEX_COUNT, spec.size);
    assert_int_equals("Simple: space is wrong", SIMPLE_VERTEX_COUNT, spec.space);

    assert_false("Equivalent labels should not be empty", is_map_empty(&spec.equivalent_labels));
    map_get(&spec.equivalent_labels, key, &check);
    assert_int_equals("Wrong equivalence mapping type", value.type, check.type);
    assert_int_equals("Wrong equivalence mapping value", value.value.string, check.value.string);

    teardown();

    // now do the larger spec
    mock_larger_digraph();

    // spec
    spec.rules = rules;
    spec.size  = LARGER_VERTEX_COUNT;
    spec.space = LARGER_VERTEX_COUNT;

    success = setup_rule_order(&spec);
    assert_true("Setup rules should succeed", success);

    assert_int_equals("Larger: 0 should be the root of the cycle", a, spec.rules[0].result_label);
    assert_int_equals("Larger: 1 should be the other cycle", b, spec.rules[1].result_label);
    // these two are really checking the topological sort as g depends on f as well as b
    assert_int_equals("Larger: 2 should be first non-cycle", f, spec.rules[2].result_label);
    assert_int_equals("Larger: 3 should be second non-cycle", g, spec.rules[3].result_label);

    assert_int_equals("Larger: size is wrong", LARGER_VERTEX_COUNT, spec.size);
    assert_int_equals("Larger: space is wrong", LARGER_VERTEX_COUNT, spec.space);

    teardown();

    // teardown the local spec
    destroy_map(&spec.equivalent_labels);
}

void test_exclusive_cycle(void) {
    bool result;

    // just allocate some memory as we don't need anything mocked except cycles and exclusive
    // we allocate an edge since malloc may not like zero size allocations
    allocate_memory(5,1);

    // set up a spec with a cycle and two exclusive rules but no problems
    rules[0].cycle_size = 0;
    rules[0].exclusion = true;
    rules[0].left_label = 2;
    rules[0].right_label = 1;
    rules[1].cycle_size = 2;
    rules[2].cycle_size = 1;
    rules[3].cycle_size = 0;
    rules[4].cycle_size = 0;
    rules[4].exclusion = true;
    rules[4].left_label = 2;
    rules[4].right_label = 1;

    result = exclusive_cycle(&spec);
    assert_false("No exclusive rules should be detected in a cycle", result);

    // now set the first rule in the cycle to exclusive
    rules[1].exclusion = true;

    result = exclusive_cycle(&spec);
    assert_true("Exclusive rule should be detected at beginning of cycle", result);

    // now try the end of the cycle
    rules[1].exclusion = false;
    rules[3].exclusion = true;

    result = exclusive_cycle(&spec);
    assert_true("Exclusive rule should be detected at end of cycle", result);

    // now modify the fifth rule so it is actually a cycle with itself
    rules[3].exclusion = false;
    rules[4].result_label = 1;
    
    result = exclusive_cycle(&spec);
    assert_true("Exclusive rule should be detected in single rule cycle", result);

    teardown();
}
