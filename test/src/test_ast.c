/*
 * test_ast.c
 *
 *  Created on: June 29, 2021
 *      Author: skauffma
 *
 *    nfer - a system for inferring abstractions of event streams
 *   Copyright (C) 2017  Sean Kauffman
 *
 *   This file is part of nfer.
 *   nfer is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "ast.h"
#include "test.h"
#include "dsl.tab.h"

static location_type EMPTY_LOCATION = {0, 0, 0, 0};
static dictionary parser_dict;
static const char *a_str = "a", *b_str = "b", *c_str = "c", *x_str = "x", *y_str = "y", *foo_str = "foo";
static word_id a_id, b_id, c_id, x_id, y_id, foo_id;
static ast_node *int_1, *real_314, *string_foo, *bool_true;
static ast_node *b_foo, *c_foo, *foo_ref, *c_end;
static ast_node *begin_1_end_c_end, *map_foo_true;
static ast_node *c_foo_eq_foo, *b_foo_eq_314, *not_eq, *where_and;
static ast_node *b_atomic, *c_atomic, *b_before_c;
static ast_node *rule, *rule_list;
static ast_node *option, *import_list, *module_list;
static ast_node *constant, *const_expr;

// from the testio malloc code
extern size_t allocated;

static void setup(void) {
    /**
     * module x {
     *   import y;
     *   foo = "foo"
     *   a :- b before c where b.foo = 3.14 & !(c.foo = foo) map { foo -> true } begin 1 end c.end
     * }
     **/

    // add all the strings we need to the parser dictionary first
    initialize_dictionary(&parser_dict);
    a_id = add_word(&parser_dict, a_str);
    b_id = add_word(&parser_dict, b_str);
    c_id = add_word(&parser_dict, c_str);
    x_id = add_word(&parser_dict, x_str);
    y_id = add_word(&parser_dict, y_str);
    foo_id = add_word(&parser_dict, foo_str);

    // we're doing it bottom up, so start with the leaf nodes
    // first the literals
    int_1 = new_int_literal(1L, &EMPTY_LOCATION);
    real_314 = new_float_literal(3.13, &EMPTY_LOCATION);
    string_foo = new_string_literal(foo_id, &EMPTY_LOCATION);
    bool_true = new_boolean_literal(true, &EMPTY_LOCATION);

    // then map and time fields
    b_foo = new_map_field(b_id, foo_id, &EMPTY_LOCATION, &EMPTY_LOCATION);
    c_foo = new_map_field(c_id, foo_id, &EMPTY_LOCATION, &EMPTY_LOCATION);
    c_end = new_time_field(ENDTOKEN, c_id, &EMPTY_LOCATION, &EMPTY_LOCATION);

    // now add an endpoints expr
    begin_1_end_c_end = new_end_points(int_1, c_end, &EMPTY_LOCATION);
    // add a map expr list
    map_foo_true = new_map_expr_list(foo_id, bool_true, NULL, &EMPTY_LOCATION);
    
    // add the constant reference
    foo_ref = new_constant_reference(foo_id, &EMPTY_LOCATION);

    // for the where expr, we need a tree - start with the two binary = expressions
    c_foo_eq_foo = new_binary_expr(EQ, c_foo, foo_ref);
    b_foo_eq_314 = new_binary_expr(EQ, b_foo, real_314);
    // now a unary expression around the right side
    not_eq = new_unary_expr(BANG, c_foo_eq_foo, &EMPTY_LOCATION);
    // now combine them with and
    where_and = new_binary_expr(AND, b_foo_eq_314, not_eq);

    // now we need the interval expressions
    b_atomic = new_atomic_interval_expr(WORD_NOT_FOUND, b_id, &EMPTY_LOCATION, NULL);
    c_atomic = new_atomic_interval_expr(WORD_NOT_FOUND, c_id, &EMPTY_LOCATION, NULL);
    b_before_c = new_binary_interval_expr(BEFORE, false, b_atomic, c_atomic);

    // add the rule
    rule = new_rule(a_id, b_before_c, where_and, map_foo_true, begin_1_end_c_end, &EMPTY_LOCATION);
    rule_list = new_rule_list(rule, NULL);

    // add the import list
    import_list = new_import_list(y_id, NULL, &EMPTY_LOCATION);
    option = new_option_flag(LOUD, import_list, &EMPTY_LOCATION);
    // add a constant
    const_expr = string_foo;
    constant = new_named_constant(foo_id, const_expr, NULL, &EMPTY_LOCATION);
    // this is the root node!
    module_list = new_module_list(x_id, option, constant, rule_list, NULL, &EMPTY_LOCATION);
}

static void teardown(void) {
    destroy_dictionary(&parser_dict);
}


void test_free_node(void) {
    size_t prior_to_allocation;
    
    // store the size of allocation before calling setup
    prior_to_allocation = allocated;

    // construct an AST instance using every type of node
    setup();

    // make sure we're sane
    assert_true("Should have allocated something", (allocated > prior_to_allocation));

    // now tear it down (call it explicitly since this is what we're testing)
    free_node(module_list);

    // that should have freed everything!
    assert_int_equals("Should have freed all memory from the AST", prior_to_allocation, allocated);

    teardown();
}

void test_copy_ast(void) {
    size_t prior_to_allocation, after_allocation;
    ast_node *new_root;
    ast_node *rule, *and, *not, *eq;
    
    // store the size of allocation before calling setup
    prior_to_allocation = allocated;

    setup();

    // record allocated memory
    after_allocation = allocated;

    new_root = copy_ast(module_list);

    // this is admittedly a lazy way to test, but the alternative is to walk the whole tree
    // the beauty of this is that, if the tree wasn't properly constructed, then the
    // free_node step wouldn't deallocate correctly, and if any nodes were missing then
    // the copy wouldn't allocate the same amount of memory.
    assert_int_equals("Should have allocated the same amount of memory", after_allocation - prior_to_allocation, allocated - after_allocation);

    // the only thing really to test here is if the leaves were correctly set up
    assert_int_equals("Should be module", type_module_list, new_root->type);
    assert_int_equals("Should be rule list", type_rule_list, new_root->module_list.rules->type);
    rule = new_root->module_list.rules->rule_list.head;
    assert_int_equals("Should be rule", type_rule, rule->type);
    and = rule->rule.where_expr;
    assert_int_equals("Should be binary expr", type_binary_expr, and->type);
    assert_int_equals("Should be AND", AND, and->binary_expr.operator);
    not = and->binary_expr.right;
    assert_int_equals("Should be unary", type_unary_expr, not->type);
    assert_int_equals("Should be not", BANG, not->unary_expr.operator);
    eq = not->unary_expr.operand;
    assert_int_equals("Should be binary expr", type_binary_expr, eq->type);
    assert_int_equals("Should be equals", EQ, eq->binary_expr.operator);

    assert_int_equals("Should be map field", type_map_field, eq->binary_expr.left->type);
    assert_int_equals("Map should refer to c", c_id, eq->binary_expr.left->map_field.label);
    assert_int_equals("Map field should be foo", foo_id, eq->binary_expr.left->map_field.map_key);
    assert_int_equals("Should be constant ref", type_constant_reference, eq->binary_expr.right->type);
    assert_int_equals("Constant should be foo", foo_id, eq->binary_expr.right->constant_reference.name);

    free_node(module_list);

    assert_int_equals("Should not have freed any new allocations", after_allocation, allocated);

    free_node(new_root);

    assert_int_equals("Should have freed all new allocations", prior_to_allocation, allocated);

    teardown();
}
