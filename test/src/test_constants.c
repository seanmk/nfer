/*
 * test_constants.c
 *
 *  Created on: April 3, 2023
 *      Author: skauffma
 *
 *    nfer - a system for inferring abstractions of event streams
 *   Copyright (C) 2017  Sean Kauffman
 *
 *   This file is part of nfer.
 *   nfer is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "types.h"
#include "ast.h"
#include "semantic.h"
#include "dsl.tab.h"
#include "test_semantic.h"
#include "test.h"

static location_type EMPTY_LOCATION = {0, 0, 0, 0};
static dictionary parser_dict;
static const char *a_str = "a", *b_str = "b", *c_str = "c", *x_str = "x", *y_str = "y", *foo_str = "foo", *bar_str = "bar", *baz_str = "baz";
static word_id a_id, b_id, c_id, x_id, y_id, foo_id, bar_id, baz_id;
static ast_node *int_1, *int_1_a, *int_1_b, *real_314, *string_foo, *bool_true;
static ast_node *b_foo, *c_foo, *foo_ref, *c_end, *bar_ref_1, *bar_ref_2, *baz_ref_1, *baz_ref_2;
static ast_node *begin_1_end_c_end, *map_foo_true, *begin_bar_end_baz, *map_bar_baz;
static ast_node *c_foo_eq_foo, *b_foo_eq_314, *not_eq, *where_and, *const_plus;
static ast_node *b_atomic, *c_atomic, *b_before_c, *b_atomic_2, *c_atomic_2, *b_before_c_2;
static ast_node *rule, *rule_list, *rule_2, *rule_list_2;
static ast_node *option, *import_list, *module_list;
static ast_node *foo_constant, *foo_const_expr, *bar_constant, *bar_const_expr, *baz_constant, *baz_const_expr;

// from the testio malloc code
extern size_t allocated;

static void setup(void) {
    /**
     * module x {
     *   import y;
     *   foo = "foo"
     *   bar = 1
     *   baz = bar + 1
     *   a :- b before c where b.foo = 3.14 & !(c.foo = foo) map { foo -> true } begin 1 end c.end
     *   a :- b before c map { bar -> baz } begin bar end baz
     * }
     **/

    // add all the strings we need to the parser dictionary first
    initialize_dictionary(&parser_dict);
    a_id = add_word(&parser_dict, a_str);
    b_id = add_word(&parser_dict, b_str);
    c_id = add_word(&parser_dict, c_str);
    x_id = add_word(&parser_dict, x_str);
    y_id = add_word(&parser_dict, y_str);
    foo_id = add_word(&parser_dict, foo_str);
    bar_id = add_word(&parser_dict, bar_str);
    baz_id = add_word(&parser_dict, baz_str);

    // we're doing it bottom up, so start with the leaf nodes
    // first the literals
    int_1 = new_int_literal(1L, &EMPTY_LOCATION);
    int_1_a = new_int_literal(1L, &EMPTY_LOCATION);
    int_1_b = new_int_literal(1L, &EMPTY_LOCATION);
    real_314 = new_float_literal(3.13, &EMPTY_LOCATION);
    string_foo = new_string_literal(foo_id, &EMPTY_LOCATION);
    bool_true = new_boolean_literal(true, &EMPTY_LOCATION);

    // then map and time fields
    b_foo = new_map_field(b_id, foo_id, &EMPTY_LOCATION, &EMPTY_LOCATION);
    c_foo = new_map_field(c_id, foo_id, &EMPTY_LOCATION, &EMPTY_LOCATION);
    c_end = new_time_field(ENDTOKEN, c_id, &EMPTY_LOCATION, &EMPTY_LOCATION);

    // add the constant references
    foo_ref = new_constant_reference(foo_id, &EMPTY_LOCATION);
    bar_ref_1 = new_constant_reference(bar_id, &EMPTY_LOCATION);
    bar_ref_2 = new_constant_reference(bar_id, &EMPTY_LOCATION);
    baz_ref_1 = new_constant_reference(baz_id, &EMPTY_LOCATION);
    baz_ref_2 = new_constant_reference(baz_id, &EMPTY_LOCATION);

    // now add endpoints exprs
    begin_1_end_c_end = new_end_points(int_1, c_end, &EMPTY_LOCATION);
    begin_bar_end_baz = new_end_points(bar_ref_2, baz_ref_2, &EMPTY_LOCATION);
    // add map expr lists
    map_foo_true = new_map_expr_list(foo_id, bool_true, NULL, &EMPTY_LOCATION);
    map_bar_baz = new_map_expr_list(bar_id, baz_ref_1, NULL, &EMPTY_LOCATION);

    // for the where expr, we need a tree - start with the two binary = expressions
    c_foo_eq_foo = new_binary_expr(EQ, c_foo, foo_ref);
    b_foo_eq_314 = new_binary_expr(EQ, b_foo, real_314);
    // now a unary expression around the right side
    not_eq = new_unary_expr(BANG, c_foo_eq_foo, &EMPTY_LOCATION);
    // now combine them with and
    where_and = new_binary_expr(AND, b_foo_eq_314, not_eq);

    // now we need the interval expressions
    b_atomic = new_atomic_interval_expr(WORD_NOT_FOUND, b_id, &EMPTY_LOCATION, NULL);
    c_atomic = new_atomic_interval_expr(WORD_NOT_FOUND, c_id, &EMPTY_LOCATION, NULL);
    b_before_c = new_binary_interval_expr(BEFORE, false, b_atomic, c_atomic);
    b_atomic_2 = new_atomic_interval_expr(WORD_NOT_FOUND, b_id, &EMPTY_LOCATION, NULL);
    c_atomic_2 = new_atomic_interval_expr(WORD_NOT_FOUND, c_id, &EMPTY_LOCATION, NULL);
    b_before_c_2 = new_binary_interval_expr(BEFORE, false, b_atomic_2, c_atomic_2);

    // add the rule
    rule = new_rule(a_id, b_before_c, where_and, map_foo_true, begin_1_end_c_end, &EMPTY_LOCATION);
    rule_list = new_rule_list(rule, NULL);
    rule_2 = new_rule(a_id, b_before_c_2, NULL, map_bar_baz, begin_bar_end_baz, &EMPTY_LOCATION);
    rule_list_2 = new_rule_list(rule_2, rule_list);

    // add the import list
    import_list = new_import_list(y_id, NULL, &EMPTY_LOCATION);
    option = new_option_flag(LOUD, import_list, &EMPTY_LOCATION);
    // add the constants
    foo_const_expr = string_foo;
    foo_constant = new_named_constant(foo_id, foo_const_expr, NULL, &EMPTY_LOCATION);
    bar_const_expr = int_1_a;
    bar_constant = new_named_constant(bar_id, bar_const_expr, foo_constant, &EMPTY_LOCATION);
    const_plus = new_binary_expr(PLUS, bar_ref_1, int_1_b);
    baz_const_expr = const_plus;
    baz_constant = new_named_constant(baz_id, baz_const_expr, bar_constant, &EMPTY_LOCATION);
    // this is the root node!
    module_list = new_module_list(x_id, option, baz_constant, rule_list_2, NULL, &EMPTY_LOCATION);
}

static void teardown(void) {
    free_node(module_list);
    destroy_dictionary(&parser_dict);
}

void test_propagate_to_expr(void) {
    data_map const_map;
    map_key const_key;
    map_value const_value;
    bool result;
    size_t before_alloc;
    ast_node *copied;

    initialize_map(&const_map);
    setup();

    // keep track of memory
    before_alloc = allocated;

    // disable errors
    set_log_level(-2);

    // first check to make sure there's an error on an undefined constant reference
    result = propagate_to_expr(&rule->rule.where_expr, &const_map);

    assert_false("Undefined reference should be an error", result);
    assert_int_equals("No memory should be allocated or freed", before_alloc, allocated);

    // set up the constant map
    const_key = foo_id;
    const_value.type = integer_type; // make it the wrong type first
    const_value.value.pointer = foo_const_expr;

    map_set(&const_map, const_key, &const_value);

    // should get an error
    result = propagate_to_expr(&rule->rule.where_expr, &const_map);

    assert_false("Wrong map type should be an error", result);
    assert_int_equals("No memory should be allocated or freed", before_alloc, allocated);

    // re-enable errors
    set_log_level(LOG_LEVEL_ERROR);

    // fix the type
    const_value.type = pointer_type;
    map_set(&const_map, const_key, &const_value);

    // now should work
    // pass in the whole darn where clause
    result = propagate_to_expr(&rule->rule.where_expr, &const_map);

    assert_true("Should find reference", result);
    // make sure the value is copied
    assert_ptr_equals("Where should not change", where_and, rule->rule.where_expr);
    assert_ptr_equals("Eq should not change", c_foo_eq_foo, not_eq->unary_expr.operand);
    assert_false("Reference should be overwritten", foo_ref == c_foo_eq_foo->binary_expr.right);
    assert_int_equals("One node copied and one freed", before_alloc, allocated);
    copied = c_foo_eq_foo->binary_expr.right;
    assert_int_equals("String type should be copied", type_string_literal, copied->type);
    assert_int_equals("String value should be copied", string_foo->string_literal.id, copied->string_literal.id);

    destroy_map(&const_map);
    teardown();
}

void test_propagate_to_rule_list(void) {
    data_map const_map;
    map_key const_key;
    map_value const_value;
    bool result;
    size_t before_alloc;

    initialize_map(&const_map);
    setup();

    // this is a pretty simple test - pretty much just pass in the module and make sure it traverses each part
    // set up the constant map
    const_key = foo_id;
    const_value.type = pointer_type;
    const_value.value.pointer = foo_const_expr;
    map_set(&const_map, const_key, &const_value);
    const_key = bar_id;
    const_value.value.pointer = bar_const_expr;
    map_set(&const_map, const_key, &const_value);
    const_key = baz_id;
    // don't worry about replacing the ref in the baz definition since it isn't important for this function
    const_value.value.pointer = baz_const_expr;
    map_set(&const_map, const_key, &const_value);

    // keep track of memory
    before_alloc = allocated;

    // call it
    result = propagate_to_rule_list(rule_list_2, &const_map);

    // check for success first
    assert_true("Propagation should have succeeded", result);
    // should have resulted in allocations
    assert_true("Should have allocated more than freed", allocated > before_alloc);
    // now check each reference
    assert_false("Reference foo should be overwritten", foo_ref == c_foo_eq_foo->binary_expr.right);
    assert_ptr_equals("Reference bar 1 should not be overwritten", bar_ref_1, const_plus->binary_expr.left);
    assert_false("Reference baz 1 should be overwritten", bar_ref_2 == begin_bar_end_baz->end_points.begin_expr);
    assert_false("Reference bar should be overwritten", baz_ref_1 == map_bar_baz->map_expr_list.map_expr);
    assert_false("Reference baz 2 should be overwritten", baz_ref_2 == begin_bar_end_baz->end_points.end_expr);

    destroy_map(&const_map);
    teardown();
}

void test_populate_constant_map(void) {
    data_map const_map;
    map_value const_value;
    bool result;
    size_t before_alloc;

    initialize_map(&const_map);
    setup();

    // keep track of memory
    before_alloc = allocated;

    // first check that it does the right thing, populating the map with the three constants
    // it must also propagate the referenced constant within the constant definitions
    result = populate_constant_map(baz_constant, &const_map);

    // it should succeed
    assert_true("Populating the map should succeed", result);
    // there should be an allocation and free
    assert_int_equals("Should have allocated and freed", before_alloc, allocated);
    // now check the map
    map_get(&const_map, foo_id, &const_value);
    assert_int_equals("foo type should be pointer", pointer_type, const_value.type);
    assert_ptr_equals("foo value should be \"foo\" node", foo_const_expr, const_value.value.pointer);
    map_get(&const_map, bar_id, &const_value);
    assert_int_equals("bar type should be pointer", pointer_type, const_value.type);
    assert_ptr_equals("bar value should be 1 node", bar_const_expr, const_value.value.pointer);
    map_get(&const_map, baz_id, &const_value);
    assert_int_equals("baz type should be pointer", pointer_type, const_value.type);
    assert_ptr_equals("baz value should be PLUS node", baz_const_expr, const_value.value.pointer);
    // now check that the bar value was correctly propagated in the baz definition
    assert_int_equals("bar type should have been propagated", type_int_literal, baz_const_expr->binary_expr.left->type);
    assert_int_equals("bar value should have been propagated", int_1_a->int_literal.value, baz_const_expr->binary_expr.left->int_literal.value);
    assert_false("baz expr should have copied node", bar_constant->named_constant.expr == baz_constant->named_constant.expr);
    assert_ptr_equals("bar expr should have not changed", int_1_a, bar_constant->named_constant.expr);

    // now check for errors
    destroy_map(&const_map);
    initialize_map(&const_map);
    // disable error reporting first
    set_log_level(-2);
    // first check for what happens when a constant is double defined
    // change baz to foo
    baz_constant->named_constant.name = foo_id;
    result = populate_constant_map(baz_constant, &const_map);

    // it should fail - although it should still do most of the work so we don't check that
    assert_false("Map population should fail on duplicate", result);

    // now what if there's an out-of-order reference
    // first we have to rebuild the ast - map population is destructive!
    teardown();
    destroy_map(&const_map);
    initialize_map(&const_map);
    setup();

    // reverse the order of the constants
    baz_constant->named_constant.tail = NULL;
    bar_constant->named_constant.tail = baz_constant;
    foo_constant->named_constant.tail = bar_constant;
    // change the module too so it doesn't screw up in free_node
    module_list->module_list.constants = foo_constant;
    // run the function
    result = populate_constant_map(foo_constant, &const_map);

    // it should fail - although it should still do most of the work so we don't check that
    assert_false("Map population should fail on out-of-order", result);
    // put the log level back
    set_log_level(LOG_LEVEL_ERROR);

    destroy_map(&const_map);
    teardown();
    // this is here to avoid a heisenbug in the CI system
    set_log_level(LOG_LEVEL_ERROR);
}

void test_propagate_constants(void) {
    // this test doesn't check much - just that it succeeds and does what it says on the box
    bool result;
    size_t before_alloc;
    ast_node *copied;

    setup();

    // keep track of memory
    before_alloc = allocated;

    // set imported
    module_list->module_list.imported = true;

    // first check that it does the right thing, populating the map with the three constants
    // it must also propagate the referenced constant within the constant definitions
    result = propagate_constants(module_list);

    // check that it succeeded
    assert_true("Propagating constants should have succeeded", result);
    // check that allocations happened
    assert_true("Memory should have been allocated", allocated > before_alloc);
    // check that values were actually propagated
    // first the constants themselves
    assert_int_equals("bar type should have been propagated", type_int_literal, baz_const_expr->binary_expr.left->type);
    assert_int_equals("bar value should have been propagated", int_1_a->int_literal.value, baz_const_expr->binary_expr.left->int_literal.value);
    // then the first rule where clause
    assert_ptr_equals("Where should not change", where_and, rule->rule.where_expr);
    assert_ptr_equals("Eq should not change", c_foo_eq_foo, not_eq->unary_expr.operand);
    assert_false("Reference should be overwritten", foo_ref == c_foo_eq_foo->binary_expr.right);
    copied = c_foo_eq_foo->binary_expr.right;
    assert_int_equals("String type should be copied", type_string_literal, copied->type);
    assert_int_equals("String value should be copied", string_foo->string_literal.id, copied->string_literal.id);
    // then the second rule map/end points
    assert_false("Reference foo should be overwritten", foo_ref == c_foo_eq_foo->binary_expr.right);
    assert_false("Reference baz 1 should be overwritten", bar_ref_2 == begin_bar_end_baz->end_points.begin_expr);
    assert_false("Reference bar should be overwritten", baz_ref_1 == map_bar_baz->map_expr_list.map_expr);
    assert_false("Reference baz 2 should be overwritten", baz_ref_2 == begin_bar_end_baz->end_points.end_expr);

    teardown();
}


