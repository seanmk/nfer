/*
 * test_eval.c
 *
 *  Created on: May 1, 2017
 *      Author: skauffma
 *
 *    nfer - a system for inferring abstractions of event streams
 *   Copyright (C) 2017  Sean Kauffman
 *
 *   This file is part of nfer.
 *   nfer is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdlib.h>

#include "types.h"
#include "test.h"
#include "stack.h"
#include "map.h"
#include "pool.h"
#include "expression.h"
#include "log.h"
#include "test_expression.h"


#define MAX_ACTION_SIZE 15

static expression_input *input;
static data_stack stack;
static data_map map1, map2;

static void setup(unsigned int actions) {
    initialize_expression_input(&input, actions);
    initialize_stack(&stack);
    initialize_map(&map1);
    initialize_map(&map2);
}
static void teardown(void) {
    destroy_expression_input(&input);
    destroy_stack(&stack);
    destroy_map(&map1);
    destroy_map(&map2);
}


void test_expression_from_input(void) {
    int position = 1;
    typed_value result;
    setup(MAX_ACTION_SIZE);

    input[position++].action = param_intlit;
    input[position++].integer_value = 4;
    input[position++].action = param_reallit;
    input[position++].real_value = 4.5;
    input[position++].action = action_add;
    input[0].length = position;

    evaluate_expression(input, &result, &stack,
            0, 0, NULL, 0, 0, NULL);

    assert_int_equals("stack should be empty", 0, stack.tos);
    assert_int_equals("result type wrong", real_type, result.type);
    assert_float_equals("result value wrong", 8.5, result.value.real);

    input[position++].action = param_intlit;
    input[position++].integer_value = 10;
    input[position++].action = action_lt;
    input[0].length = position;

    evaluate_expression(input, &result, &stack,
            0, 0, NULL, 0, 0, NULL);

    assert_int_equals("stack should be empty", 0, stack.tos);
    assert_int_equals("result type wrong", boolean_type, result.type);
    assert_true("result value wrong", result.value.boolean);

    teardown();
}
void test_expression_from_runtime(void) {
    int position = 1;
    typed_value result;
    map_value value;
    setup(MAX_ACTION_SIZE);

    value.type = integer_type;
    value.value.integer = 10;
    map_set(&map1, 0, &value);
    input[position++].action = param_left_field;
    input[position++].field_name = 0;
    value.type = integer_type;
    value.value.integer = 20;
    map_set(&map2, 1, &value);
    input[position++].action = param_right_field;
    input[position++].field_name = 1;
    input[position++].action = action_mul;
    input[0].length = position;

    evaluate_expression(input, &result, &stack,
            0, 0, &map1, 0, 0, &map2);

    assert_int_equals("stack should be empty", 0, stack.tos);
    assert_int_equals("result type wrong", integer_type, result.type);
    assert_int_equals("result value wrong", 200, result.value.integer);

    input[position++].action = param_left_end;
    input[position++].action = action_sub;
    input[position++].action = action_neg;
    input[0].length = position;

    evaluate_expression(input, &result, &stack,
            10, 20, &map1, 30, 40, &map2);

    assert_int_equals("stack should be empty", 0, stack.tos);
    assert_int_equals("result type wrong", integer_type, result.type);
    assert_int_equals("result value wrong", -180, result.value.integer);

    teardown();
}
/**
 * Test evaluation when things go right.
 */
void test_evaluate_expression(void) {
    // evaluate valid expressions that use every possible action type
    // a before b where...
    // 1. (a.x + 1) = (b.x - 1)          - add, sub, left_field, right_field, int_lit, eq (int,real)
    // 2. (a.begin * 2) != (b.begin / 2) - mul, div, left_begin, right_begin, neq (real)
    // 3. (a.end % 2) < -(b.end)         - mod, left_end, right_end, lt (int), neg
    // 4. (a.y = "foo") & true           - left_field, eq (str), bool_lit (t), and
    // 5. (b.y != "foo") | !(false)      - right_field, neq (str), or, not, bool_lit (f)
    // 6. (a.x != 1) & (a.x > 1)         - left_field, neq (int), int_lit, and, gt (int)
    // 7. (a.z <= 3.14) | (b.z >= 3.14)  - left_field, lte (real), or, right_field, gte (real), real_lit
    // 8. !(true) = false                - bool_lit, neg, eq (bool)
    // 9. true != false                  - bool_lit, neq (bool)

    // 10. (a.z + b.z) = (a.z + b.x)     - eq (real,real), add (real,real), add (real,int)
    // 11. (a.z - b.z) != (a.z - b.x)    - neq (real,real), sub (real,real), sub (real,int)
    // 12. (a.x - b.z) < (a.z * b.z)     - lt (real,real), sub (int,real), mul (real,real)
    // 13. (a.z * b.x) > (a.x * b.z)     - gt (real,real), mul (real,int), mul (int,real)
    // 14. (a.z / b.z) <= (a.z / b.x)    - lte (real,real), div (real,real), div (real,int)
    // 15. 1 <= (a.x / b.z)              - int_lit, lte (int,real), div (int,real)
    // 16. 1 < 1.25                      - int_lit, real_lit, lt (int,real)
    // 17. 1.25 > 1 & 1 > -(1.25)        - int_lit, real_lit, and, gt (real,int), gt (int,real), neg (real)
    // 18. 1.25 <= 1.25 & 1 <= 1.25      - real_lit, lte (real,real), int_real, lte (int,real)
    // 19. 1 <= a.x & 1 >= b.x           - int_lit, lte (int,int), gte (int,int)
    // 20. 1.25 >= 1 & 1 >= 1.25         - gte (real,int), gte (int,real)
    // 21. a.z = b.x | a.x = b.z         - eq (real,int), eq (int,real)
    // 22. a.z != b.x & a.x != b.z       - neq (real,int), neq (int,real)
    // 23. 1.25 <= 1                     - lte (real,int)

    typed_value result;
    map_key x = 0, y = 1, z = 2;
    word_id foo = 0, bar = 1;
    map_value ax_val, bx_val, ay_val, by_val, az_val, bz_val;
    timestamp a_begin = 100, b_begin = 400, a_end = 333, b_end = 599;
    int position;

    setup(MAX_ACTION_SIZE);

    // set up the values that will be passed in - we'll use the same for every test
    // x values
    ax_val.type = integer_type;
    ax_val.value.integer = 3;
    bx_val.type = integer_type;
    bx_val.value.integer = 5;
    map_set(&map1, x, &ax_val);
    map_set(&map2, x, &bx_val);
    // y values
    ay_val.type = string_type;
    ay_val.value.string = foo;
    by_val.type = string_type;
    by_val.value.string = bar;
    map_set(&map1, y, &ay_val);
    map_set(&map2, y, &by_val);
    // z values
    az_val.type = real_type;
    az_val.value.real = 3.01;
    bz_val.type = real_type;
    bz_val.value.real = 2.14;
    map_set(&map1, z, &az_val);
    map_set(&map2, z, &bz_val);

    // 1. (a.x + 1) = (b.x - 1)          - add, sub, left_field, right_field, int_lit, eq (int,real)
    // (a.x + 1)
    position = 1;
    input[position++].action = param_left_field;
    input[position++].field_name = x;
    input[position++].action = param_intlit;
    input[position++].integer_value = 1;
    input[position++].action = action_add;
    // (b.x - 1)
    input[position++].action = param_right_field;
    input[position++].field_name = x;
    input[position++].action = param_intlit;
    input[position++].integer_value = 1;
    input[position++].action = action_sub;
    // =
    input[position++].action = action_eq;
    input[0].length = position; // 12

    evaluate_expression(input, &result, &stack,
                        a_begin, a_end, &map1, b_begin, b_end, &map2);

    assert_int_equals("1. stack should be empty", 0, stack.tos);
    assert_int_equals("1. result type wrong", boolean_type, result.type);
    assert_true("1. result value wrong", result.value.boolean);

    // 2. (a.begin * 2) != (b.begin / 2) - mul, div, left_begin, right_begin, neq (real)
    // (a.begin * 2)
    position = 1;
    input[position++].action = param_left_begin;
    input[position++].action = param_intlit;
    input[position++].integer_value = 2;
    input[position++].action = action_mul;
    // (b.begin / 2)
    input[position++].action = param_right_begin;
    input[position++].action = param_intlit;
    input[position++].integer_value = 2;
    input[position++].action = action_div;
    // !=
    input[position++].action = action_ne;
    input[0].length = position; // 10

    evaluate_expression(input, &result, &stack,
                        a_begin, a_end, &map1, b_begin, b_end, &map2);

    assert_int_equals("2. stack should be empty", 0, stack.tos);
    assert_int_equals("2. result type wrong", boolean_type, result.type);
    assert_false("2. result value wrong", result.value.boolean);

    // 3. (a.end % 2) < -(b.end)         - mod, left_end, right_end, lt (int), neg
    // (a.end % 2)
    position = 1;
    input[position++].action = param_left_end;
    input[position++].action = param_intlit;
    input[position++].integer_value = 2;
    input[position++].action = action_mod;
    // -(b.end)
    input[position++].action = param_right_end;
    input[position++].action = action_neg;
    // <
    input[position++].action = action_lt;
    input[0].length = position; // 8

    evaluate_expression(input, &result, &stack,
                        a_begin, a_end, &map1, b_begin, b_end, &map2);

    assert_int_equals("3. stack should be empty", 0, stack.tos);
    assert_int_equals("3. result type wrong", boolean_type, result.type);
    assert_false("3. result value wrong", result.value.boolean);

    // 4. (a.y = "foo") & true           - left_field, eq (str), bool_lit (t), and
    // (a.y = "foo")
    position = 1;
    input[position++].action = param_left_field;
    input[position++].field_name = y;
    input[position++].action = param_strlit;
    input[position++].string_value = foo;
    input[position++].action = action_eq;
    // true
    input[position++].action = param_boollit;
    input[position++].boolean_value = true;
    // &
    input[position++].action = action_and;
    input[0].length = position; // 9

    evaluate_expression(input, &result, &stack,
                        a_begin, a_end, &map1, b_begin, b_end, &map2);

    assert_int_equals("4. stack should be empty", 0, stack.tos);
    assert_int_equals("4. result type wrong", boolean_type, result.type);
    assert_true("4. result value wrong", result.value.boolean);

    // 5. (b.y != "foo") | !(false)      - right_field, neq (str), or, not, bool_lit (f)
    // (b.y != "foo")
    position = 1;
    input[position++].action = param_right_field;
    input[position++].field_name = y;
    input[position++].action = param_strlit;
    input[position++].string_value = foo;
    input[position++].action = action_ne;
    // !(false)
    input[position++].action = param_boollit;
    input[position++].boolean_value = false;
    input[position++].action = action_not;
    // |
    input[position++].action = action_or;
    input[0].length = position; // 10

    evaluate_expression(input, &result, &stack,
                        a_begin, a_end, &map1, b_begin, b_end, &map2);

    assert_int_equals("5. stack should be empty", 0, stack.tos);
    assert_int_equals("5. result type wrong", boolean_type, result.type);
    assert_true("5. result value wrong", result.value.boolean);

    // 6. (a.x != 1) & (a.x > 1)         - left_field, neq (int), int_lit, and, gt (int)
    // (a.x != 1)
    position = 1;
    input[position++].action = param_left_field;
    input[position++].field_name = x;
    input[position++].action = param_intlit;
    input[position++].integer_value = 1;
    input[position++].action = action_ne;
    // (a.x > 1)
    input[position++].action = param_left_field;
    input[position++].field_name = x;
    input[position++].action = param_intlit;
    input[position++].integer_value = 1;
    input[position++].action = action_gt;
    // &
    input[position++].action = action_and;
    input[0].length = position; // 12

    evaluate_expression(input, &result, &stack,
                        a_begin, a_end, &map1, b_begin, b_end, &map2);

    assert_int_equals("6. stack should be empty", 0, stack.tos);
    assert_int_equals("6. result type wrong", boolean_type, result.type);
    assert_true("6. result value wrong", result.value.boolean);

    // 7. (a.z <= 3.14) | (b.z >= 3.14)    - left_field, lt (real), or, right_field, gt (real), real_lit
    // (a.z <= 3.14)
    position = 1;
    input[position++].action = param_left_field;
    input[position++].field_name = z;
    input[position++].action = param_reallit;
    input[position++].real_value = 3.14;
    input[position++].action = action_lte;
    // (b.z >= 3.14)
    input[position++].action = param_right_field;
    input[position++].field_name = z;
    input[position++].action = param_reallit;
    input[position++].real_value = 3.14;
    input[position++].action = action_gte;
    // |
    input[position++].action = action_or;
    input[0].length = position; // 12

    evaluate_expression(input, &result, &stack,
                        a_begin, a_end, &map1, b_begin, b_end, &map2);

    assert_int_equals("7. stack should be empty", 0, stack.tos);
    assert_int_equals("7. result type wrong", boolean_type, result.type);
    assert_true("7. result value wrong", result.value.boolean);

    // 8. !(true) = false                - bool_lit, neg, eq (bool)
    // !(true)
    position = 1;
    input[position++].action = param_boollit;
    input[position++].boolean_value = true;
    input[position++].action = action_not;
    // false
    input[position++].action = param_boollit;
    input[position++].boolean_value = false;
    // =
    input[position++].action = action_eq;
    input[0].length = position; // 7

    evaluate_expression(input, &result, &stack,
                        a_begin, a_end, &map1, b_begin, b_end, &map2);

    assert_int_equals("8. stack should be empty", 0, stack.tos);
    assert_int_equals("8. result type wrong", boolean_type, result.type);
    assert_true("8. result value wrong", result.value.boolean);

    // 9. true != false                  - bool_lit, neq (bool)
    // true
    position = 1;
    input[position++].action = param_boollit;
    input[position++].boolean_value = true;
    // false
    input[position++].action = param_boollit;
    input[position++].boolean_value = false;
    // !=
    input[position++].action = action_ne;
    input[0].length = position; // 6

    evaluate_expression(input, &result, &stack,
                        a_begin, a_end, &map1, b_begin, b_end, &map2);

    assert_int_equals("9. stack should be empty", 0, stack.tos);
    assert_int_equals("9. result type wrong", boolean_type, result.type);
    assert_true("9. result value wrong", result.value.boolean);

    // 10. (a.z + b.z) = (a.z + b.x)     - eq (real,real), add (real,real), add (real,int)
    // (a.z + b.z)
    position = 1;
    input[position++].action = param_left_field;
    input[position++].field_name = z;
    input[position++].action = param_right_field;
    input[position++].field_name = z;
    input[position++].action = action_add;
    // (a.z + b.x)
    input[position++].action = param_left_field;
    input[position++].field_name = z;
    input[position++].action = param_right_field;
    input[position++].field_name = x;
    input[position++].action = action_add;
    // =
    input[position++].action = action_eq;
    input[0].length = position; // 12

    evaluate_expression(input, &result, &stack,
                        a_begin, a_end, &map1, b_begin, b_end, &map2);

    assert_int_equals("10. stack should be empty", 0, stack.tos);
    assert_int_equals("10. result type wrong", boolean_type, result.type);
    assert_false("10. result value wrong", result.value.boolean);

    // 11. (a.z - b.z) != (a.z - b.x)    - neq (real,real), sub (real,real), sub (real,int)
    // (a.z - b.z)
    position = 1;
    input[position++].action = param_left_field;
    input[position++].field_name = z;
    input[position++].action = param_right_field;
    input[position++].field_name = z;
    input[position++].action = action_sub;
    // (a.z - b.x)
    input[position++].action = param_left_field;
    input[position++].field_name = z;
    input[position++].action = param_right_field;
    input[position++].field_name = x;
    input[position++].action = action_sub;
    // !=
    input[position++].action = action_ne;
    input[0].length = position; // 12

    evaluate_expression(input, &result, &stack,
                        a_begin, a_end, &map1, b_begin, b_end, &map2);

    assert_int_equals("11. stack should be empty", 0, stack.tos);
    assert_int_equals("11. result type wrong", boolean_type, result.type);
    assert_true("11. result value wrong", result.value.boolean);

    // 12. (a.x - b.z) < (a.z * b.z)     - lt (real,real), sub (int,real), mul (real,real)
    // (a.x - b.z)
    position = 1;
    input[position++].action = param_left_field;
    input[position++].field_name = x;
    input[position++].action = param_right_field;
    input[position++].field_name = z;
    input[position++].action = action_sub;
    // (a.z * b.z)
    input[position++].action = param_left_field;
    input[position++].field_name = z;
    input[position++].action = param_right_field;
    input[position++].field_name = z;
    input[position++].action = action_mul;
    // <
    input[position++].action = action_lt;
    input[0].length = position; // 12

    evaluate_expression(input, &result, &stack,
                        a_begin, a_end, &map1, b_begin, b_end, &map2);

    assert_int_equals("12. stack should be empty", 0, stack.tos);
    assert_int_equals("12. result type wrong", boolean_type, result.type);
    assert_true("12. result value wrong", result.value.boolean);

    // 13. (a.z * b.x) > (a.x * b.z)     - gt (real,real), mul (real,int), mul (int,real)
    // (a.z * b.x)
    position = 1;
    input[position++].action = param_left_field;
    input[position++].field_name = z;
    input[position++].action = param_right_field;
    input[position++].field_name = x;
    input[position++].action = action_mul;
    // (a.x * b.z)
    input[position++].action = param_left_field;
    input[position++].field_name = x;
    input[position++].action = param_right_field;
    input[position++].field_name = z;
    input[position++].action = action_mul;
    // >
    input[position++].action = action_gt;
    input[0].length = position; // 12

    evaluate_expression(input, &result, &stack,
                        a_begin, a_end, &map1, b_begin, b_end, &map2);

    assert_int_equals("13. stack should be empty", 0, stack.tos);
    assert_int_equals("13. result type wrong", boolean_type, result.type);
    assert_true("13. result value wrong", result.value.boolean);

    // 14. (a.z / b.z) <= (a.z / b.x)    - lte (real,real), div (real,real), div (real,int)
    // (a.z / b.z)
    position = 1;
    input[position++].action = param_left_field;
    input[position++].field_name = z;
    input[position++].action = param_right_field;
    input[position++].field_name = z;
    input[position++].action = action_div;
    // (a.z / b.x)
    input[position++].action = param_left_field;
    input[position++].field_name = z;
    input[position++].action = param_right_field;
    input[position++].field_name = x;
    input[position++].action = action_div;
    // <=
    input[position++].action = action_lte;
    input[0].length = position; // 12

    evaluate_expression(input, &result, &stack,
                        a_begin, a_end, &map1, b_begin, b_end, &map2);

    assert_int_equals("14. stack should be empty", 0, stack.tos);
    assert_int_equals("14. result type wrong", boolean_type, result.type);
    assert_false("14. result value wrong", result.value.boolean);

    // 15. 1 <= (a.x / b.z)              - int_lit, lte (int,real), div (int,real)
    // 1
    position = 1;
    input[position++].action = param_intlit;
    input[position++].integer_value = 1;
    // (a.x / b.z)
    input[position++].action = param_left_field;
    input[position++].field_name = x;
    input[position++].action = param_right_field;
    input[position++].field_name = z;
    input[position++].action = action_div;
    // <=
    input[position++].action = action_lte;
    input[0].length = position; // 9

    evaluate_expression(input, &result, &stack,
                        a_begin, a_end, &map1, b_begin, b_end, &map2);

    assert_int_equals("15. stack should be empty", 0, stack.tos);
    assert_int_equals("15. result type wrong", boolean_type, result.type);
    assert_true("15. result value wrong", result.value.boolean);

    // 16. 1 < 1.25                      - int_lit, real_lit, lt (int,real)
    // 1
    position = 1;
    input[position++].action = param_intlit;
    input[position++].integer_value = 1;
    // 1.25
    input[position++].action = param_reallit;
    input[position++].real_value = 1.25;
    // <
    input[position++].action = action_lt;
    input[0].length = position; // 6

    evaluate_expression(input, &result, &stack,
                        a_begin, a_end, &map1, b_begin, b_end, &map2);

    assert_int_equals("16. stack should be empty", 0, stack.tos);
    assert_int_equals("16. result type wrong", boolean_type, result.type);
    assert_true("16. result value wrong", result.value.boolean);

    // 17. 1.25 > 1 & 1 > -(1.25)        - int_lit, real_lit, and, gt (real,int), gt (int,real), neg (real)
    // 1.25 > 1
    position = 1;
    input[position++].action = param_reallit;
    input[position++].real_value = 1.25;
    input[position++].action = param_intlit;
    input[position++].integer_value = 1;
    input[position++].action = action_gt;
    // 1 > -(1.25)
    input[position++].action = param_intlit;
    input[position++].integer_value = 1;
    input[position++].action = param_reallit;
    input[position++].real_value = 1.25;
    input[position++].action = action_neg;
    input[position++].action = action_gt;
    // &
    input[position++].action = action_and;
    input[0].length = position; // 13

    evaluate_expression(input, &result, &stack,
                        a_begin, a_end, &map1, b_begin, b_end, &map2);

    assert_int_equals("17. stack should be empty", 0, stack.tos);
    assert_int_equals("17. result type wrong", boolean_type, result.type);
    assert_true("17. result value wrong", result.value.boolean);

    // 18. 1.25 <= 1.25 & 1 <= 1.25      - real_lit, lte (real,real), int_real, lte (int,real)
    // 1.25 <= 1.25
    position = 1;
    input[position++].action = param_reallit;
    input[position++].real_value = 1.25;
    input[position++].action = param_reallit;
    input[position++].real_value = 1.25;
    input[position++].action = action_lte;
    // 1 <= 1.25
    input[position++].action = param_intlit;
    input[position++].integer_value = 1;
    input[position++].action = param_reallit;
    input[position++].real_value = 1.25;
    input[position++].action = action_lte;
    // &
    input[position++].action = action_and;
    input[0].length = position; // 12

    evaluate_expression(input, &result, &stack,
                        a_begin, a_end, &map1, b_begin, b_end, &map2);

    assert_int_equals("18. stack should be empty", 0, stack.tos);
    assert_int_equals("18. result type wrong", boolean_type, result.type);
    assert_true("18. result value wrong", result.value.boolean);

    // 19. 1 <= a.x & 1 >= b.x           - int_lit, lte (int,int), gte (int,int)
    // 1 <= a.x
    position = 1;
    input[position++].action = param_intlit;
    input[position++].integer_value = 1;
    input[position++].action = param_left_field;
    input[position++].field_name = x;
    input[position++].action = action_lte;
    // 1 >= b.x
    input[position++].action = param_intlit;
    input[position++].integer_value = 1;
    input[position++].action = param_right_field;
    input[position++].field_name = x;
    input[position++].action = action_gte;
    // &
    input[position++].action = action_and;
    input[0].length = position; // 12

    evaluate_expression(input, &result, &stack,
                        a_begin, a_end, &map1, b_begin, b_end, &map2);

    assert_int_equals("19. stack should be empty", 0, stack.tos);
    assert_int_equals("19. result type wrong", boolean_type, result.type);
    assert_false("19. result value wrong", result.value.boolean);

    // 20. 1.25 >= 1 & 1 >= 1.25         - gte (real,int), gte (int,real)
    // 1.25 >= 1
    position = 1;
    input[position++].action = param_reallit;
    input[position++].real_value = 1.25;
    input[position++].action = param_intlit;
    input[position++].integer_value = 1;
    input[position++].action = action_gte;
    // 1 >= 1.25
    input[position++].action = param_intlit;
    input[position++].integer_value = 1;
    input[position++].action = param_reallit;
    input[position++].real_value = 1.25;
    input[position++].action = action_gte;
    // &
    input[position++].action = action_and;
    input[0].length = position; // 12

    evaluate_expression(input, &result, &stack,
                        a_begin, a_end, &map1, b_begin, b_end, &map2);

    assert_int_equals("20. stack should be empty", 0, stack.tos);
    assert_int_equals("20. result type wrong", boolean_type, result.type);
    assert_false("20. result value wrong", result.value.boolean);

    // 21. a.z = b.x | a.x = b.z         - eq (real,int), eq (int,real)
    // (a.z = b.x)
    position = 1;
    input[position++].action = param_left_field;
    input[position++].field_name = z;
    input[position++].action = param_right_field;
    input[position++].field_name = x;
    input[position++].action = action_eq;
    // (a.x = b.z)
    input[position++].action = param_left_field;
    input[position++].field_name = x;
    input[position++].action = param_right_field;
    input[position++].field_name = z;
    input[position++].action = action_eq;
    // |
    input[position++].action = action_or;
    input[0].length = position; // 12

    evaluate_expression(input, &result, &stack,
                        a_begin, a_end, &map1, b_begin, b_end, &map2);

    assert_int_equals("21. stack should be empty", 0, stack.tos);
    assert_int_equals("21. result type wrong", boolean_type, result.type);
    assert_false("21. result value wrong", result.value.boolean);

    // 22. a.z != b.x & a.x != b.z       - neq (real,int), neq (int,real)
    // (a.z != b.x)
    position = 1;
    input[position++].action = param_left_field;
    input[position++].field_name = z;
    input[position++].action = param_right_field;
    input[position++].field_name = x;
    input[position++].action = action_ne;
    // (a.x != b.z)
    input[position++].action = param_left_field;
    input[position++].field_name = x;
    input[position++].action = param_right_field;
    input[position++].field_name = z;
    input[position++].action = action_ne;
    // &
    input[position++].action = action_and;
    input[0].length = position; // 12

    evaluate_expression(input, &result, &stack,
                        a_begin, a_end, &map1, b_begin, b_end, &map2);

    assert_int_equals("22. stack should be empty", 0, stack.tos);
    assert_int_equals("22. result type wrong", boolean_type, result.type);
    assert_true("22. result value wrong", result.value.boolean);

    // 23. 1.25 <= 1                     - lte (real,int)
    // 1.25
    position = 1;
    input[position++].action = param_reallit;
    input[position++].real_value = 1.25;
    // 1
    input[position++].action = param_intlit;
    input[position++].integer_value = 1;
    // <=
    input[position++].action = action_lte;
    input[0].length = position; // 6

    evaluate_expression(input, &result, &stack,
                        a_begin, a_end, &map1, b_begin, b_end, &map2);

    assert_int_equals("23. stack should be empty", 0, stack.tos);
    assert_int_equals("23. result type wrong", boolean_type, result.type);
    assert_false("23. result value wrong", result.value.boolean);

    teardown();
}

void test_max_expression_depth(void) {
    int position;
    setup(MAX_ACTION_SIZE);

    // create some valid actions and pass them through
    // 1.25 <= 1 (stack depth 2)
    position = 1;
    input[position++].action = param_reallit;
    input[position++].real_value = 1.25;
    input[position++].action = param_intlit;
    input[position++].integer_value = 1;
    input[position++].action = action_lte;
    input[0].length = position; // 6

    assert_int_equals("Max stack depth wrong", 2, max_expression_stack_depth(input));

    // (1 + (2 + 3)) + (4 + 5) (stack depth 3)
    position = 1;
    input[position++].action = param_intlit;
    input[position++].integer_value = 1;
    input[position++].action = param_intlit;
    input[position++].integer_value = 2;
    input[position++].action = param_intlit;
    input[position++].integer_value = 3;
    input[position++].action = action_add;
    input[position++].action = action_add;
    input[position++].action = param_intlit;
    input[position++].integer_value = 4;
    input[position++].action = param_intlit;
    input[position++].integer_value = 5;
    input[position++].action = action_add;
    input[position++].action = action_add;
    input[0].length = position; // 15

    assert_int_equals("Max stack depth wrong", 3, max_expression_stack_depth(input));

    // (1 + (2 + (3 + (4 + 5)))) (stack depth 5)
    position = 1;
    input[position++].action = param_intlit;
    input[position++].integer_value = 1;
    input[position++].action = param_intlit;
    input[position++].integer_value = 2;
    input[position++].action = param_intlit;
    input[position++].integer_value = 3;
    input[position++].action = param_intlit;
    input[position++].integer_value = 4;
    input[position++].action = param_intlit;
    input[position++].integer_value = 5;
    input[position++].action = action_add;
    input[position++].action = action_add;
    input[position++].action = action_add;
    input[position++].action = action_add;
    input[0].length = position; // 15

    assert_int_equals("Max stack depth wrong", 5, max_expression_stack_depth(input));

    // 1 + -(a.begin) (stack depth 2)
    position = 1;
    input[position++].action = param_intlit;
    input[position++].integer_value = 1;
    input[position++].action = param_left_begin;
    input[position++].action = action_neg;
    input[position++].action = action_add;
    input[0].length = position; // 6

    assert_int_equals("Max stack depth wrong", 2, max_expression_stack_depth(input));

    teardown();
}

extern char test_log_buffer[];
extern int test_log_buffer_pos;
void test_write_expression(void) {
    int position;
    const char *a = "a", *b = "b";
    char *foo_str = "foo", *x_str = "x";
    word_id foo, x;
    dictionary key_dict, val_dict;
    char *expected1 = "(1 + (- a.begin))";
    char *expected2 = "(a.x = \"foo\")";
    char *expected3 = "(b.x >= 3)";
    char *expected4 = "(b.end + 1.250000)";
    char *expected5 = "(true = (! false))";


    setup(MAX_ACTION_SIZE);

    initialize_dictionary(&key_dict);
    initialize_dictionary(&val_dict);

    foo = add_word(&val_dict, foo_str);
    x   = add_word(&key_dict, x_str);

    // check that we write the correct outputs - the write_expression function is complicated!
    // 1. (1 + (- a.begin))
    position = 1;
    input[position++].action = param_intlit;
    input[position++].integer_value = 1;
    input[position++].action = param_left_begin;
    input[position++].action = action_neg;
    input[position++].action = action_add;
    input[0].length = position; // 6

    test_log_buffer_pos = 0;
    write_expression(input, &key_dict, &val_dict, a, b, WRITE_TESTING);
    assert_str_equals("1. Problem writing expression", expected1, test_log_buffer);

    // 2. a.x = "foo"
    position = 1;
    input[position++].action = param_left_field;
    input[position++].field_name = x;
    input[position++].action = param_strlit;
    input[position++].string_value = foo;
    input[position++].action = action_eq;
    input[0].length = position; // 6

    test_log_buffer_pos = 0;
    write_expression(input, &key_dict, &val_dict, a, b, WRITE_TESTING);
    assert_str_equals("2. Problem writing expression", expected2, test_log_buffer);

    // 3. (b.x >= 3)
    position = 1;
    input[position++].action = param_right_field;
    input[position++].field_name = x;
    input[position++].action = param_intlit;
    input[position++].integer_value = 3;
    input[position++].action = action_gte;
    input[0].length = position; // 6

    test_log_buffer_pos = 0;
    write_expression(input, &key_dict, &val_dict, a, b, WRITE_TESTING);
    assert_str_equals("3. Problem writing expression", expected3, test_log_buffer);

    // 4. (b.end + 1.25)
    position = 1;
    input[position++].action = param_right_end;
    input[position++].action = param_reallit;
    input[position++].real_value = 1.25;
    input[position++].action = action_add;
    input[0].length = position; // 5

    test_log_buffer_pos = 0;
    write_expression(input, &key_dict, &val_dict, a, b, WRITE_TESTING);
    assert_str_equals("4. Problem writing expression", expected4, test_log_buffer);

    // 5. (true = (! false))
    position = 1;
    input[position++].action = param_boollit;
    input[position++].boolean_value = true;
    input[position++].action = param_boollit;
    input[position++].boolean_value = false;
    input[position++].action = action_not;
    input[position++].action = action_eq;
    input[0].length = position; // 7

    test_log_buffer_pos = 0;
    write_expression(input, &key_dict, &val_dict, a, b, WRITE_TESTING);
    assert_str_equals("5. Problem writing expression", expected5, test_log_buffer);

    teardown();
    destroy_dictionary(&key_dict);
    destroy_dictionary(&val_dict);
}
