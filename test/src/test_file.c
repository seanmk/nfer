/*
 * test_file.c
 *
 *  Created on: June 30, 2021
 *      Author: skauffma
 *
 *    nfer - a system for inferring abstractions of event streams
 *   Copyright (C) 2017  Sean Kauffman
 *
 *   This file is part of nfer.
 *   nfer is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "file.h"
#include "test.h"

#include "pool.h"
#include "dict.h"
#include "strings.h"

#include "log.h"

/*  we want to break up the tests into different functionality rather than different functions.
    Typically, different functionality should _be in_ different functions, but this is a big
    state machine instead, so there's a lot going on here. */

static pool p;
static dictionary name_dict, key_dict, val_dict;

static void setup(void) {
    initialize_pool(&p);
    initialize_dictionary(&name_dict);
    initialize_dictionary(&key_dict);
    initialize_dictionary(&val_dict);
}

static void teardown(void) {
    destroy_pool(&p);
    destroy_dictionary(&name_dict);
    destroy_dictionary(&key_dict);
    destroy_dictionary(&val_dict);
}

static bool pool_contains(const char *name, const timestamp time) {
    pool_iterator pit;
    interval *i;
    label name_id;

    name_id = find_word(&name_dict, name);

    if (name_id == WORD_NOT_FOUND) {
        return false;
    }

    get_pool_iterator(&p, &pit);
    while (has_next_interval(&pit)) {
        i = next_interval(&pit);
        if (i->name == name_id && i->start == time && i->end == time) {
            return true;
        }
    }
    return false;
}
static bool pool_contains_map(const char *name, const timestamp time, const char *key_str, map_value *value) {
    pool_iterator pit;
    interval *i;
    label name_id;
    map_key key;
    map_value val_check;

    name_id = find_word(&name_dict, name);
    key = find_word(&key_dict, key_str);

    if (name_id == WORD_NOT_FOUND || key == WORD_NOT_FOUND) {
        return false;
    }

    get_pool_iterator(&p, &pit);
    while (has_next_interval(&pit)) {
        i = next_interval(&pit);
        if (i->name == name_id && i->start == time && i->end == time) {
            // now look up the map key
            map_get(&i->map, key, &val_check);
            if (equals(value, &val_check)) {
                return true;
            } 
            // else {
            //     log_msg("type %d ", val_check.type);
            // }
        }
    }
    return false;
}

// try parsing simple events
void test_parse_events(void) {
    const char *good_events[] = {"test|123", "x|1", "true123|009", "123|009", "xyz,33\n"};
    const char *good_names[] = {"test", "x", "true123", "123", "xyz"};
    const timestamp good_times[] = {123, 1, 9, 9, 33};
    int i, n = ((int)sizeof(good_events)/sizeof(good_events[0]));
    const char *bad_events[] = {"test123", "|1", "y||||"};
    const char *bad_names[] = {"test123", "", "y"};
    const timestamp bad_times[] = {0, 1, 0};
    int j, m = ((int)sizeof(bad_events)/sizeof(bad_events[0]));
    event_parse_result result;
    char buffer[MAX_LINE_LENGTH + 1];

    setup();

    for (i = 0; i < n; i++) {
        // copy to the buffer
        copy_string(buffer, good_events[i], MAX_LINE_LENGTH);
        // just parse it, don't add the name, but don't filter
        result = read_event_from_csv(&p, buffer, i, &name_dict, &key_dict, &val_dict, DO_NOT_FILTER);
        assert_int_equals("Event parsing should have succeeded", PARSE_SUCCESS, result);
        assert_true("Pool should contain interval", pool_contains(good_names[i], good_times[i]));
    }
    clear_pool(&p);

    for (j = 0; j < m; j++) {
        // copy to the buffer
        copy_string(buffer, bad_events[j], MAX_LINE_LENGTH);
        // just parse it, don't add the name, but don't filter
        result = read_event_from_csv(&p, buffer, j, &name_dict, &key_dict, &val_dict, DO_NOT_FILTER);
        assert_false("Event parsing should have failed", result == PARSE_SUCCESS);
        assert_false("Pool should not contain interval", pool_contains(bad_names[j], bad_times[j]));
    }
    // output_pool(&p, &name_dict, &key_dict, &val_dict, WRITE_LOGGING);
    
    teardown();
}
void test_hard_timestamps(void) {
    const char *good_events[] = {"test|18446744073709551615"};
    const char *good_names[] = {"test"};
    const timestamp good_times[] = {18446744073709551615UL};
    int i, n = ((int)sizeof(good_events)/sizeof(good_events[0]));
    const char *bad_events[] = {"test|18446744073709551616", "test2|-1", "test3|3.14", "test4|4th"};
    const char *bad_names[] = {"test", "test2", "test3", "test4"};
    const timestamp bad_times[] = {0, 0, 3, 4};
    int j, m = ((int)sizeof(bad_events)/sizeof(bad_events[0]));
    event_parse_result result;
    char buffer[MAX_LINE_LENGTH + 1];

    setup();

    for (i = 0; i < n; i++) {
        // copy to the buffer
        copy_string(buffer, good_events[i], MAX_LINE_LENGTH);
        // just parse it, don't add the name, but don't filter
        result = read_event_from_csv(&p, buffer, i, &name_dict, &key_dict, &val_dict, DO_NOT_FILTER);
        assert_int_equals("Event parsing should have succeeded", PARSE_SUCCESS, result);
        assert_true("Pool should contain interval", pool_contains(good_names[i], good_times[i]));
    }
    clear_pool(&p);

    // in this case, we actually accept the bad events but convert the timestamps
    for (j = 0; j < m; j++) {
        // copy to the buffer
        copy_string(buffer, bad_events[j], MAX_LINE_LENGTH);
        // just parse it, don't add the name, but don't filter
        result = read_event_from_csv(&p, buffer, j, &name_dict, &key_dict, &val_dict, DO_NOT_FILTER);
        assert_int_equals("Event parsing should have succeeded", PARSE_SUCCESS, result);
        assert_true("Pool should contain interval", pool_contains(bad_names[j], bad_times[j]));
    }
    // output_pool(&p, &name_dict, &key_dict, &val_dict, WRITE_LOGGING);
    
    teardown();
}
void test_special_event_names(void) {
    // some IDEs might not display all the UTF-8 chars correctly
    // the second event contains Danish diacritics
    // the third event contains Chinese characters
    const char *good_events[] = {"_!~#$%;^&-?'\"\\*()[].<>test|123", "ÆæØøÅå|456", "的不我|789"};
    const char *good_names[] = {"_!~#$%;^&-?'\"\\*()[].<>test", "ÆæØøÅå", "的不我"};
    const timestamp good_times[] = {123, 456, 789};
    int i, n = ((int)sizeof(good_events)/sizeof(good_events[0]));

    event_parse_result result;
    char buffer[MAX_LINE_LENGTH + 1];

    setup();

    for (i = 0; i < n; i++) {
        // copy to the buffer
        copy_string(buffer, good_events[i], MAX_LINE_LENGTH);
        // just parse it, don't add the name, but don't filter
        result = read_event_from_csv(&p, buffer, i, &name_dict, &key_dict, &val_dict, DO_NOT_FILTER);
        assert_int_equals("Event parsing should have succeeded", PARSE_SUCCESS, result);
        assert_true("Pool should contain interval", pool_contains(good_names[i], good_times[i]));
    }
    // output_pool(&p, &name_dict, &key_dict, &val_dict, WRITE_LOGGING);
    
    teardown();
}
void test_event_delimiters(void) {
    // it is possible to mix and match delimiters
    const char *good_events[] = {"test,123"};
    const char *good_names[] = {"test"};
    const timestamp good_times[] = {123};
    int i, n = ((int)sizeof(good_events)/sizeof(good_events[0]));
    const char *map_events[] = {"x,1|n|3", "y,2,m|4", "z,3,o,5"};
    const char *map_names[] = {"x", "y", "z"};
    const timestamp map_times[] = {1, 2, 3};
    const char *map_keys[] = {"n", "m", "o"};
    map_value map_values[3]; // we have to fill these in still
    int j, m = ((int)sizeof(map_events)/sizeof(map_events[0]));
    event_parse_result result;
    char buffer[MAX_LINE_LENGTH + 1];

    // set the expected map values
    map_values[0].type = integer_type;
    map_values[0].value.integer = 3;
    map_values[1].type = integer_type;
    map_values[1].value.integer = 4;
    map_values[2].type = integer_type;
    map_values[2].value.integer = 5;

    setup();

    for (i = 0; i < n; i++) {
        // copy to the buffer
        copy_string(buffer, good_events[i], MAX_LINE_LENGTH);
        // just parse it, don't add the name, but don't filter
        result = read_event_from_csv(&p, buffer, i, &name_dict, &key_dict, &val_dict, DO_NOT_FILTER);
        assert_int_equals("Event parsing should have succeeded", PARSE_SUCCESS, result);
        assert_true("Pool should contain interval", pool_contains(good_names[i], good_times[i]));
    }
    clear_pool(&p);

    for (j = 0; j < m; j++) {
        // copy to the buffer
        copy_string(buffer, map_events[j], MAX_LINE_LENGTH);
        // just parse it, don't add the name, but don't filter
        result = read_event_from_csv(&p, buffer, j, &name_dict, &key_dict, &val_dict, DO_NOT_FILTER);
        assert_int_equals("Event parsing should have succeeded", PARSE_SUCCESS, result);
        assert_true("Pool should contain interval", pool_contains_map(map_names[j], map_times[j], map_keys[j], &map_values[j]));
    }
    // output_pool(&p, &name_dict, &key_dict, &val_dict, WRITE_LOGGING);
    
    teardown();
}
void test_parse_data(void) {
    const char *map_events = "test|1|x;y;z|3;4.5;pass";
    const char *map_names = "test";
    const timestamp map_times = 1;
    const char *map_keys[] = {"x", "y", "z"};
    map_value map_values[3]; // we have to fill these in still
    int j, m = ((int)sizeof(map_keys)/sizeof(map_keys[0]));

    // make sure we don't parse evens where the number of map keys and values are unequal
    const char *bad_events[] = {"test2|2|x;y;z|3;4.5", "test3|3|x;y|3;4.5;pass"};
    const char *bad_names[] = {"test2", "test3"};
    const timestamp bad_times[] = {2, 3};
    int i, n = ((int)sizeof(bad_events)/sizeof(bad_events[0]));

    event_parse_result result;
    char buffer[MAX_LINE_LENGTH + 1];

    setup();

    // set the expected map values
    map_values[0].type = integer_type;
    map_values[0].value.integer = 3;
    map_values[1].type = real_type;
    map_values[1].value.real = 4.5;
    map_values[2].type = string_type;
    map_values[2].value.string = add_word(&val_dict, "pass");

   
    // copy to the buffer
    copy_string(buffer, map_events, MAX_LINE_LENGTH);
    // just parse it, don't add the name, but don't filter
    result = read_event_from_csv(&p, buffer, 0, &name_dict, &key_dict, &val_dict, DO_NOT_FILTER);
    assert_int_equals("Event parsing should have succeeded", PARSE_SUCCESS, result);
    for (j = 0; j < m; j++) {
        assert_true("Pool should contain interval", pool_contains_map(map_names, map_times, map_keys[j], &map_values[j]));
    }

    for (i = 0; i < n; i++) {
        // copy to the buffer
        copy_string(buffer, bad_events[i], MAX_LINE_LENGTH);
        // just parse it, don't add the name, but don't filter
        result = read_event_from_csv(&p, buffer, i, &name_dict, &key_dict, &val_dict, DO_NOT_FILTER);
        assert_int_equals("Event parsing should have failed", PARSE_UNEXPECTED_NULL, result);
        assert_false("Pool should not contain interval", pool_contains(bad_names[i], bad_times[i]));
    }
    // output_pool(&p, &name_dict, &key_dict, &val_dict, WRITE_LOGGING);

    teardown();
}
void test_data_types(void) {
    const char *map_events[] = {"testi|1|i|3", "testn|2|n|-3", "testr|3|r|4.567", "testt|4|t|true", "testf|5|f|false", "tests|6|s|string"};
    const char *map_names[] = {"testi", "testn", "testr", "testt", "testf", "tests"};
    const timestamp map_times[] = {1, 2, 3, 4, 5, 6};
    const char *map_keys[] = {"i", "n", "r", "t", "f", "s"};
    map_value map_values[6]; // we have to fill these in still
    int j, m = ((int)sizeof(map_events)/sizeof(map_events[0]));
    event_parse_result result;
    char buffer[MAX_LINE_LENGTH + 1];

    setup();

    // set the expected map values
    map_values[0].type = integer_type;
    map_values[0].value.integer = 3;
    map_values[1].type = integer_type;
    map_values[1].value.integer = -3;
    map_values[2].type = real_type;
    map_values[2].value.real = 4.567;
    map_values[3].type = boolean_type;
    map_values[3].value.boolean = true;
    map_values[4].type = boolean_type;
    map_values[4].value.boolean = false;
    map_values[5].type = string_type;
    map_values[5].value.string = add_word(&val_dict, "string");

    for (j = 0; j < m; j++) {
        // copy to the buffer
        copy_string(buffer, map_events[j], MAX_LINE_LENGTH);
        // just parse it, don't add the name, but don't filter
        result = read_event_from_csv(&p, buffer, j, &name_dict, &key_dict, &val_dict, DO_NOT_FILTER);
        assert_int_equals("Event parsing should have succeeded", PARSE_SUCCESS, result);
        assert_true("Pool should contain interval", pool_contains_map(map_names[j], map_times[j], map_keys[j], &map_values[j]));
    }
    // output_pool(&p, &name_dict, &key_dict, &val_dict, WRITE_LOGGING);
    
    teardown();
}
void test_filter_events(void) {
    const char *good_events[] = {"a|1", "b|2", "c|3"};
    const char *good_names[] = {"a", "b", "c"};
    const timestamp good_times[] = {1, 2, 3};
    int i, n = ((int)sizeof(good_events)/sizeof(good_events[0]));
    const char *bad_events[] = {"d|4", "e|5"};
    const char *bad_names[] = {"d", "e"};
    const timestamp bad_times[] = {4, 5};
    int j, m = ((int)sizeof(bad_events)/sizeof(bad_events[0]));
    event_parse_result result;
    char buffer[MAX_LINE_LENGTH + 1];

    setup();

    // we are going to filter so we need to add the event names we want to the name_dict first
    for (i = 0; i < n; i++) {
        add_word(&name_dict, good_names[i]);
    }

    for (i = 0; i < n; i++) {
        // copy to the buffer
        copy_string(buffer, good_events[i], MAX_LINE_LENGTH);
        // just parse it, don't add the name, but don't filter
        result = read_event_from_csv(&p, buffer, i, &name_dict, &key_dict, &val_dict, FILTER_NAMES_AND_KEYS);
        assert_int_equals("Event parsing should have succeeded", PARSE_SUCCESS, result);
        assert_true("Pool should contain interval", pool_contains(good_names[i], good_times[i]));
    }
    clear_pool(&p);

    for (j = 0; j < m; j++) {
        // copy to the buffer
        copy_string(buffer, bad_events[j], MAX_LINE_LENGTH);
        // just parse it, don't add the name, but don't filter
        result = read_event_from_csv(&p, buffer, j, &name_dict, &key_dict, &val_dict, FILTER_NAMES_AND_KEYS);
        assert_int_equals("Event parsing should have filtered", PARSE_LABEL_FILTERED, result);
        assert_false("Pool should not contain interval", pool_contains(bad_names[j], bad_times[j]));
    }
    // output_pool(&p, &name_dict, &key_dict, &val_dict, WRITE_LOGGING);
    
    teardown();
}
void test_filter_data(void) {
    const char *map_events = "test|1|x;a;y;b;z;c|3;3;4.5;4.5;pass;pass";
    const char *map_names = "test";
    const timestamp map_times = 1;
    const char *map_keys[] = {"x", "y", "z"};
    map_value map_values[3]; // we have to fill these in still
    int j, m = ((int)sizeof(map_keys)/sizeof(map_keys[0]));

    // check that these keys are missing - just use the map_values array over again
    const char *bad_keys[] = {"a", "b", "c"};
    int i, n = ((int)sizeof(bad_keys)/sizeof(bad_keys[0]));

    event_parse_result result;
    char buffer[MAX_LINE_LENGTH + 1];

    setup();

    // set the expected map values
    map_values[0].type = integer_type;
    map_values[0].value.integer = 3;
    map_values[1].type = real_type;
    map_values[1].value.real = 4.5;
    map_values[2].type = string_type;
    map_values[2].value.string = add_word(&val_dict, "pass");

    // we are going to filter so we need to add the event names and map keys first
    add_word(&name_dict, map_names);
    for (j = 0; j < m; j++) {
        add_word(&key_dict, map_keys[j]);
    }

    // copy to the buffer
    copy_string(buffer, map_events, MAX_LINE_LENGTH);
    // just parse it, don't add the name, but don't filter
    result = read_event_from_csv(&p, buffer, 0, &name_dict, &key_dict, &val_dict, FILTER_NAMES_AND_KEYS);
    assert_int_equals("Event parsing should have succeeded", PARSE_SUCCESS, result);
    for (j = 0; j < m; j++) {
        assert_true("Pool should contain interval", pool_contains_map(map_names, map_times, map_keys[j], &map_values[j]));
    }

    for (i = 0; i < n; i++) {
        assert_false("Pool should not contain interval", pool_contains_map(map_names, map_times, bad_keys[i], &map_values[i]));
    }
    // output_pool(&p, &name_dict, &key_dict, &val_dict, WRITE_LOGGING);

    teardown();
}
void test_whitespace(void) {
    // just test that whitespace doesn't mess anything up
    const char *good_events[] = {"  test,  123", "test2   |234  \n", "	 test3 	|	 345 	"};
    const char *good_names[] = {"test", "test2", "test3"};
    const timestamp good_times[] = {123, 234, 345};
    int i, n = ((int)sizeof(good_events)/sizeof(good_events[0]));
    const char *map_events[] = {"x|1|  n|  3", "y|2|m  |4.5  ", "z|3|	o	|	true	", "z2|4| 	p	 | 	pass	 \n", "z3|5| a key thing | a string value "};
    const char *map_names[] = {"x", "y", "z", "z2", "z3"};
    const timestamp map_times[] = {1, 2, 3, 4, 5};
    const char *map_keys[] = {"n", "m", "o", "p", "a key thing"};
    map_value map_values[5]; // we have to fill these in still
    int j, m = ((int)sizeof(map_events)/sizeof(map_events[0]));
    event_parse_result result;
    char buffer[MAX_LINE_LENGTH + 1];

    setup();

    // set the expected map values
    map_values[0].type = integer_type;
    map_values[0].value.integer = 3;
    map_values[1].type = real_type;
    map_values[1].value.real = 4.5;
    map_values[2].type = boolean_type;
    map_values[2].value.boolean = true;
    map_values[3].type = string_type;
    map_values[3].value.string = add_word(&val_dict, "pass");
    map_values[4].type = string_type;
    map_values[4].value.string = add_word(&val_dict, "a string value");

    for (i = 0; i < n; i++) {
        // copy to the buffer
        copy_string(buffer, good_events[i], MAX_LINE_LENGTH);
        // just parse it, don't add the name, but don't filter
        result = read_event_from_csv(&p, buffer, i, &name_dict, &key_dict, &val_dict, DO_NOT_FILTER);
        assert_int_equals("Event parsing should have succeeded", PARSE_SUCCESS, result);
        assert_true("Pool should contain interval", pool_contains(good_names[i], good_times[i]));
    }
    clear_pool(&p);

    for (j = 0; j < m; j++) {
        // copy to the buffer
        copy_string(buffer, map_events[j], MAX_LINE_LENGTH);
        // just parse it, don't add the name, but don't filter
        result = read_event_from_csv(&p, buffer, j, &name_dict, &key_dict, &val_dict, DO_NOT_FILTER);
        assert_int_equals("Event parsing should have succeeded", PARSE_SUCCESS, result);
        assert_true("Pool should contain interval", pool_contains_map(map_names[j], map_times[j], map_keys[j], &map_values[j]));
    }
    // output_pool(&p, &name_dict, &key_dict, &val_dict, WRITE_LOGGING);
    
    teardown();
}
void test_value_edge_cases(void) {
    // map value edge cases
    // trues
    // falses
    // -
    // 0
    // test special chars is map keys
    // test special chars in map values
    // empty vals (;;)? -- this one is not currently supported, so ignore for the moment
    const char *map_events[] = {"testt|1|t|trues", "testf|2|f|falses", "testn|3|n|-", "testz|4|z|0", "testsk|5|ÆæØøÅå|1", "testsv|6|sv|ÆæØøÅå"};
    const char *map_names[] = {"testt", "testf", "testn", "testz", "testsk", "testsv"};
    const timestamp map_times[] = {1, 2, 3, 4, 5, 6};
    const char *map_keys[] = {"t", "f", "n", "z", "ÆæØøÅå", "sv"};
    map_value map_values[6]; // we have to fill these in still
    int j, m = ((int)sizeof(map_events)/sizeof(map_events[0]));
    event_parse_result result;
    char buffer[MAX_LINE_LENGTH + 1];

    setup();

    // set the expected map values
    map_values[0].type = string_type;
    map_values[0].value.string = add_word(&val_dict, "trues");
    map_values[1].type = string_type;
    map_values[1].value.string = add_word(&val_dict, "falses");
    map_values[2].type = string_type;
    map_values[2].value.string = add_word(&val_dict, "-");
    map_values[3].type = integer_type;
    map_values[3].value.integer = 0;
    map_values[4].type = integer_type;
    map_values[4].value.integer = 1;
    map_values[5].type = string_type;
    map_values[5].value.string = add_word(&val_dict, "ÆæØøÅå");

    for (j = 0; j < m; j++) {
        // copy to the buffer
        copy_string(buffer, map_events[j], MAX_LINE_LENGTH);
        // just parse it, don't add the name, but don't filter
        result = read_event_from_csv(&p, buffer, j, &name_dict, &key_dict, &val_dict, DO_NOT_FILTER);
        assert_int_equals("Event parsing should have succeeded", PARSE_SUCCESS, result);
        assert_true("Pool should contain interval", pool_contains_map(map_names[j], map_times[j], map_keys[j], &map_values[j]));
    }
    // output_pool(&p, &name_dict, &key_dict, &val_dict, WRITE_LOGGING);
    
    teardown();
}
