/*
 * test_learn.c
 *
 *  Created on: Jan 26, 2017
 *      Author: skauffma
 *
 *    nfer - a system for inferring abstractions of event streams
 *   Copyright (C) 2017  Sean Kauffman
 *
 *   This file is part of nfer.
 *   nfer is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "test.h"
#include "test_learn.h"
#include "learn.h"
#include "map.h"
#include "memory.h"

static learning learn;
static dictionary dict;

static char *words[] = {
        "foo", "bar", "cat", "dog"
};
#define N_WORDS 4

static interval intervals[] = {
        {0, 0, 0, {0, NULL, MAP_MISSING_KEY}, false},
        {1, 1, 1, {0, NULL, MAP_MISSING_KEY}, false},
        {2, 2, 2, {0, NULL, MAP_MISSING_KEY}, false},
        {3, 3, 3, {0, NULL, MAP_MISSING_KEY}, false},
        {2, 4, 4, {0, NULL, MAP_MISSING_KEY}, false},
        {0, 5, 5, {0, NULL, MAP_MISSING_KEY}, false},
        {1, 6, 6, {0, NULL, MAP_MISSING_KEY}, false},
        {3, 7, 7, {0, NULL, MAP_MISSING_KEY}, false},
        {2, 8, 8, {0, NULL, MAP_MISSING_KEY}, false}
};
#define N_INTS 9

static void setup(void) {
    int i;
    initialize_dictionary(&dict);
    for (i = 0; i < N_WORDS; i++) {
        add_word(&dict, words[i]);
    }
    initialize_learning(&learn, dict.size);
}

static void teardown(void) {
    destroy_learning(&learn);
    destroy_dictionary(&dict);
}

void test_initialize_learning(void) {
    long unsigned int i;
    setup();

    assert_int_equals("size wrong", 4, learn.size);
    assert_not_null("matrix not initialized", learn.matrix);
    for (i = 0; i < learn.size * learn.size * sizeof(learning_cell); i++) {
        assert_int_equals("matrix not cleared", 0, *((char *) learn.matrix + i));
    }
    assert_not_null("stats not initialized", learn.stats);
    for (i = 0; i < learn.size * sizeof(interval_stat); i++) {
        assert_int_equals("stats not cleared", 0, *((char *) learn.stats + i));
    }

    teardown();
}
void test_destroy_learning(void) {
    setup();
    teardown();

    assert_int_equals("size wrong", 0, learn.size);
    assert_null("matrix not null", learn.matrix);
    assert_null("stats not null", learn.stats);
}
void test_add_learned_rules(void) {
    nfer_specification spec;
    nfer_rule *rule;

    setup();
    initialize_specification(&spec, N_WORDS);

    learn.matrix[0 * learn.size + 1].success = 3;
    learn.matrix[0 * learn.size + 1].average_length = 2;
    // for all these, make sure the inverse also shows up
    // and set the avg length to longer if we want the first one
    learn.matrix[1 * learn.size + 0].success = 3;
    learn.matrix[1 * learn.size + 0].average_length = 4;

    learn.matrix[3 * learn.size + 2].success = 2;
    learn.matrix[3 * learn.size + 2].failure = 1;
    learn.matrix[3 * learn.size + 2].average_length = 5;
    learn.matrix[2 * learn.size + 3].success = 2;
    learn.matrix[2 * learn.size + 3].failure = 1;
    learn.matrix[2 * learn.size + 3].average_length = 6;

    learn.matrix[2 * learn.size + 0].success = 64;
    learn.matrix[2 * learn.size + 0].average_length = 10;
    learn.matrix[0 * learn.size + 2].success = 64;
    learn.matrix[0 * learn.size + 2].average_length = 11;

    // confidence is 1, support is 0
    add_learned_rules(&learn, &dict, &spec, 1.0, 0.0);

    // check that the dictionary has had 2 words added
    assert_int_equals("two words should have been added", N_WORDS + 2, dict.size);
    assert_str_equals("first added word wrong", "learned_0", dict.words[dict.size - 2].string);
    assert_str_equals("second added word wrong", "learned_1", dict.words[dict.size - 1].string);

    // now check that rules were added to the spec
    assert_int_equals("two rules should have been added", 2, spec.size);
    // check the first rule
    rule = &spec.rules[0];
    assert_int_equals("left label wrong", 0, rule->left_label);
    assert_int_equals("right label wrong", 1, rule->right_label);
    assert_int_equals("result label wrong", 4, rule->result_label);
    assert_int_equals("operator wrong", BEFORE_OPERATOR, rule->op_code);
    // check the second rule
    rule = &spec.rules[1];
    assert_int_equals("left label wrong", 2, rule->left_label);
    assert_int_equals("right label wrong", 0, rule->right_label);
    assert_int_equals("result label wrong", 5, rule->result_label);
    assert_int_equals("operator wrong", BEFORE_OPERATOR, rule->op_code);

    destroy_specification(&spec);
    teardown();
}
void test_finish_learning(void) {
    int i, j;
    learning_cell *cell;

    setup();

    // set up test
    learn.matrix[0 * learn.size + 1].matched = 1;

    learn.matrix[3 * learn.size + 2].matched = 1;
    learn.matrix[3 * learn.size + 2].success = 2;
    learn.matrix[3 * learn.size + 2].failure = 1;

    learn.matrix[2 * learn.size + 0].matched = 2;
    learn.matrix[2 * learn.size + 0].success = 64;

    learn.matrix[1 * learn.size + 1].failure = 127;
    learn.matrix[2 * learn.size + 2].success = 42;

    // actually call the function
    finish_learning(&learn);

    for (i = 0; i < N_WORDS; i++) {
        for (j = 0; j < N_WORDS; j++) {
            cell = &learn.matrix[i * learn.size + j];
            if (i == 0 && j == 1) {
                assert_int_equals("0,1 should have succeeded", 1, cell->success);
                assert_int_equals("0,1 should not have failed", 0, cell->failure);
            } else if (i == 3 && j == 2) {
                assert_int_equals("3,2 should have succeeded", 3, cell->success);
                assert_int_equals("3,2 should not have failed", 1, cell->failure);
            } else if (i == 2 && j == 0) {
                assert_int_equals("2,0 should not have succeeded", 64, cell->success);
                assert_int_equals("2,0 should have failed", 1, cell->failure);
            } else if (i == 1 && j == 1) {
                assert_int_equals("1,1 should not have succeeded", 0, cell->success);
                assert_int_equals("1,1 should not have failed", 127, cell->failure);
            } else if (i == 2 && j == 2) {
                assert_int_equals("2,2 should not have succeeded", 42, cell->success);
                assert_int_equals("2,2 should not have failed", 0, cell->failure);
            } else {
                assert_int_equals("(others) should not have succeeded", 0, cell->success);
                assert_int_equals("(others) should note have failed", 0, cell->failure);
            }
            assert_int_equals("Matched should be set to zero", 0, cell->matched);
        }
    }

    teardown();
}

static void check_matrix(int pos, learning_cell *test_matrix) {
    int left, right;
    learning_cell *cell, *test_cell;

    left = intervals[pos].name;

    assert_int_equals("start time wrong", intervals[pos].start, learn.stats[left].start);
    assert_int_equals("end time wrong", intervals[pos].end, learn.stats[left].end);
    assert_int_equals("should be seen", true, learn.stats[left].seen);
    // interval is left side
    for (right = 0; right < N_WORDS; right++) {
        cell = &learn.matrix[left * learn.size + right];
        test_cell = &test_matrix[left * learn.size + right];

        // log_msg(" / L(%d) r(%d)\n", left, right);
        assert_int_equals("matched is wrong", test_cell->matched, cell->matched);
        assert_int_equals("success is wrong", test_cell->success, cell->success);
        assert_int_equals("failure is wrong", test_cell->failure, cell->failure);
        assert_int_equals("last_length is wrong", test_cell->last_length, cell->last_length);
        assert_float_equals("average_length is wrong", test_cell->average_length, cell->average_length);
        assert_float_equals("length_variance_s is wrong", test_cell->length_variance_s, cell->length_variance_s);
    }
    // interval is right side
    right = intervals[pos].name;
    for (left = 0; left < N_WORDS; left++) {
        cell = &learn.matrix[left * learn.size + right];
        test_cell = &test_matrix[left * learn.size + right];

        // log_msg(" / l(%d) R(%d)\n", left, right);
        assert_int_equals("matched is wrong", test_cell->matched, cell->matched);
        assert_int_equals("success is wrong", test_cell->success, cell->success);
        assert_int_equals("failure is wrong", test_cell->failure, cell->failure);
        assert_int_equals("last_length is wrong", test_cell->last_length, cell->last_length);
        assert_float_equals("average_length is wrong", test_cell->average_length, cell->average_length);
        assert_float_equals("length_variance_s is wrong", test_cell->length_variance_s, cell->length_variance_s);
    }
}

void test_learn_interval(void) {
    int i;
    learning_cell test_matrix[N_WORDS][N_WORDS];

    // use the same sequence of intervals as we had in test_nfer
    setup();

    // initialize test_matrix
    clear_memory(&test_matrix, N_WORDS * N_WORDS * sizeof(learning_cell));

    // get started by adding the first interval
    learn_interval(&learn, &intervals[0]);
    check_matrix(0, (learning_cell *) test_matrix);

    // with the second interval there are things to check
    learn_interval(&learn, &intervals[1]);
    // 0 -> before
    test_matrix[0][1].matched = true;
    test_matrix[0][1].last_length = 1;
    // average length is still zero here actually...
    // test_matrix[0][1].average_length = 1;
    check_matrix(1, (learning_cell *) test_matrix);

    // third interval
    learn_interval(&learn, &intervals[2]);
    test_matrix[0][2].matched = 1;
    test_matrix[0][2].last_length = 2;
    test_matrix[1][2].matched = 1;
    test_matrix[1][2].last_length = 1;
    check_matrix(2, (learning_cell *) test_matrix);

    // fourth
    learn_interval(&learn, &intervals[3]);
    test_matrix[0][3].matched = 1;
    test_matrix[0][3].last_length = 3;
    test_matrix[1][3].matched = 1;
    test_matrix[1][3].last_length = 2;
    test_matrix[2][3].matched = 1;
    test_matrix[2][3].last_length = 1;
    check_matrix(3, (learning_cell *) test_matrix);

    // with the fifth interval we finally see a duplicate (2)
    learn_interval(&learn, &intervals[4]);
    // just set everything with 2 before X to fail, then change
    // the cells that aren't failures
    for (i = 0; i < N_WORDS; i++) {
        test_matrix[2][i].matched = 0;
        test_matrix[2][i].failure = 1;
    }
    // matched is a count and so we see how many were matched
    // last_length is then whatever the most recent length was
    test_matrix[0][2].matched = 2;
    test_matrix[0][2].last_length = 4;
    test_matrix[1][2].matched = 2;
    test_matrix[1][2].last_length = 3;

    test_matrix[2][2].success = 1;
    test_matrix[2][2].failure = 0;
    test_matrix[2][2].average_length = 2;
    test_matrix[2][3].success = 1;
    test_matrix[2][3].failure = 0;
    // last_length gets zeroed on success
    test_matrix[2][3].last_length = 0;
    test_matrix[2][3].average_length = 1;
    test_matrix[3][2].matched = 1;
    test_matrix[3][2].last_length = 1;
    check_matrix(4, (learning_cell *) test_matrix);

    // with the sixth it just gets harder (0)
    learn_interval(&learn, &intervals[5]);
    // do the same thing again
    for (i = 0; i < N_WORDS; i++) {
        test_matrix[0][i].matched = false;
        test_matrix[0][i].failure = 1;
    }
    test_matrix[0][0].success = 1;
    test_matrix[0][0].failure = 0;
    test_matrix[0][0].average_length = 5;
    test_matrix[0][1].success = 1;
    test_matrix[0][1].failure = 0;
    test_matrix[0][1].last_length = 0;
    test_matrix[0][1].average_length = 1;
    test_matrix[0][2].success = 0;
    test_matrix[0][2].failure = 1;
    test_matrix[0][3].success = 1;
    test_matrix[0][3].failure = 0;
    test_matrix[0][3].last_length = 0;
    test_matrix[0][3].average_length = 3;
    test_matrix[1][0].matched = 1;
    test_matrix[1][0].last_length = 4;
    test_matrix[2][0].matched = 1;
    test_matrix[2][0].last_length = 1;
    test_matrix[3][0].matched = 1;
    test_matrix[3][0].last_length = 2;
    check_matrix(5, (learning_cell *) test_matrix);

    // seventh (1)
    learn_interval(&learn, &intervals[6]);
    // do the same thing again
    for (i = 0; i < N_WORDS; i++) {
        test_matrix[1][i].matched = false;
        test_matrix[1][i].failure = 1;
    }
    test_matrix[1][0].success = 1;
    test_matrix[1][0].failure = 0;
    test_matrix[1][0].last_length = 0;
    test_matrix[1][0].average_length = 4;
    test_matrix[1][1].success = 1;
    test_matrix[1][1].failure = 0;
    test_matrix[1][1].last_length = 0;
    test_matrix[1][1].average_length = 5;
    test_matrix[1][2].success = 0;
    test_matrix[1][2].failure = 1;
    test_matrix[1][2].last_length = 3;
    test_matrix[1][3].success = 1;
    test_matrix[1][3].failure = 0;
    test_matrix[1][3].last_length = 0;
    test_matrix[1][3].average_length = 2;
    test_matrix[0][1].matched = 1;
    test_matrix[0][1].last_length = 1;
    test_matrix[2][1].matched = 1;
    test_matrix[2][1].last_length = 2;
    test_matrix[3][1].matched = 1;
    test_matrix[3][1].last_length = 3;
    check_matrix(6, (learning_cell *) test_matrix);

    // eighth (3)
    learn_interval(&learn, &intervals[7]);
    // do the same thing again
    for (i = 0; i < N_WORDS; i++) {
        test_matrix[3][i].matched = false;
        test_matrix[3][i].failure = 1;
    }
    test_matrix[3][0].success = 1;
    test_matrix[3][0].failure = 0;
    test_matrix[3][0].last_length = 0;
    test_matrix[3][0].average_length = 2;
    test_matrix[3][1].success = 1;
    test_matrix[3][1].failure = 0;
    test_matrix[3][1].last_length = 0;
    test_matrix[3][1].average_length = 3;
    test_matrix[3][2].success = 1;
    test_matrix[3][2].failure = 0;
    test_matrix[3][2].last_length = 0;
    test_matrix[3][2].average_length = 1;
    test_matrix[3][3].success = 1;
    test_matrix[3][3].failure = 0;
    test_matrix[3][3].last_length = 0;
    test_matrix[3][3].average_length = 4;
    test_matrix[0][3].matched = 1;
    test_matrix[0][3].last_length = 2;
    test_matrix[1][3].matched = 1;
    test_matrix[1][3].last_length = 1;
    test_matrix[2][3].matched = 1;
    test_matrix[2][3].last_length = 3;
    check_matrix(7, (learning_cell *) test_matrix);

    // ninth (3rd 2)
    learn_interval(&learn, &intervals[8]);
    // do the same thing again
    for (i = 0; i < N_WORDS; i++) {
            test_matrix[2][i].matched = false;
            test_matrix[2][i].failure = 2;
    }
    test_matrix[2][0].success = 1;
    test_matrix[2][0].failure = 1;
    test_matrix[2][0].last_length = 0;
    test_matrix[2][0].average_length = 1;
    test_matrix[2][1].success = 1;
    test_matrix[2][1].failure = 1;
    test_matrix[2][1].last_length = 0;
    test_matrix[2][1].average_length = 2;
    test_matrix[2][2].success = 2;
    test_matrix[2][2].failure = 0;
    test_matrix[2][2].last_length = 0;
    test_matrix[2][2].average_length = 3;
    test_matrix[2][2].length_variance_s = 2;
    test_matrix[2][3].success = 2;
    test_matrix[2][3].failure = 0;
    test_matrix[2][3].last_length = 0;
    test_matrix[2][3].average_length = 2;
    test_matrix[2][3].length_variance_s = 2;
    test_matrix[0][2].matched = 1;
    test_matrix[0][2].last_length = 3;
    test_matrix[1][2].matched = 1;
    test_matrix[1][2].last_length = 2;
    test_matrix[3][2].matched = 1;
    test_matrix[3][2].last_length = 1;
    check_matrix(8, (learning_cell *) test_matrix);

    teardown();
}
