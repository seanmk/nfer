/*
 * test_map.c
 *
 *  Created on: Apr 3, 2017
 *      Author: skauffma
 *
 *    nfer - a system for inferring abstractions of event streams
 *   Copyright (C) 2017  Sean Kauffman
 *
 *   This file is part of nfer.
 *   nfer is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#include "test.h"
#include "test_map.h"
#include "map.h"

static data_map map;

static void setup(void) {
    initialize_map(&map);
}

static void teardown(void) {
    destroy_map(&map);
}

void test_initialize_map(void) {
    setup();

    assert_int_equals("map should be empty", 0, map.space);
    assert_null("values should be null", map.values);

    teardown();
}
void test_destroy_map(void) {
    setup();

    teardown();

    assert_int_equals("map should be empty", 0, map.space);
    assert_null("values should be null", map.values);

    // this will segfault if null isn't handled
    destroy_map(NULL);
}
void test_map_set(void) {
    int i;
    map_value v;
    setup();

    v.type = string_type;
    v.value.string = 1;

    assert_int_equals("start should not be set", MAP_MISSING_KEY, map.start);

    map_set(&map, 2, &v);
    assert_int_equals("space should have grown", 2 + MAP_SIZE_INCREMENT, map.space);
    assert_int_equals("type should be set", v.type, map.values[2].entry.type);
    assert_int_equals("value should be set", v.value.string, map.values[2].entry.value.string);
    assert_int_equals("start is wrong", 2, map.start);
    assert_int_equals("next is wrong", MAP_MISSING_KEY, map.values[2].next);
    assert_int_equals("prior is wrong", MAP_MISSING_KEY, map.values[2].prior);

    for (i = 0; i < map.space; i++) {
        if (i != 2) {
            assert_int_equals("other types should be null", null_type, map.values[i].entry.type);
        }
    }

    v.value.string = 2;
    map_set(&map, 1, &v);
    assert_int_equals("space should not have grown", 2 + MAP_SIZE_INCREMENT, map.space);
    assert_int_equals("type should be set", v.type, map.values[1].entry.type);
    assert_int_equals("value should be set", v.value.string, map.values[1].entry.value.string);
    assert_int_equals("start is wrong", 1, map.start);
    assert_int_equals("next is wrong", 2, map.values[1].next);
    assert_int_equals("prior is wrong", MAP_MISSING_KEY, map.values[1].prior);
    assert_int_equals("prior is wrong", 1, map.values[2].prior);

    v.type = real_type;
    v.value.real = 3.14;
    map_set(&map, 2, &v);
    assert_int_equals("space should not have grown", 2 + MAP_SIZE_INCREMENT, map.space);
    assert_int_equals("type should be set", v.type, map.values[2].entry.type);
    assert_int_equals("value should be set", v.value.real, map.values[2].entry.value.real);
    assert_int_equals("start is wrong", 1, map.start);
    assert_int_equals("next is wrong", 2, map.values[1].next);
    assert_int_equals("prior is wrong", MAP_MISSING_KEY, map.values[1].prior);
    assert_int_equals("prior is wrong", 1, map.values[2].prior);

    v.value.real = 6.28;
    map_set(&map, 2 + MAP_SIZE_INCREMENT, &v);
    assert_int_equals("space should have grown", 2 + (MAP_SIZE_INCREMENT * 2), map.space);
    assert_int_equals("type should be set", v.type, map.values[2 + MAP_SIZE_INCREMENT].entry.type);
    assert_int_equals("value should be set", v.value.real, map.values[2 + MAP_SIZE_INCREMENT].entry.value.real);
    assert_int_equals("start is wrong", 2 + MAP_SIZE_INCREMENT, map.start);
    assert_int_equals("next is wrong", 1, map.values[2 + MAP_SIZE_INCREMENT].next);
    assert_int_equals("prior is wrong", MAP_MISSING_KEY, map.values[2 + MAP_SIZE_INCREMENT].prior);
    assert_int_equals("prior is wrong", 2 + MAP_SIZE_INCREMENT, map.values[1].prior);

    for (i = 0; i < map.space; i++) {
        if (i != 1 && i != 2 && i != 2 + MAP_SIZE_INCREMENT) {
            assert_int_equals("other types should be null", null_type, map.values[i].entry.type);
        }
    }

    teardown();
}
void test_map_get(void) {
    map_value set_value, value;
    setup();

    map_get(&map, 0, &value);
    assert_int_equals("should be missing value", null_type, value.type);
    assert_int_equals("should not have resized map", 0, map.space);
    assert_null("values should be null", map.values);

    map_get(&map, 1, &value);
    assert_int_equals("should be missing value", null_type, value.type);

    set_value.type = boolean_type;
    set_value.value.boolean = true;
    map_set(&map, 5, &set_value);
    set_value.type = integer_type;
    set_value.value.integer = 33;
    map_set(&map, 3, &set_value);

    map_get(&map, 0, &value);
    assert_int_equals("should be missing value", null_type, value.type);
    map_get(&map, 1, &value);
    assert_int_equals("should be missing value", null_type, value.type);

    map_get(&map, 5, &value);
    assert_int_equals("type should be set", boolean_type, value.type);
    assert_int_equals("key should be set", true, value.value.boolean);
    map_get(&map, 3, &value);
    assert_int_equals("type should be set", integer_type, value.type);
    assert_int_equals("key should be set", 33, value.value.integer);

    teardown();
}
void test_map_find(void) {
    map_key key;
    map_value value1, value2, value3;

    setup();

    value1.type = integer_type;
    value1.value.integer = 150;

    value2.type = string_type;
    value2.value.string = 2;

    map_set(&map, 3, &value1);
    map_set(&map, 1, &value2);

    key = map_find(&map, &value1);
    assert_int_equals("should find value", 3, key);

    key = map_find(&map, &value2);
    assert_int_equals("should find value", 1, key);

    value3.type = integer_type;
    value3.value.integer = 100;

    key = map_find(&map, &value3);
    assert_int_equals("should not find value", MAP_MISSING_KEY, key);

    teardown();
}
void test_map_has_key(void) {
    map_value value, value_null;

    setup();

    value.type = integer_type;
    value.value.integer = 150;

    value_null.type = null_type;
    value_null.value.boolean = false;

    map_set(&map, 1, &value);
    map_set(&map, 3, &value);
    map_set(&map, 4, &value_null);

    assert_false("Map key 0 should not be set", map_has_key(&map, 0));
    assert_true("Map key 1 should be set", map_has_key(&map, 1));
    assert_false("Map key 2 should not be set", map_has_key(&map, 2));
    assert_true("Map key 3 should be set", map_has_key(&map, 3));
    assert_false("Map key 4 should not be set", map_has_key(&map, 4));

    map_set(&map, 1, &value_null);

    assert_false("Map key 1 should not be set", map_has_key(&map, 1));

    teardown();
}
void test_map_unset_key(void) {
    map_key key;
    map_value value1, value2, value_null;
    map_iterator mit;

    setup();

    value1.type = integer_type;
    value1.value.integer = 150;

    value2.type = string_type;
    value2.value.string = 2;

    value_null.type = null_type;
    value_null.value.boolean = false;

    map_set(&map, 3, &value1);
    map_set(&map, 1, &value2);

    get_map_iterator(&map, &mit);
    assert_true("Map should have a next key", has_next_map_key(&mit));
    key = next_map_key(&mit);
    assert_int_equals("First key is wrong", 1, key);
    assert_true("Map should have a next key", has_next_map_key(&mit));
    key = next_map_key(&mit);
    assert_int_equals("Second key is wrong", 3, key);
    assert_false("Map should not have a next key", has_next_map_key(&mit));

    // unset one of the keys
    map_set(&map, 1, &value_null);

    get_map_iterator(&map, &mit);
    assert_true("Map should have a next key", has_next_map_key(&mit));
    key = next_map_key(&mit);
    assert_int_equals("First key is wrong", 3, key);
    assert_false("Map should not have a next key", has_next_map_key(&mit));

    teardown();
}

void test_empty_map_iterator(void) {
    map_iterator mit;

    // check null map
    get_map_iterator(NULL, &mit);
    assert_false("NULL map should have no keys", has_next_map_key(&mit));

    setup();

    // check empty but initialized map
    get_map_iterator(&map, &mit);
    assert_false("Empty map should have no keys", has_next_map_key(&mit));

    teardown();
}

void test_map_compare(void) {
    data_map map2;
    map_key key1, key2;
    map_value value1, value2;

    setup();
    initialize_map(&map2);

    // set a couple keys and values to use
    key1 = 2;
    key2 = 5;
    value1.type = integer_type;
    value1.value.integer = 11;
    value2.type = integer_type;
    value2.value.integer = 15;

    // first check the case where different keys are set
    // ----------
    // set a key on map 1
    map_set(&map, key1, &value1);
    // check that they are unequal before setting anything on map2
    assert_true("Maps with different keys should be unequal", map_compare(&map, &map2) > 0);
    map_set(&map2, key2, &value2);
    // should still be unequal, but map1 is still greater since it has a key unset on map2
    assert_true("Maps with different keys should be unequal", map_compare(&map, &map2) > 0);


    // then the same keys but the values are different
    // ----------------
    map_set(&map, key2, &value1);
    map_set(&map2, key1, &value2);
    assert_true("Maps with different values should be unequal", map_compare(&map, &map2) < 0);


    // then when both are the same
    // -------------
    map_set(&map, key2, &value2);
    map_set(&map2, key1, &value1);
    assert_int_equals("Equal maps should be equal", 0, map_compare(&map, &map2));

    // then check the case where there are many keys
    // ---------------
    // first when they aren't equal
    #define SET_VECTOR_LENGTH ((int)sizeof(unsigned long long))

    // we have to set keys above the threshold that can be stored in the bit vector
    // setting at the length should be sufficient
    map_set(&map, SET_VECTOR_LENGTH, &value1);
    // check that they are unequal before setting anything on map2
    assert_true("Maps with different keys should be unequal", map_compare(&map, &map2) > 0);

    // now set a different key on map 2
    map_set(&map2, SET_VECTOR_LENGTH + 1, &value2);
    // check that they are unequal
    assert_true("Maps with different keys should be unequal", map_compare(&map, &map2) > 0);

    // then when they are equal but the values are different
    map_set(&map, SET_VECTOR_LENGTH + 1, &value1);
    map_set(&map2, SET_VECTOR_LENGTH, &value2);
    // check that they are unequal
    assert_true("Maps with different keys should be unequal", map_compare(&map, &map2) < 0);

    // then when both are the same
    map_set(&map, SET_VECTOR_LENGTH + 1, &value2);
    map_set(&map2, SET_VECTOR_LENGTH, &value1);
    assert_int_equals("Equal maps should be equal", 0, map_compare(&map, &map2));

    destroy_map(&map2);
    teardown();
}
