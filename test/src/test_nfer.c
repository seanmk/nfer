/*
 * test_nfer.c
 *
 *  Created on: Jan 26, 2017
 *      Author: skauffma
 *
 *    nfer - a system for inferring abstractions of event streams
 *   Copyright (C) 2017  Sean Kauffman
 *
 *   This file is part of nfer.
 *   nfer is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>

#include "test.h"
#include "test_nfer.h"
#include "nfer.h"
#include "pool.h"
#include "log.h"

static nfer_specification spec;

#define INITIAL_SPEC_SIZE 4

static void setup(void) {
    initialize_specification(&spec, INITIAL_SPEC_SIZE);
}

static void teardown(void) {
    destroy_specification(&spec);
}

void test_initialize_specification(void) {
    unsigned int i;
    setup();

    // because size was non-zero in init call
    assert_int_equals("rule_list.space wrong", INITIAL_SPEC_SIZE, spec.space);
    assert_int_equals("rule_list.size wrong", 0, spec.size);
    assert_not_null("rule_list.rules null", spec.rules);
    assert_int_equals("rule_list.config.opt_window_size wrong", NO_WINDOW, spec.config.opt_window_size);
    assert_false("rule_list.config.opt_full true", spec.config.opt_full);

    // test setting up label equivalence?  Kinda hard to do, actually

    for (i = 0; i < spec.space * sizeof(nfer_rule); i++) {
        assert_int_equals("rules not cleared", 0, *((char *) spec.rules + i));
    }

    teardown();
}
void test_destroy_specification(void) {
    setup();

    // consider adding some rules here, to make sure they are properly deallocated
    teardown();

    assert_int_equals("rule_list.space wrong", 0, spec.space);
    assert_int_equals("rule_list.size wrong", 0, spec.size);
    assert_null("rule_list.rules not null", spec.rules);
}

// helper functions
static void check_rule_status(nfer_rule *rule, operator_code op_code, label lhs, label rhs, label res) {
    // check the rule fields
    assert_int_equals("operator is wrong", op_code, rule->op_code);
    assert_int_equals("lhs interval is wrong", lhs, rule->left_label);
    assert_int_equals("rhs interval is wrong", rhs, rule->right_label);
    assert_int_equals("result interval is wrong", res, rule->result_label);
}

static void check_cache_status(nfer_rule *rule) {
    // check the caches
    assert_int_equals("left cache should be empty", 0, rule->left_cache.size);
    assert_int_equals("left cache should be initialized", INITIAL_POOL_SIZE, rule->left_cache.space);
    assert_int_equals("right cache should be empty", 0, rule->right_cache.size);
    assert_int_equals("right cache should be initialized", INITIAL_POOL_SIZE, rule->right_cache.space);
    assert_int_equals("produced cache should be empty", 0, rule->produced.size);
    assert_int_equals("produced cache should be initialized", INITIAL_POOL_SIZE, rule->produced.space);
}


void test_add_rule_to_specification(void) {
    nfer_rule *rule1, *rule2, *rule3, *rule4;
    unsigned int i;
    setup();

    // add a rule
    add_rule_to_specification(&spec, 1, 1, BEFORE_OPERATOR, 1, NULL);
    // check the rules list
    assert_int_equals("size is wrong", 1, spec.size);
    rule1 = &spec.rules[spec.size - 1];
    check_rule_status(rule1, 1, 1, 1, 1);
    check_cache_status(rule1);

    // add another rule
    add_rule_to_specification(&spec, 2, 1, MEET_OPERATOR, 2, NULL);
    // check the rules list
    assert_int_equals("size is wrong", 2, spec.size);
    rule2 = &spec.rules[spec.size - 1];
    check_rule_status(rule2, 2, 1, 2, 2);
    check_cache_status(rule2);

    // add 2 more
    add_rule_to_specification(&spec, 3, 1, DURING_OPERATOR, 3, NULL);
    rule3 = &spec.rules[spec.size - 1];
    check_rule_status(rule3, 3, 1, 3, 3);
    check_cache_status(rule3);
    add_rule_to_specification(&spec, 0, 1, COINCIDE_OPERATOR, 0, NULL);
    rule4 = &spec.rules[spec.size - 1];
    check_rule_status(rule4, 4, 1, 0, 0);
    check_cache_status(rule4);

    // check the rules list
    assert_int_equals("size is wrong", 4, spec.size);
    assert_int_equals("space is wrong", INITIAL_SPEC_SIZE, spec.space);

    // now add another interval so the rules list must be resized
    add_rule_to_specification(&spec, 1, 1, BEFORE_OPERATOR, 1, NULL);

    assert_int_equals("size is wrong", INITIAL_SPEC_SIZE + 1, spec.size);
    assert_int_equals("space is wrong", INITIAL_SPEC_SIZE * 2, spec.space);

    for (i = (INITIAL_SPEC_SIZE + 1) * sizeof(nfer_rule); i < spec.space * sizeof(nfer_rule); i++) {
        assert_int_equals("rules not cleared", 0, *((char *) spec.rules + i));
    }

    teardown();
}

void test_move_rule(void) {
    nfer_rule *src_rule1, *src_rule2;
    nfer_rule *dest_rule1, *dest_rule2;
    nfer_specification dest_spec;
    map_key key;
    map_value value, check_value;
    expression_input *map_expression, *check_expression;
    
    setup();
    initialize_specification(&dest_spec, 2);
    dest_rule1 = &dest_spec.rules[0];
    dest_rule2 = &dest_spec.rules[1];
    dest_spec.size = 2;

    // add a rule
    add_rule_to_specification(&spec, 1, 1, BEFORE_OPERATOR, 1, NULL);
    src_rule1 = &spec.rules[spec.size - 1];

    // this rule has very little going on - just check basic behavior
    move_rule(dest_rule1, src_rule1);
    check_rule_status(dest_rule1, 1, 1, 1, 1);
    check_cache_status(dest_rule1);

    // add another rule - this time set up a lot more
    add_rule_to_specification(&spec, 2, 1, MEET_OPERATOR, 2, NULL);
    src_rule2 = &spec.rules[spec.size - 1];
    // initialize and do something with an expression
    initialize_expression_input(&src_rule2->where_expression, 2);
    src_rule2->where_expression[0].length = 2;
    src_rule2->where_expression[1].boolean_value = true;
    // initialize and do something with the map expression
    initialize_expression_input(&map_expression, 2);
    map_expression[0].length = 2;
    map_expression[1].integer_value = 10;
    initialize_map(&src_rule2->map_expressions);
    key = 3;
    value.type = pointer_type;
    value.value.pointer = map_expression;
    map_set(&src_rule2->map_expressions, key, &value);
    
    // now try moving it
    move_rule(dest_rule2, src_rule2);
    // initial checks
    check_rule_status(dest_rule2, 2, 1, 2, 2);
    check_cache_status(dest_rule2);
    // now test that the expressions all got moved over
    // first check where
    assert_int_equals("Dest where expr length wrong", 2, dest_rule2->where_expression[0].length);
    assert_true("Dest where expr value wrong",  dest_rule2->where_expression[1].boolean_value);
    assert_null("Src where expr should be nulled", src_rule2->where_expression);
    // now check map expr
    assert_true("Dest map expr should contain key", map_has_key(&dest_rule2->map_expressions, key));
    map_get(&dest_rule2->map_expressions, key, &check_value);
    assert_int_equals("Map expr should be pointer", pointer_type, check_value.type);
    assert_not_null("Map expr pointer should be set", check_value.value.pointer);
    check_expression = (expression_input*) check_value.value.pointer;
    assert_int_equals("Map expression length wrong", 2, check_expression[0].length);
    assert_int_equals("Map expression value wrong", 10, check_expression[1].integer_value);

    teardown();

    // after the spec is destroyed, first verify that the map expr is intact
    assert_true("Dest map expr should contain key after src free", map_has_key(&dest_rule2->map_expressions, key));
    map_get(&dest_rule2->map_expressions, key, &check_value);
    assert_int_equals("Map expr should be pointer after src free", pointer_type, check_value.type);
    assert_not_null("Map expr pointer should be set after src free", check_value.value.pointer);
    check_expression = (expression_input*) check_value.value.pointer;
    assert_int_equals("Map expression length wrong after src free", 2, check_expression[0].length);
    assert_int_equals("Map expression value wrong after src free", 10, check_expression[1].integer_value);

    // then free the destination spec
    destroy_specification(&dest_spec);
}

/*
 * Rules:
 * 4 <- 0 before 2
 * 5 <- 1 before 3
 *
 * 6 <- 4 slice 5
 *
 * Trace:
 * 0 @ 0 -- 4
 * 1 @ 1 ---|-- 5-- 6
 * 2 @ 2 -- 4---|-- 6
 * 3 @ 3 -------5
 * 2 @ 4
 * 0 @ 5 ------ 4
 * 1 @ 6 -- 5---|-- 6
 * 3 @ 7 -- 5---|-- 6
 * 2 @ 8 ------ 4
 */
void test_apply_rule(void) {
#define N_INTS 9
    interval intervals[] = {
            { 0, 0, 0, EMPTY_MAP, false },
            { 1, 1, 1, EMPTY_MAP, false },
            { 2, 2, 2, EMPTY_MAP, false },
            { 3, 3, 3, EMPTY_MAP, false },
            { 2, 4, 4, EMPTY_MAP, false },
            { 0, 5, 5, EMPTY_MAP, false },
            { 1, 6, 6, EMPTY_MAP, false },
            { 3, 7, 7, EMPTY_MAP, false },
            { 2, 8, 8, EMPTY_MAP, false } };
    pool in, out1;
    nfer_rule *rule0b2, *rule1b3, *rule4s5;
    pool_iterator pq, pq_out;
    interval *i;

    setup();
    initialize_pool(&in);
    initialize_pool(&out1);

    // setup input
    add_interval(&in, &intervals[0]);
    add_interval(&in, &intervals[1]);
    add_interval(&in, &intervals[2]);
    add_interval(&in, &intervals[3]);
    add_interval(&in, &intervals[4]);
    add_interval(&in, &intervals[5]);
    add_interval(&in, &intervals[6]);
    add_interval(&in, &intervals[7]);
    add_interval(&in, &intervals[8]);

    // setup rules
    rule0b2 = add_rule_to_specification(&spec, 4, 0, BEFORE_OPERATOR, 2, NULL); // 4 <- 0 before 2
    rule1b3 = add_rule_to_specification(&spec, 5, 1, BEFORE_OPERATOR, 3, NULL); // 5 <- 1 before 3
    rule4s5 = add_rule_to_specification(&spec, 6, 4, SLICE_OPERATOR, 5, NULL); // 6 <- 4 slice 5

    // set up input pool queue
    get_pool_queue(&in, &pq, QUEUE_FROM_BEGINNING);
    // set up output pool queue
    get_pool_queue(&out1, &pq_out, QUEUE_FROM_END);

    // apply first rule
    apply_rule(rule0b2, &pq, &out1, &spec.config, &spec.equivalent_labels);

    assert_int_equals("out size wrong", 2, out1.size);
    assert_int_equals("left_size wrong", 2, rule0b2->left_cache.size);
    assert_int_equals("right_size wrong", 3, rule0b2->right_cache.size);


    assert_int_equals("generated interval name wrong", 4, out1.intervals[0].i.name);
    assert_int_equals("generated interval start wrong", 0, out1.intervals[0].i.start);
    assert_int_equals("generated interval end wrong", 2, out1.intervals[0].i.end);

    assert_int_equals("generated interval name wrong", 4, out1.intervals[1].i.name);
    assert_int_equals("generated interval start wrong", 5, out1.intervals[1].i.start);
    assert_int_equals("generated interval end wrong", 8, out1.intervals[1].i.end);

    // set up input pool queue again
    get_pool_queue(&in, &pq, QUEUE_FROM_BEGINNING);

    // apply second rule
    apply_rule(rule1b3, &pq, &out1, &spec.config, &spec.equivalent_labels);

    assert_int_equals("out size wrong", 4, out1.size);
    assert_int_equals("left_size wrong", 2, rule1b3->left_cache.size);
    assert_int_equals("right_size wrong", 2, rule1b3->right_cache.size);

    assert_int_equals("generated interval name wrong", 5, out1.intervals[2].i.name);
    assert_int_equals("generated interval start wrong", 1, out1.intervals[2].i.start);
    assert_int_equals("generated interval end wrong", 3, out1.intervals[2].i.end);

    assert_int_equals("generated interval name wrong", 5, out1.intervals[3].i.name);
    assert_int_equals("generated interval start wrong", 6, out1.intervals[3].i.start);
    assert_int_equals("generated interval end wrong", 7, out1.intervals[3].i.end);

    // copy the outputs to the input
    while (has_next_queue_interval(&pq_out)) {
        i = next_queue_interval(&pq_out);
        add_interval(&in, i);
    }

    // set up input pool queue again
    get_pool_queue(&in, &pq, QUEUE_FROM_BEGINNING);

    // apply second rule
    apply_rule(rule4s5, &pq, &out1, &spec.config, &spec.equivalent_labels);

    assert_int_equals("out size wrong", 6, out1.size);
    assert_int_equals("left_size wrong", 2, rule4s5->left_cache.size);
    assert_int_equals("right_size wrong", 2, rule4s5->right_cache.size);

    assert_int_equals("generated interval name wrong", 6, out1.intervals[4].i.name);
    assert_int_equals("generated interval start wrong", 1, out1.intervals[4].i.start);
    assert_int_equals("generated interval end wrong", 2, out1.intervals[4].i.end);

    assert_int_equals("generated interval name wrong", 6, out1.intervals[5].i.name);
    assert_int_equals("generated interval start wrong", 6, out1.intervals[5].i.start);
    assert_int_equals("generated interval end wrong", 7, out1.intervals[5].i.end);

    destroy_pool(&in);
    destroy_pool(&out1);
    teardown();
}

/*
 * Rules:
 * 4 <- 0 before 2
 * 5 <- 1 before 3
 *
 * 6 <- 4 slice 5
 *
 * Trace:
 * 0 @ 0 -- 4
 * 1 @ 1 ---|-- 5-- 6
 * 2 @ 2 -- 4---|-- 6
 * 3 @ 3 -------5
 * 2 @ 4
 * 0 @ 5 ------ 4
 * 1 @ 6 -- 5---|-- 6
 * 3 @ 7 -- 5---|-- 6
 * 2 @ 8 ------ 4
 */
void test_run_nfer(void) {
#define N_INTS 9
    interval intervals[] = {
            { 0, 0, 0, EMPTY_MAP, false },
            { 1, 1, 1, EMPTY_MAP, false },
            { 2, 2, 2, EMPTY_MAP, false },
            { 3, 3, 3, EMPTY_MAP, false },
            { 2, 4, 4, EMPTY_MAP, false },
            { 0, 5, 5, EMPTY_MAP, false },
            { 1, 6, 6, EMPTY_MAP, false },
            { 3, 7, 7, EMPTY_MAP, false },
            { 2, 8, 8, EMPTY_MAP, false }
    };
    int i;
    pool in, out, in2, out2;
    interval *check;
    pool_iterator pit;
    nfer_rule *rules[3];

    setup();
    initialize_pool(&in);
    initialize_pool(&in2);
    initialize_pool(&out);
    initialize_pool(&out2);

    // add the rules
    rules[0] = add_rule_to_specification(&spec, 4, 0, BEFORE_OPERATOR, 2, NULL); // 4 <- 0 before 2
    rules[1] = add_rule_to_specification(&spec, 5, 1, BEFORE_OPERATOR, 3, NULL); // 5 <- 1 before 3
    rules[2] = add_rule_to_specification(&spec, 6, 4, SLICE_OPERATOR, 5, NULL); // 6 <- 4 slice 5

    // add the intervals to the input pool
    for (i = 0; i < N_INTS; i++) {
        add_interval(&in, &intervals[i]);
        add_interval(&in2, &intervals[i]);
    }

    // run nfer
    run_nfer(&spec, &in, &out);

    get_pool_iterator(&out, &pit);

    // check the results
    assert_int_equals("input size wrong", N_INTS + 6, in.size);
    assert_int_equals("output size wrong", 6, out.size);

    check = next_interval(&pit);
    assert_int_equals("generated interval name wrong", 4, check->name);
    assert_int_equals("generated interval start wrong", 0, check->start);
    assert_int_equals("generated interval end wrong", 2, check->end);

    check = next_interval(&pit);
    assert_int_equals("generated interval name wrong", 6, check->name);
    assert_int_equals("generated interval start wrong", 1, check->start);
    assert_int_equals("generated interval end wrong", 2, check->end);

    check = next_interval(&pit);
    assert_int_equals("generated interval name wrong", 5, check->name);
    assert_int_equals("generated interval start wrong", 1, check->start);
    assert_int_equals("generated interval end wrong", 3, check->end);

    check = next_interval(&pit);
    assert_int_equals("generated interval name wrong", 5, check->name);
    assert_int_equals("generated interval start wrong", 6, check->start);
    assert_int_equals("generated interval end wrong", 7, check->end);

    check = next_interval(&pit);
    assert_int_equals("generated interval name wrong", 6, check->name);
    assert_int_equals("generated interval start wrong", 6, check->start);
    assert_int_equals("generated interval end wrong", 7, check->end);

    check = next_interval(&pit);
    assert_int_equals("generated interval name wrong", 4, check->name);
    assert_int_equals("generated interval start wrong", 5, check->start);
    assert_int_equals("generated interval end wrong", 8, check->end);

    /*
    * Idempotent Trace:
    * 0 @ 0 -- 4-- 4-- 4
    * 1 @ 1 ---|---|---|-- 5-- 5-- 6-- 6-- 6-- 6
    * 2 @ 2 -- 4---|---|---|---|-- 6---|---|---|
    * 3 @ 3 -------|---|-- 5---|-------6---|---|
    * 2 @ 4 -------4---|-------|---------- 6---|
    * 0 @ 5 ------ 4---|-------|---------- 6---|
    * 1 @ 6 -- 5---|---|-------|-- 6-------|---|
    * 3 @ 7 -- 5---|---|-------5-- 6------ 6-- 6
    * 2 @ 8 ------ 4-- 4
    */
    // disable minimality
    spec.config.opt_full = true;
    // clean up the caches before we run
    for (i = 0; i < 3; i++) {
        clear_pool(&rules[i]->left_cache);
        clear_pool(&rules[i]->right_cache);
        clear_pool(&rules[i]->produced);
    }

    // run nfer
    run_nfer(&spec, &in2, &out2);
    
    // get_pool_iterator(&out2, &pit);
    // while (has_next_interval(&pit)) {
    //     check = next_interval(&pit);
    //     log_interval(check);
    // }

    get_pool_iterator(&out2, &pit);

    // check the results
    // TODO: this is not really right - there should only be 13 but there are 3 duplicates
    // we require set semantics for pools before this really works correctly
    assert_int_equals("input size wrong", N_INTS + 16, in2.size);
    assert_int_equals("output size wrong", 16, out2.size);
    assert_int_equals("produced cache should be empty", 0, rules[0]->produced.size);
    assert_int_equals("produced cache should be empty", 0, rules[1]->produced.size);
    assert_int_equals("produced cache should be empty", 0, rules[2]->produced.size);

    // don't bother checking them all...

    // teardown
    destroy_pool(&out);
    destroy_pool(&in);
    destroy_pool(&out2);
    destroy_pool(&in2);
    teardown();
}

void test_minimality(void) {
    interval intervals[] = {
            { 0, 10, 15, EMPTY_MAP, false },
            { 0, 20, 25, EMPTY_MAP, false },
            { 1, 30, 35, EMPTY_MAP, false }
    };
    pool out, in;
    nfer_rule *rule;

    setup();
    initialize_pool(&in);
    initialize_pool(&out);

    // add the rules
    rule = add_rule_to_specification(&spec, 2, 0, BEFORE_OPERATOR, 1, NULL); // 2 <- 0 before 1

    // set up the input poool
    add_interval(&in, &intervals[0]);
    add_interval(&in, &intervals[1]);
    add_interval(&in, &intervals[2]);
    // set up the input queue to point at the beginning
    get_pool_queue(&in, &rule->input_queue, QUEUE_FROM_BEGINNING);

    apply_rule(rule, &rule->input_queue, &out, &spec.config, &spec.equivalent_labels);
    assert_int_equals("One interval should be generated", 1, out.size);

    assert_int_equals("Name wrong", 2, out.intervals[0].i.name);
    assert_int_equals("Start wrong", 20, out.intervals[0].i.start);
    assert_int_equals("End wrong", 35, out.intervals[0].i.end);

    destroy_pool(&out);
    destroy_pool(&in);
    teardown();
}

void test_select_minimal(void) {
    label mark1 = 0;
    interval intervals[] = {
            { mark1, 10, 20, EMPTY_MAP, false },
            { mark1, 15, 25, EMPTY_MAP, false }, // completely contains 18/23 - should be removed
            { mark1, 8, 19, EMPTY_MAP, false },  // overlapping
            { mark1, 18, 23, EMPTY_MAP, false }, // overlaps several
            { mark1, 5, 30, EMPTY_MAP, false }   // longer than everything - should be removed
    };
    interval intervals2[] = {
            { mark1, 9, 15, EMPTY_MAP, false },  // within the 3rd interval
            { mark1, 2, 32, EMPTY_MAP, false },  // longer than everything
    };
    pool potentials, prior;
    pool_iterator pit;
    interval *check;
    int i;

    setup();

    initialize_pool(&potentials);
    initialize_pool(&prior);

    // first add everything into the potentials pool
    for (i = 0; i < 5; i++) {
        add_interval(&potentials, &intervals[i]);
    }

    // prior is empty, check if the right things are filtered from potentials
    select_minimal(&potentials, &prior);

    get_pool_iterator(&potentials, &pit);
    while(has_next_interval(&pit)) {
        check = next_interval(&pit);
        assert_true("Non-minimal interval in potentials not deleted", (check->start != 15 || check->end != 25) && (check->start != 5 || check->end != 30));
    }
    assert_int_equals("Wrong number of intervals removed", 2, potentials.removed);

    // now we want to test that things are filtered when there's stuff in prior
    // add to the prior pool
    for (i = 0; i < 2; i++) {
        add_interval(&prior, &intervals2[i]);
    }

    // run it again
    select_minimal(&potentials, &prior);

    get_pool_iterator(&potentials, &pit);
    while(has_next_interval(&pit)) {
        check = next_interval(&pit);
        assert_true("Non-minimal interval in potentials not deleted", (check->start != 8 || check->end != 22));
    }
    assert_int_equals("Wrong number of intervals removed", 3, potentials.removed);
    assert_int_equals("Nothing should be removed from prior", 0, prior.removed);

    destroy_pool(&potentials);
    destroy_pool(&prior);
    teardown();
}

void test_sspss_intervals(void) {
    label mark1 = 0, mark2 = 1, mark3 = 2, mark4 = 3, mark5 = 4;
    label acquisition = 5, processing1 = 6, processing2wc = 7, processing2to5 = 8, processing2woc = 9, communication =
            10, sleep = 11, processing = 12, handling = 13, finalization = 14, sspss = 15;
    int i;
    interval intervals[] = {
            { mark2, 3570703667UL, 3570703667UL, EMPTY_MAP, false },
            { mark3, 3871719292UL, 3871719292UL, EMPTY_MAP, false },
            { mark4, 26884595250UL, 26884595250UL, EMPTY_MAP, false },
            { mark5, 29364814584UL, 29364814584UL, EMPTY_MAP, false },
            { mark1, 36433289167UL, 36433289167UL, EMPTY_MAP, false },
            { mark2, 45096812209UL, 45096812209UL, EMPTY_MAP, false },
            { mark3, 45406664209UL, 45406664209UL, EMPTY_MAP, false },
//			{mark4, 68212600750UL, 68212600750, EMPTY_MAP}, // remove this because we want to see a non-negation
            { mark5, 70795198750UL, 70795198750UL, EMPTY_MAP, false },
            { mark1, 77865924917UL, 77865924917UL, EMPTY_MAP, false } };

    pool in, out;
    pool_iterator pit;
    interval *check;

    setup();
    initialize_pool(&in);
    initialize_pool(&out);

    // add all the test intervals to the pool
    for (i = 0; i < 9; i++) {
        add_interval(&in, &intervals[i]);
    }

    // add the rules
    add_rule_to_specification(&spec, acquisition, mark1, BEFORE_OPERATOR, mark2, NULL);
    add_rule_to_specification(&spec, processing1, mark2, BEFORE_OPERATOR, mark3, NULL);
    // we need to actually exclude the presence of mark4 from the woc version...
    add_rule_to_specification(&spec, processing2wc, mark3, BEFORE_OPERATOR, mark4, NULL);
    add_rule_to_specification(&spec, processing2to5, mark3, BEFORE_OPERATOR, mark5, NULL);
    // this ensures that we don't get a processing2woc unless comm is missing
    add_rule_to_specification(&spec, processing2woc, processing2to5, CONTAIN_OPERATOR, mark4, NULL);

    add_rule_to_specification(&spec, communication, mark4, BEFORE_OPERATOR, mark5, NULL);
    add_rule_to_specification(&spec, sleep, mark5, BEFORE_OPERATOR, mark1, NULL);

    // disjunction...
    add_rule_to_specification(&spec, processing, processing1, MEET_OPERATOR, processing2wc, NULL);
    add_rule_to_specification(&spec, processing, processing1, MEET_OPERATOR, processing2woc, NULL);

    add_rule_to_specification(&spec, handling, acquisition, MEET_OPERATOR, processing, NULL);
    // more disjunction...
    add_rule_to_specification(&spec, finalization, communication, MEET_OPERATOR, sleep, NULL);
    add_rule_to_specification(&spec, finalization, sleep, FOLLOW_OPERATOR, communication, NULL);

    // wrap it up
    add_rule_to_specification(&spec, sspss, handling, MEET_OPERATOR, finalization, NULL);

    // actually run nfer
    run_nfer(&spec, &in, &out);

    assert_int_equals("size of out is wrong", 16, out.size);

//    get_pool_iterator(&out, &pit);
//    while (has_next_interval(&pit)) {
//        check = next_interval(&pit);
//        log_interval(check);
//    }

    get_pool_iterator(&out, &pit);

    check = next_interval(&pit);
    assert_int_equals("result 1 label is wrong", processing1, check->name);
    assert_int_equals("result 1 start is wrong", intervals[0].start, check->start);
    assert_int_equals("result 1 end is wrong", intervals[1].end, check->end);

    check = next_interval(&pit);
    assert_int_equals("result 2 label is wrong", processing, check->name);
    assert_int_equals("result 2 start is wrong", intervals[0].start, check->start);
    assert_int_equals("result 2 end is wrong", intervals[2].end, check->end);

    check = next_interval(&pit);
    assert_int_equals("result 3 label is wrong", processing2wc, check->name);
    assert_int_equals("result 3 start is wrong", intervals[1].start, check->start);
    assert_int_equals("result 3 end is wrong", intervals[2].end, check->end);

    check = next_interval(&pit);
    assert_int_equals("result 4 label is wrong", processing2to5, check->name);
    assert_int_equals("result 4 start is wrong", intervals[1].start, check->start);
    assert_int_equals("result 4 end is wrong", intervals[3].end, check->end);

    check = next_interval(&pit);
    assert_int_equals("result 5 label is wrong", communication, check->name);
    assert_int_equals("result 5 start is wrong", intervals[2].start, check->start);
    assert_int_equals("result 5 end is wrong", intervals[3].end, check->end);

    check = next_interval(&pit);
    assert_int_equals("result 6 label is wrong", finalization, check->name);
    assert_int_equals("result 6 start is wrong", intervals[2].start, check->start);
    assert_int_equals("result 6 end is wrong", intervals[4].end, check->end);

    check = next_interval(&pit);
    assert_int_equals("result 7 label is wrong", sleep, check->name);
    assert_int_equals("result 7 start is wrong", intervals[3].start, check->start);
    assert_int_equals("result 7 end is wrong", intervals[4].end, check->end);

    check = next_interval(&pit);
    assert_int_equals("result 8 label is wrong", acquisition, check->name);
    assert_int_equals("result 8 start is wrong", intervals[4].start, check->start);
    assert_int_equals("result 8 end is wrong", intervals[5].end, check->end);

    check = next_interval(&pit);
    assert_int_equals("result 9 label is wrong", processing1, check->name);
    assert_int_equals("result 9 start is wrong", intervals[5].start, check->start);
    assert_int_equals("result 9 end is wrong", intervals[6].end, check->end);

    check = next_interval(&pit);
    assert_int_equals("result 10 label is wrong", handling, check->name);
    assert_int_equals("result 10 start is wrong", intervals[4].start, check->start);
    assert_int_equals("result 10 end is wrong", intervals[7].end, check->end);

    check = next_interval(&pit);
    assert_int_equals("result 11 label is wrong", processing, check->name);
    assert_int_equals("result 11 start is wrong", intervals[5].start, check->start);
    assert_int_equals("result 11 end is wrong", intervals[7].end, check->end);

    check = next_interval(&pit);
    assert_int_equals("result 12 label is wrong", processing2to5, check->name);
    assert_int_equals("result 12 start is wrong", intervals[6].start, check->start);
    assert_int_equals("result 12 end is wrong", intervals[7].end, check->end);

    check = next_interval(&pit);
    assert_int_equals("result 13 label is wrong", processing2woc, check->name);
    assert_int_equals("result 13 start is wrong", intervals[6].start, check->start);
    assert_int_equals("result 13 end is wrong", intervals[7].end, check->end);

    check = next_interval(&pit);
    assert_int_equals("result 14 label is wrong", sspss, check->name);
    assert_int_equals("result 14 start is wrong", intervals[4].start, check->start);
    assert_int_equals("result 14 end is wrong", intervals[8].end, check->end);

    check = next_interval(&pit);
    assert_int_equals("result 15 label is wrong", sleep, check->name);
    assert_int_equals("result 15 start is wrong", intervals[7].start, check->start);
    assert_int_equals("result 15 end is wrong", intervals[8].end, check->end);

    check = next_interval(&pit);
    assert_int_equals("result 16 label is wrong", finalization, check->name);
    assert_int_equals("result 16 start is wrong", intervals[7].start, check->start);
    assert_int_equals("result 16 end is wrong", intervals[8].end, check->end);

    destroy_pool(&out);
    destroy_pool(&in);
    teardown();
}

static bool within300(
        timestamp ls, timestamp UNUSED(le), data_map *UNUSED(left),
        timestamp UNUSED(rs), timestamp re, data_map *UNUSED(right)) {
    return (re - ls) < 300;
}

void test_prolog_intervals(void) {
    //    Adding interval to spec (24,40,46)
    //    Adding interval to spec (24,44,50)
    label boot_s_f = 0, downlink = 1, boot_s_a = 2, boot_s_d = 3,
            boot_e_f = 4, boot_e_d = 5, boot_e_a = 6;
    label boot = 7, dboot = 8, risk = 9;
    interval intervals[] = {
            { boot_s_f, 40, 40, EMPTY_MAP, false },
            { downlink, 41, 41, EMPTY_MAP, false },
            { boot_s_a, 42, 42, EMPTY_MAP, false },
            { boot_s_d, 44, 44, EMPTY_MAP, false },
            { boot_e_f, 46, 46, EMPTY_MAP, false },
            { downlink, 48, 48, EMPTY_MAP, false },
            { boot_e_d, 50, 50, EMPTY_MAP, false }
    };
    pool in, out;
    interval *check;
    int i;
    phi_function within300_phi = { "WITHIN300", within300, NULL };

    setup();
    initialize_pool(&in);
    initialize_pool(&out);

    // add all the test intervals to the pool
    for (i = 0; i < 7; i++) {
        add_interval(&in, &intervals[i]);
    }

    add_rule_to_specification(&spec, boot, boot_s_a, BEFORE_OPERATOR, boot_e_a, NULL);
    add_rule_to_specification(&spec, boot, boot_s_d, BEFORE_OPERATOR, boot_e_d, NULL);
    add_rule_to_specification(&spec, boot, boot_s_f, BEFORE_OPERATOR, boot_e_f, NULL);

    add_rule_to_specification(&spec, dboot, boot, BEFORE_OPERATOR, boot, &within300_phi);
    add_rule_to_specification(&spec, risk, downlink, DURING_OPERATOR, dboot, NULL);

    // actually run nfer
    run_nfer(&spec, &in, &out);

    assert_int_equals("result size is wrong", 2, out.size);
    i = 0;

    check = &out.intervals[i++].i;
    assert_int_equals("result label is wrong", boot, check->name);
    assert_int_equals("result start is wrong", intervals[3].start, check->start);
    assert_int_equals("result end is wrong", intervals[6].end, check->end);

    check = &out.intervals[i++].i;
    assert_int_equals("result label is wrong", boot, check->name);
    assert_int_equals("result start is wrong", intervals[0].start, check->start);
    assert_int_equals("result end is wrong", intervals[4].end, check->end);

    destroy_pool(&out);
    destroy_pool(&in);
    teardown();
}

static bool equal_event_string(
        timestamp UNUSED(ls), timestamp UNUSED(le), data_map *left,
        timestamp UNUSED(rs), timestamp UNUSED(re), data_map *right) {
    map_value left_value, right_value;

    map_get(left, 0, &left_value);
    map_get(right, 0, &right_value);
    if (left_value.type != string_type || right_value.type != string_type) {
        return false;
    }

    return left_value.value.string == right_value.value.string;
}

static void first_event(data_map *dest,
        timestamp UNUSED(ls), timestamp UNUSED(le), data_map *left,
        timestamp UNUSED(rs), timestamp UNUSED(re), data_map *UNUSED(right)) {
    map_value value;
    map_get(left, 0, &value);
    map_set(dest, 0, &value);
}

static phi_function equal_phi = { "EQUAL_EVENT", equal_event_string, first_event };

void test_beaglebone_intervals(void) {
    label int_enter = 0, int_exit = 1, hand_enter = 2, hand_exit = 3,
            interrupt = 4, handler = 5, handled = 6;
    map_key event = 0;
    map_value v44, v29, check_map, expected_map;
    interval intervals[] = {
            { int_enter, 10, 10, EMPTY_MAP, false },
            { hand_enter, 20, 20, EMPTY_MAP, false },
            { int_enter, 25, 25, EMPTY_MAP, false },
            { hand_exit, 30, 30, EMPTY_MAP, false },
            { hand_enter, 35, 35, EMPTY_MAP, false },
            { int_exit, 40, 40, EMPTY_MAP, false },
            { hand_exit, 45, 45, EMPTY_MAP, false },
            { int_exit, 55, 55, EMPTY_MAP, false }
    };
    pool in, out;
    pool_iterator pit;
    interval *check;
    int i;

    setup();
    initialize_pool(&in);
    initialize_pool(&out);

    v44.type = string_type;
    v44.value.string = 0;
    v29.type = string_type;
    v29.value.string = 1;

    // add all the test intervals to the pool
    for (i = 0; i < 8; i++) {
        initialize_map(&intervals[i].map);
        // a lazy way of differentiating...
        if (intervals[i].start % 10 == 0) {
            map_set(&intervals[i].map, event, &v44);
        } else {
            map_set(&intervals[i].map, event, &v29);
        }
        add_interval(&in, &intervals[i]);
    }

    add_rule_to_specification(&spec, interrupt, int_enter, BEFORE_OPERATOR, int_exit, &equal_phi);
    add_rule_to_specification(&spec, handler, hand_enter, BEFORE_OPERATOR, hand_exit, &equal_phi);
    add_rule_to_specification(&spec, handled, handler, DURING_OPERATOR, interrupt, &equal_phi);

    // actually run nfer
    run_nfer(&spec, &in, &out);

    assert_int_equals("result size is wrong", 6, out.size);

    get_pool_iterator(&out, &pit);
    check = next_interval(&pit);
    map_get(&check->map, event, &check_map);
    assert_int_equals("1 result label is wrong", handler, check->name);
    assert_int_equals("1 result start is wrong", intervals[1].start, check->start);
    assert_int_equals("1 result end is wrong", intervals[3].end, check->end);
    map_get(&intervals[1].map, event, &expected_map);
    assert_int_equals("1 map value is wrong", expected_map.value.string, check_map.value.string);

    check = next_interval(&pit);
    map_get(&check->map, event, &check_map);
    assert_int_equals("2 result label is wrong", interrupt, check->name);
    assert_int_equals("2 result start is wrong", intervals[0].start, check->start);
    assert_int_equals("2 result end is wrong", intervals[5].end, check->end);
    map_get(&intervals[0].map, event, &expected_map);
    assert_int_equals("2 map value is wrong", expected_map.value.string, check_map.value.string);

    check = next_interval(&pit);
    map_get(&check->map, event, &check_map);
    assert_int_equals("3 result label is wrong", handled, check->name);
    assert_int_equals("3 result start is wrong", intervals[0].start, check->start);
    assert_int_equals("3 result end is wrong", intervals[5].end, check->end);
    map_get(&intervals[0].map, event, &expected_map);
    assert_int_equals("3 map value is wrong", expected_map.value.string, check_map.value.string);

    check = next_interval(&pit);
    map_get(&check->map, event, &check_map);
    assert_int_equals("4 result label is wrong", handler, check->name);
    assert_int_equals("4 result start is wrong", intervals[4].start, check->start);
    assert_int_equals("4 result end is wrong", intervals[6].end, check->end);
    map_get(&intervals[4].map, event, &expected_map);
    assert_int_equals("4 map value is wrong", expected_map.value.string, check_map.value.string);

    check = next_interval(&pit);
    map_get(&check->map, event, &check_map);
    assert_int_equals("5 result label is wrong", interrupt, check->name);
    assert_int_equals("5 result start is wrong", intervals[2].start, check->start);
    assert_int_equals("5 result end is wrong", intervals[7].end, check->end);
    map_get(&intervals[2].map, event, &expected_map);
    assert_int_equals("5 map value is wrong", expected_map.value.string, check_map.value.string);

    check = next_interval(&pit);
    map_get(&check->map, event, &check_map);
    assert_int_equals("6 result label is wrong", handled, check->name);
    assert_int_equals("6 result start is wrong", intervals[2].start, check->start);
    assert_int_equals("6 result end is wrong", intervals[7].end, check->end);
    map_get(&intervals[2].map, event, &expected_map);
    assert_int_equals("6 map value is wrong", expected_map.value.string, check_map.value.string);

    destroy_pool(&out);
    destroy_pool(&in);
    teardown();
    for (i = 0; i < 8; i++) {
        destroy_map(&intervals[i].map);
    }
}

void test_nfer_dsl(void) {
    label boot_s = 0, downlink = 1, boot_e = 2, boot = 3;
    word_id a = 10, d = 11, f = 12;
    map_key count = 2;
    map_value count_value, check_map_value;
    interval intervals[] = {
            { boot_s, 40, 40, EMPTY_MAP, false },
            { downlink, 41, 41, EMPTY_MAP, false },
            { boot_s, 42, 42, EMPTY_MAP, false },
            { boot_s, 44, 44, EMPTY_MAP, false },
            { boot_e, 46, 46, EMPTY_MAP, false },
            { downlink, 48, 48, EMPTY_MAP, false },
            { boot_e, 50, 50, EMPTY_MAP, false }
    };
    pool in, out;
    interval *check;
    int i;
    expression_input *where_clause, *begin_clause, *end_clause, *map_clause;

    setup();
    initialize_pool(&in);
    initialize_pool(&out);

    count_value.type = string_type;
    count_value.value.string = a;
    map_set(&intervals[2].map, count, &count_value);
    count_value.type = string_type;
    count_value.value.string = f;
    map_set(&intervals[0].map, count, &count_value);
    map_set(&intervals[4].map, count, &count_value);
    count_value.type = string_type;
    count_value.value.string = d;
    map_set(&intervals[3].map, count, &count_value);
    map_set(&intervals[6].map, count, &count_value);

    // add all the test intervals to the pool
    for (i = 0; i < 7; i++) {
        add_interval(&in, &intervals[i]);
        destroy_map(&intervals[i].map);
    }

    add_rule_to_specification(&spec, boot, boot_s, ALSO_OPERATOR, boot_e, NULL);
    // where lhs.end < rhs.begin & lhs.count = rhs.count
    initialize_expression_input(&where_clause, 10);
    where_clause[0].length = 10;
    where_clause[1].action = param_left_end;
    where_clause[2].action = param_right_begin;
    where_clause[3].action = action_lt;
    where_clause[4].action = param_left_field;
    where_clause[5].field_name = count;
    where_clause[6].action = param_right_field;
    where_clause[7].field_name = count;
    where_clause[8].action = action_eq;
    where_clause[9].action = action_and;
    spec.rules[0].where_expression = where_clause;

    initialize_expression_input(&begin_clause, 2);
    begin_clause[0].length = 2;
    begin_clause[1].action = param_left_begin;
    spec.rules[0].begin_expression = begin_clause;

    initialize_expression_input(&end_clause, 2);
    end_clause[0].length = 2;
    end_clause[1].action = param_right_end;
    spec.rules[0].end_expression = end_clause;

    initialize_expression_input(&map_clause, 3);
    map_clause[0].length = 3;
    map_clause[1].action = param_left_field;
    map_clause[2].field_name = count;
    count_value.type = pointer_type;
    count_value.value.pointer = map_clause;
    map_set(&spec.rules[0].map_expressions, count, &count_value);

    // actually run nfer
    run_nfer(&spec, &in, &out);

    assert_int_equals("result size is wrong", 2, out.size);
    i = 0;

    check = &out.intervals[i++].i;
    assert_int_equals("result label is wrong", boot, check->name);
    assert_int_equals("result start is wrong", intervals[0].start, check->start);
    assert_int_equals("result end is wrong", intervals[4].end, check->end);
    map_get(&check->map, count, &check_map_value);
    assert_int_equals("result map is wrong", f, check_map_value.value.string);

    check = &out.intervals[i++].i;
    assert_int_equals("result label is wrong", boot, check->name);
    assert_int_equals("result start is wrong", intervals[3].start, check->start);
    assert_int_equals("result end is wrong", intervals[6].end, check->end);
    map_get(&check->map, count, &check_map_value);
    assert_int_equals("result map is wrong", d, check_map_value.value.string);

    destroy_pool(&out);
    destroy_pool(&in);
    teardown();
}

static void setup_test_spec(pool *p) {
    label int_enter = 0, int_exit = 1, hand_enter = 2, hand_exit = 3,
          interrupt = 4, handler = 5, handled = 6, unhandled = 7;
    map_key string0 = 0, count1 = 1, count2 = 2, count3 = 3;
    map_value count_value;
    expression_input *map_clause1, *map_clause2, *where_clause1, *begin_clause, *end_clause;
    nfer_rule *rule1, *rule2, *erule;
    interval *i;
    map_value mval;

    // the phi function compares the 0th map value for string equality
    // the phi map function copies 0 from the left side to the result
    rule1 = add_rule_to_specification(&spec, interrupt, int_enter, BEFORE_OPERATOR, int_exit, &equal_phi);
    rule2 = add_rule_to_specification(&spec, handler, hand_enter, BEFORE_OPERATOR, hand_exit, NULL);

    add_rule_to_specification(&spec, handled, handler, SLICE_OPERATOR, interrupt, NULL);
    // exclusive rule
    erule = add_rule_to_specification(&spec, unhandled, interrupt, CONTAIN_OPERATOR, handled, NULL);
    erule->exclusion = true;

    // this will compare field 1 for integer equality
    initialize_expression_input(&where_clause1, 6);
    where_clause1[0].length = 6;
    where_clause1[1].action = param_left_field;
    where_clause1[2].field_name = count1;
    where_clause1[3].action = param_right_field;
    where_clause1[4].field_name = count1;
    where_clause1[5].action = action_eq;
    rule1->where_expression = where_clause1;

    initialize_expression_input(&map_clause1, 3);
    map_clause1[0].length = 3;
    map_clause1[1].action = param_left_field;
    map_clause1[2].field_name = count2;
    count_value.type = pointer_type;
    count_value.value.pointer = map_clause1;
    map_set(&rule1->map_expressions, count2, &count_value);

    initialize_expression_input(&map_clause2, 3);
    map_clause2[0].length = 3;
    map_clause2[1].action = param_left_field;
    map_clause2[2].field_name = count3;
    count_value.type = pointer_type;
    count_value.value.pointer = map_clause2;
    map_set(&rule2->map_expressions, count2, &count_value);

    initialize_expression_input(&begin_clause, 2);
    begin_clause[0].length = 2;
    begin_clause[1].action = param_left_end;
    rule2->begin_expression = begin_clause;

    initialize_expression_input(&end_clause, 2);
    end_clause[0].length = 2;
    end_clause[1].action = param_right_begin;
    rule2->end_expression = end_clause;

    // set up a pool - this is pretty verbose but whatever it is for testing
    if (p != NULL) {
        initialize_pool(p);
        i = allocate_interval(p);
        i->name = int_enter;
        i->start = 0;
        i->end = 1;
        mval.type = string_type;
        mval.value.string = 8;
        map_set(&i->map, string0, &mval);
        mval.type = integer_type;
        mval.value.integer = 3;
        map_set(&i->map, count1, &mval);
        mval.type = integer_type;
        mval.value.integer = 4;
        map_set(&i->map, count2, &mval);

        i = allocate_interval(p);
        i->name = int_exit;
        i->start = 4;
        i->end = 5;
        mval.type = string_type;
        mval.value.string = 8;
        map_set(&i->map, string0, &mval);
        mval.type = integer_type;
        mval.value.integer = 3;
        map_set(&i->map, count1, &mval);

        i = allocate_interval(p);
        i->name = hand_enter;
        i->start = 2;
        i->end = 2;
        mval.type = string_type;
        mval.value.string = 8;
        map_set(&i->map, string0, &mval);
        mval.type = integer_type;
        mval.value.integer = 2;
        map_set(&i->map, count1, &mval);
        mval.type = integer_type;
        mval.value.integer = 6;
        map_set(&i->map, count3, &mval);

        i = allocate_interval(p);
        i->name = hand_exit;
        i->start = 3;
        i->end = 3;
        mval.type = string_type;
        mval.value.string = 5;
        map_set(&i->map, string0, &mval);
        mval.type = integer_type;
        mval.value.integer = 3;
        map_set(&i->map, count1, &mval);
    }
}

void test_is_subscribed(void) {
    label l;
    setup();

    setup_test_spec(NULL);

    for (l = 0; l < 7; l++) {
        assert_true("Should subscribe", is_subscribed(&spec, l));
    }
    assert_false("Should not subscribe", is_subscribed(&spec, 7));
    assert_false("Should not subscribe (not in alphabet)", is_subscribed(&spec, 8));

    teardown();
}

void test_is_published(void) {
    label l;
    setup();

    setup_test_spec(NULL);

    for (l = 0; l < 4; l++) {
        assert_false("Should not publish", is_published(&spec, l));
    }
    for (l = 4; l < 8; l++) {
        assert_true("Should publish", is_published(&spec, l));
    }
    assert_false("Should not publish (not in alphabet)", is_published(&spec, 8));

    teardown();
}

void test_is_mapped(void) {
    setup();

    setup_test_spec(NULL);

    assert_false("Does not map 1", is_mapped(&spec, 1));
    assert_true("Maps 2", is_mapped(&spec, 2));
    assert_false("Does not map 3", is_mapped(&spec, 3));
    assert_false("Does not map 4 (not in alphabet)", is_mapped(&spec, 4));

    teardown();
}


void test_interval_match(void) {
    pool p;
    nfer_rule *rule1;
    interval *ien, *iex, *hen, *hex;

    setup();
    setup_test_spec(&p);
    rule1 = &spec.rules[0];
    ien = &p.intervals[0].i;
    iex = &p.intervals[1].i;
    hen = &p.intervals[2].i;
    hex = &p.intervals[3].i;

    // we have a conjunction of three Booleans to accept a match
    // failing any of the three tests should reject
    // Op /\ Phi /\ Where
    // so let's test TTT, FTT, TFT, TTF
    // it helps that interval_match does not test interval names, so we can pass whatever we like

    // TTT
    assert_true("Op, Phi, and Where are all true", interval_match(rule1, ien, iex));
    // FTT
    assert_false("Op is false", interval_match(rule1, iex, ien));
    // TFT
    assert_false("Phi is false", interval_match(rule1, ien, hex));
    // // TTF
    assert_false("Where is false", interval_match(rule1, ien, hen));

    destroy_pool(&p);
    teardown();
}

void test_set_end_times(void) {
    pool p;
    nfer_rule *rule1, *rule2;
    interval *ien, *iex, check;

    setup();
    setup_test_spec(&p);
    rule1 = &spec.rules[0];
    rule2 = &spec.rules[1];
    ien = &p.intervals[0].i;
    iex = &p.intervals[1].i;

    // here we just need to make sure that begin/end expressions override the operator

    // first test that the operator is used
    // make sure these are set to something we don't want
    check.start = 100;
    check.end = 100;
    set_end_times(rule1, ien, iex, &check);
    assert_int_equals("Begin should be set by operator", ien->start, check.start);
    assert_int_equals("End should be set by operator", iex->end, check.end);
    // then test that the begin/end expressions are used
    // make sure these are set to something we don't want
    check.start = 100;
    check.end = 100;
    set_end_times(rule2, ien, iex, &check);
    assert_int_equals("Begin should be set by expression", ien->end, check.start);
    assert_int_equals("End should be set by expression", iex->start, check.end);

    destroy_pool(&p);
    teardown();
}

void test_set_map(void) {
    pool p;
    nfer_rule *rule1, *rule2;
    interval *ien, *iex, *hen, *hex, check;
    map_key string0 = 0, count2 = 2;
    map_value mval;

    setup();
    setup_test_spec(&p);
    rule1 = &spec.rules[0];
    rule2 = &spec.rules[1];
    ien = &p.intervals[0].i;
    iex = &p.intervals[1].i;
    hen = &p.intervals[2].i;
    hex = &p.intervals[3].i;

    initialize_map(&check.map);

    // check that both phi functions and map expressions can be used to set map values
    // uses both!
    set_map(rule1, ien, iex, &check.map);
    assert_int_equals("Map space should be 3", 3, check.map.space);
    map_get(&check.map, string0, &mval);
    assert_int_equals("Phi should set string", string_type, mval.type);
    assert_int_equals("Phi should set 8", 8, mval.value.string);
    map_get(&check.map, count2, &mval);
    assert_int_equals("Expr should set int", integer_type, mval.type);
    assert_int_equals("Expr should set 4", 4, mval.value.string);

    destroy_map(&check.map);
    initialize_map(&check.map);

    // check that expr works without phi
    set_map(rule2, hen, hex, &check.map);
    assert_int_equals("Map space should be 3", 3, check.map.space);
    map_get(&check.map, count2, &mval);
    assert_int_equals("Expr should set int", integer_type, mval.type);
    assert_int_equals("Expr should set 6", 6, mval.value.string);

    destroy_map(&check.map);
    destroy_pool(&p);
    teardown();
}

void test_discard_older_events(void) {
    pool p;
    interval *ien, *hex;
    timestamp threshold;

    setup();
    setup_test_spec(&p);
    ien = &p.intervals[0].i;
    hex = &p.intervals[3].i;

    // we are testing essentially two things:
    // 1. that intervals are removed if they are too old
    // 2. that the pool is purged if enough intervals are removed

    // check that intervals are removed - we want to keep the number removed low to avoid triggering a purge
    // just try to remove one
    threshold = ien->end + 1;
    discard_older_events(&p, threshold);
    assert_int_equals("Threshold of 2 should have removed one interval", 1, p.removed);
    // now try removing all but one
    threshold = hex->end + 1;
    discard_older_events(&p, threshold);
    assert_int_equals("Purge should have been triggered with 75 percent removed", 0, p.removed);
    assert_int_equals("Threshold of 4 should have removed all but one interval", 1, p.size);

    destroy_pool(&p);
    teardown();
}


void test_exclusive_rule(void) {
    pool p, out;
    nfer_rule *erule;
    interval *ien, *iex, *hen, *hex;
    interval *inti, *hi;
    label interrupt = 4, handled = 6, unhandled = 7;

    setup();
    setup_test_spec(&p);
    erule = &spec.rules[3];
    ien = &p.intervals[0].i;
    iex = &p.intervals[1].i;
    hen = &p.intervals[2].i;
    hex = &p.intervals[3].i;

    initialize_pool(&out);

    // we just want to check that an exclusive rule matches when it should and does not when it shouldn't
    // first, this should not match because it does not include the interval the rule includes
    get_pool_queue(&p, &erule->input_queue, QUEUE_FROM_BEGINNING);
    apply_rule(erule, &erule->input_queue, &out, &spec.config, &spec.equivalent_labels);
    assert_int_equals("Exclusive rule should not have matched anything", 0, out.size);

    // now we need to add an interval that will cause the rule to not match
    // it has to be added here because this is where it would be added if the algorithm actually ran
    // however! we need to ignore it - so we'll update the queue after it's added to skip it
    hi = allocate_interval(&p);
    hi->name = handled;
    hi->start = hen->start;
    hi->end = hex->end;

    // skip the handled interval
    get_pool_queue(&p, &erule->input_queue, QUEUE_FROM_END);

    // now add an interval to match
    inti = allocate_interval(&p);
    inti->name = interrupt;
    inti->start = ien->start;
    inti->end = iex->end;

    apply_rule(erule, &erule->input_queue, &out, &spec.config, &spec.equivalent_labels);
    assert_int_equals("Exclusive rule should have matched", 1, out.size);
    assert_int_equals("Produced name is wrong", unhandled, out.intervals[out.start].i.name);
    assert_int_equals("Produced start is wrong", inti->start, out.intervals[out.start].i.start);
    assert_int_equals("Produced end is wrong", inti->end, out.intervals[out.start].i.end);

    // hack the rule and clear its produced cache so we don't need to worry about minimality being an issue
    clear_pool(&erule->produced);
    // clear the output pool
    clear_pool(&out);

    // now reset the input queue to the beginning
    get_pool_queue(&p, &erule->input_queue, QUEUE_FROM_BEGINNING);
    apply_rule(erule, &erule->input_queue, &out, &spec.config, &spec.equivalent_labels);
    assert_int_equals("Exclusive rule should not have matched anything", 0, out.size);

    destroy_pool(&out);
    destroy_pool(&p);
    teardown();
}

void test_atomic_rules(void) {
    pool p, out;
    interval *iex;
    label int_exit = 1, handled = 6;
    expression_input *map_clause1, *where_clause1;
    map_key count1 = 1;
    map_value count_value, check_value;
    nfer_rule *arule;

    setup();
    setup_test_spec(&p);
    iex = &p.intervals[1].i;

    // add an atomic rule
    arule = add_rule_to_specification(&spec, handled, int_exit, ALSO_OPERATOR, WORD_NOT_FOUND, NULL);

    // this will compare field 1 for integer equality with a constant value
    // it will also fail the first time because the value is wrong
    initialize_expression_input(&where_clause1, 6);
    where_clause1[0].length = 6;
    where_clause1[1].action = param_left_field;
    where_clause1[2].field_name = count1;
    where_clause1[3].action = param_intlit;
    where_clause1[4].integer_value = 2;
    where_clause1[5].action = action_eq;
    arule->where_expression = where_clause1;

    initialize_expression_input(&map_clause1, 3);
    map_clause1[0].length = 3;
    map_clause1[1].action = param_left_field;
    map_clause1[2].field_name = count1;
    count_value.type = pointer_type;
    count_value.value.pointer = map_clause1;
    map_set(&arule->map_expressions, count1, &count_value);

    initialize_pool(&out);
    get_pool_queue(&p, &arule->input_queue, QUEUE_FROM_BEGINNING);
    apply_rule(arule, &arule->input_queue, &out, &spec.config, &spec.equivalent_labels);
    assert_int_equals("Exclusive rule should not have matched anything", 0, out.size);

    // now change to match the same value as the existing interval
    where_clause1[4].integer_value = 3;
    get_pool_queue(&p, &arule->input_queue, QUEUE_FROM_BEGINNING);
    apply_rule(arule, &arule->input_queue, &out, &spec.config, &spec.equivalent_labels);
    assert_int_equals("Atomic rule should have matched", 1, out.size);
    assert_int_equals("Produced name is wrong", handled, out.intervals[out.start].i.name);
    assert_int_equals("Produced start is wrong", iex->start, out.intervals[out.start].i.start);
    assert_int_equals("Produced end is wrong", iex->end, out.intervals[out.start].i.end);
    map_get(&out.intervals[out.start].i.map, count1, &check_value);
    assert_int_equals("Atomic rule should set int in map", integer_type, check_value.type);
    assert_int_equals("Atomic rule should set 3 in map", 3, check_value.value.integer);

    destroy_pool(&out);
    destroy_pool(&p);
    teardown();
}

void test_apply_rule_list(void) {
    pool p, out;
    rule_id id;
    nfer_rule *rule;
    pool_iterator pit;
    interval *check;
    map_key mkey = 2;
    map_value mval;

    setup();
    setup_test_spec(&p);
    initialize_pool(&out);

    // this is really just to call apply_rule_list and make sure it produces what we expect
    // apply_rule_list handles most of the work of running nfer, so this is mostly just
    // checking that everything apppears that we expect, since the spec in question has no cycle

    // should produce
    // 4,0,5, 0 -> 8, 2 -> 4
    // 5,2,3, 2 -> 6
    // 6,2,3

    // first we have to initialize the input_queues
    for (id = 0; id < spec.size; id++) {
        rule = &spec.rules[id];
        get_pool_queue(&p, &rule->input_queue, QUEUE_FROM_BEGINNING);
    }

    // now call it - there are three rules just them all to make sure it properly handles a list
    apply_rule_list(&spec, 0, 2, &p, &out);

    // get_pool_iterator(&out, &pit);
    // while(has_next_interval(&pit)) {
    //     check = next_interval(&pit);
    //     log_interval(check);
    // }

    // check the output
    assert_int_equals("Input pool should be right size", 4+3, p.size);
    assert_int_equals("Output pool should be right size", 3, out.size);
    get_pool_iterator(&out, &pit);
    
    check = next_interval(&pit);
    assert_int_equals("Wrong name", 4, check->name);
    assert_int_equals("Wrong start", 0, check->start);
    assert_int_equals("Wrong end", 5, check->end);
    assert_int_equals("Wrong map space", 3, check->map.space);
    map_get(&check->map, 0, &mval);
    assert_int_equals("Wrong map type", string_type, mval.type);
    assert_int_equals("Wrong map value", 8, mval.value.string);
    map_get(&check->map, mkey, &mval);
    assert_int_equals("Wrong map type", integer_type, mval.type);
    assert_int_equals("Wrong map value", 4, mval.value.integer);

    check = next_interval(&pit);
    assert_int_equals("Wrong name", 5, check->name);
    assert_int_equals("Wrong start", 2, check->start);
    assert_int_equals("Wrong end", 3, check->end);
    assert_int_equals("Wrong map space", 3, check->map.space);
    map_get(&check->map, mkey, &mval);
    assert_int_equals("Wrong map type", integer_type, mval.type);
    assert_int_equals("Wrong map value", 6, mval.value.integer);

    check = next_interval(&pit);
    assert_int_equals("Wrong name", 6, check->name);
    assert_int_equals("Wrong start", 2, check->start);
    assert_int_equals("Wrong end", 3, check->end);
    assert_int_equals("Wrong map space", 0, check->map.space);

    destroy_pool(&out);
    destroy_pool(&p);
    teardown();
}

extern char test_log_buffer[];
extern int test_log_buffer_pos;
void test_write_rule(void) {
    dictionary name_dict, key_dict;
    nfer_rule *rule1, *rule2, *rule3, *erule;
    char *rule1str = "interrupt :- int_enter before int_exit phi EQUAL_EVENT where (int_enter.count1 = int_exit.count1) map { count2 -> int_enter.count2 }";
    // begin/end should come after map
    char *rule2str = "handler :- hand_enter before hand_exit map { count2 -> hand_enter.count3 } begin hand_enter.end end hand_exit.begin";
    char *rule3str = "handled :- handler slice interrupt";
    char *erulestr = "unhandled :- interrupt unless contain handled";

    // not going too in-depth here, but let's try to check something at least
    setup();
    setup_test_spec(NULL);

    rule1 = &spec.rules[0];
    rule2 = &spec.rules[1];
    rule3 = &spec.rules[2];
    erule = &spec.rules[3];

    initialize_dictionary(&name_dict);
    initialize_dictionary(&key_dict);

    // need labels up to 7, but val_dict refers to 8 (although it should not be read)
    // label int_enter = 0, int_exit = 1, hand_enter = 2, hand_exit = 3,
    //       interrupt = 4, handler = 5, handled = 6, unhandled = 7;
    add_word(&name_dict, "int_enter");
    add_word(&name_dict, "int_exit");
    add_word(&name_dict, "hand_enter");
    add_word(&name_dict, "hand_exit");
    add_word(&name_dict, "interrupt");
    add_word(&name_dict, "handler");
    add_word(&name_dict, "handled");
    add_word(&name_dict, "unhandled");
    add_word(&name_dict, "foobar");
    // map_key string0 = 0, count1 = 1, count2 = 2, count3 = 3;
    add_word(&key_dict, "string0");
    add_word(&key_dict, "count1");
    add_word(&key_dict, "count2");
    add_word(&key_dict, "count3");

    test_log_buffer_pos = 0;
    write_rule(rule1, &name_dict, &key_dict, &name_dict, WRITE_TESTING);
    assert_str_equals("Problem logging rule", rule1str, test_log_buffer);

    test_log_buffer_pos = 0;
    write_rule(rule2, &name_dict, &key_dict, &name_dict, WRITE_TESTING);
    assert_str_equals("Problem logging rule", rule2str, test_log_buffer);

    test_log_buffer_pos = 0;
    write_rule(rule3, &name_dict, &key_dict, &name_dict, WRITE_TESTING);
    assert_str_equals("Problem logging rule", rule3str, test_log_buffer);

    test_log_buffer_pos = 0;
    write_rule(erule, &name_dict, &key_dict, &name_dict, WRITE_TESTING);
    assert_str_equals("Problem logging rule", erulestr, test_log_buffer);

    destroy_dictionary(&key_dict);
    destroy_dictionary(&name_dict);

    teardown();
}
