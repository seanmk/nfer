/*
 * test_semantic.c
 *
 *  Created on: September 9, 2019
 *      Author: skauffma
 *
 *    nfer - a system for inferring abstractions of event streams
 *   Copyright (C) 2017  Sean Kauffman
 *
 *   This file is part of nfer.
 *   nfer is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "types.h"
#include "ast.h"
#include "semantic.h"
#include "dsl_test_helper.h"
#include "dsl.tab.h"
#include "test_semantic.h"
#include "test.h"
#include "log.h"

static mock_ast ast = { .a_str = "a", .b_str = "b", .c_str = "c", .d_str = "d", .x1_str = "x1",
    .foo_str = "foo", .bar_str = "bar", .x_str = "x", .y_str = "y", .boo_str = "boo" };

void test_set_field_mapping_per_rule(void) {
    map_key result;
    side_enum side;
    int dict_size;
    map_value value;
    word_id mapped_word;

    /* basic test: there is a rule with two BIEs, and the top level rule includes an expression which references the bottom one.
     * d :- a before (b before c) where a.foo = b.foo
     * What should happen: lower rule gets a mapping for b.foo to a new key and top rule gets set to reference it.
     */
    mock_parse(&ast);
    mock_determine_labels(&ast);
    mock_set_subfields(&ast);
    mock_determine_fields(&ast);

    // store key dict size
    dict_size = ast.key_dict.size;

    // setup does most of the work
    // call the function for a.foo
    set_field_mapping_per_rule(
            ast.a_before_x1, // the interval expression where the field will be used
            &ast.key_dict, // the key dictionary
            ast.a_lab,    // the label of the interval to reference
            ast.foo_key,  // the key of the field
            &result,  // where the field key should be stored
            &side,    // where the side of the key should be stored
            true,     // exclusion is okay because this is a where clause
            false     // always false when the function is called
            );

    // verify that the key_dict remains the same
    assert_int_equals("Key dict should not have been changed", dict_size, ast.key_dict.size);

    // verify the correct values for result and side
    assert_str_equals("Wrong key returned for a.foo field mapping", get_word(&ast.key_dict, ast.foo_key), get_word(&ast.key_dict, result));
    assert_int_equals("Wrong side returned for a.foo field mapping", left_side, side);

    // now do the b.foo field
    set_field_mapping_per_rule(
            ast.a_before_x1, // the interval expression where the field will be used
            &ast.key_dict, // the key dictionary
            ast.b_lab,    // the label of the interval to reference
            ast.foo_key,  // the key of the field
            &result,  // where the field key should be stored
            &side,    // where the side of the key should be stored
            true,     // exclusion is okay because this is a where clause
            false     // always false when the function is called
            );

    // verify that the key_dict increased by one
    assert_int_equals("Key dict should have been changed", dict_size + 1, ast.key_dict.size);

    // this is abusing the implementation.  We assume we know that the new key will be "F_foo-0".
    mapped_word = find_word(&ast.key_dict, "F_foo-0");
    // verify the correct values for result and side
    // use strings for better failure reporting
    assert_str_equals("Wrong key returned for b.foo field mapping", "F_foo-0", get_word(&ast.key_dict, result));
    assert_int_equals("Wrong side returned for b.foo field mapping", right_side, side);

    // we also need to check that the map was updated on the lower bie
    map_get(&ast.b_before_c->binary_interval_expr.left_field_map, mapped_word, &value);
    assert_int_equals("A mapping should be created for b.foo", string_type, value.type);
    assert_int_equals("foo field should be mapped on b before c", ast.foo_key, value.value.string);

    teardown_mock_ast(&ast);
}

void test_set_time_mapping_per_rule(void) {
    map_key result;
    side_enum side;
    int dict_size;
    bool is_time;
    word_id mapped_word;

    /* basic test: there is a rule with two BIEs, and the top level rule includes an expression which references the bottom one.
     * d :- a before (b before c) where a.bar = b.begin
     * What should happen: lower rule gets a mapping for b.begin to a new key and top rule gets set to reference it.
     */
    mock_parse(&ast);
    mock_determine_labels(&ast);
    mock_set_subfields(&ast);
    mock_determine_fields(&ast);

    // store key dict size
    dict_size = ast.key_dict.size;

    // setup does most of the work
    // do the b.begin time field
    set_time_mapping_per_rule(
            ast.a_before_x1, // the interval expression where the field will be used
            &ast.key_dict, // the key dictionary
            ast.b_lab,    // the label of the interval to reference
            &result,  // where the field key should be stored
            &side,    // where the side of the key should be stored
            &is_time, // returns if the time field is still a time field or not
            BEGINTOKEN, // the time field in question
            true,     // exclusion is okay because this is a where clause
            false     // always false when the function is called
            );

    // verify that the key_dict increased by one
    assert_int_equals("Key dict should have been changed", dict_size + 1, ast.key_dict.size);

    // this is abusing the implementation.  We assume we know that the new key will be F_BEGIN-0.
    mapped_word = find_word(&ast.key_dict, "F_BEGIN-0");
    // verify the correct values for result and side
    // use strings for better failure reporting
    assert_str_equals("Wrong key returned for b.begin time mapping", "F_BEGIN-0", get_word(&ast.key_dict, result));
    assert_int_equals("Wrong side returned for b.begin time mapping", right_side, side);
    // check is_time
    assert_false("Time field should not be time", is_time);

    // we also need to check that the map was created on the lower bie
    assert_int_equals("A mapping should be created for b.begin", mapped_word, ast.b_before_c->binary_interval_expr.left_begin_map);
    assert_int_equals("No mapping should be created for b.end", WORD_NOT_FOUND, ast.b_before_c->binary_interval_expr.left_end_map);
    assert_int_equals("No mapping should be created for c.begin", WORD_NOT_FOUND, ast.b_before_c->binary_interval_expr.right_end_map);
    assert_int_equals("No mapping should be created for c.end", WORD_NOT_FOUND, ast.b_before_c->binary_interval_expr.right_end_map);

    // try a somewhat more complex rule
    // (a.x & (b.y = c.y)) & (a.y = b.y)

    teardown_mock_ast(&ast);
}

void test_remap_field_or_time_mappings(void) {
    int dict_size;
    word_id mapped_word;
    map_value value;

    /* basic test: there is a rule with two BIEs, and the top level rule includes an expression which references the bottom one.
     * d :- a before (b before c) where a.foo = b.foo
     * What should happen: lower rule gets a mapping for b.foo to a new key and top rule gets set to reference it.
     */
    mock_parse(&ast);
    mock_determine_labels(&ast);
    mock_set_subfields(&ast);
    mock_determine_fields(&ast);

    // store key dict size
    dict_size = ast.key_dict.size;
    // call remap for the top level rule and the node that references its child
    remap_field_or_time_mappings(ast.a_before_x1, ast.b_foo, &ast.key_dict, true);

    // verify that the key_dict increased by one
    assert_int_equals("Key dict should have been changed", dict_size + 1, ast.key_dict.size);

    // check the results
    mapped_word = find_word(&ast.key_dict, "F_foo-0");
    assert_int_equals("Should have remapped b.foo to x1.F_foo-0", mapped_word, ast.b_foo->map_field.resulting_map_key);
    // we also need to check that the map was updated on the lower bie
    map_get(&ast.b_before_c->binary_interval_expr.left_field_map, mapped_word, &value);
    assert_int_equals("A mapping should be created for b.foo", string_type, value.type);
    assert_int_equals("foo field should be mapped on b before c", ast.foo_key, value.value.string);

    // call remap for the top level rule and the node that references its child
    remap_field_or_time_mappings(ast.a_before_x1, ast.b_begin, &ast.key_dict, true);

    // verify that the key_dict increased again
    assert_int_equals("Key dict should have been changed", dict_size + 2, ast.key_dict.size);

    // check the results
    mapped_word = find_word(&ast.key_dict, "F_BEGIN-0");
    // verify the correct values for result and side
    assert_int_equals("Should have remapped b.begin to x1.F_BEGIN-0", mapped_word, ast.b_begin->time_field.resulting_map_key);
    assert_false("Time field should not be time", ast.b_begin->time_field.is_time);

    // we also need to check that the map was created on the lower bie
    assert_int_equals("A mapping should be created for b.begin", mapped_word, ast.b_before_c->binary_interval_expr.left_begin_map);
    assert_int_equals("No mapping should be created for b.end", WORD_NOT_FOUND, ast.b_before_c->binary_interval_expr.left_end_map);
    assert_int_equals("No mapping should be created for c.begin", WORD_NOT_FOUND, ast.b_before_c->binary_interval_expr.right_end_map);
    assert_int_equals("No mapping should be created for c.end", WORD_NOT_FOUND, ast.b_before_c->binary_interval_expr.right_end_map);

    teardown_mock_ast(&ast);
}

void test_remap_nested_boolean(void) {
    int dict_size;

    /* test with a single nested boolean: there is a rule with two BIEs, and the only restriction is on the nested rule.
     * d :- a before (b before c) where b.boo
     * What should happen: the lower rule references the field and no remapping is performed.
     */
    mock_parse(&ast);
    mock_determine_labels(&ast);
    mock_set_subfields(&ast);
    mock_determine_fields(&ast);

    // store key dict size
    dict_size = ast.key_dict.size;
    // call remap for the top level rule and the node that references its child
    remap_field_or_time_mappings(ast.a_before_x1, ast.b_boo, &ast.key_dict, true);

    // verify that the key_dict did not change
    assert_int_equals("Key dict should not have been changed", dict_size, ast.key_dict.size);

    // check the results
    assert_int_equals("Should not have remapped b.boo", ast.boo_key, ast.b_boo->map_field.resulting_map_key);

    // we also need to check that the map on the lower bie was not changed
    assert_true("The map on b before c should not have been changed", is_map_empty(&ast.b_before_c->binary_interval_expr.left_field_map));

    teardown_mock_ast(&ast);
}

void test_remap_field_complex(void) {
    int dict_size;
    word_id mapped_word;
    map_value value;

    /* complex test: there's a rule with a complex where expr: (a.x & (b.y = c.y)) & (a.y = b.y)
     * What should happen: lower rule gets a mapping for b.y to a new key and only the correct field is set to reference it.
     * bies are traversed bottom-up, left-to-right
     * exprs are also traversed bottom-up, left-to-right
     */
    mock_parse(&ast);
    mock_determine_labels(&ast);
    mock_set_subfields(&ast);
    mock_determine_fields(&ast);

    // store key dict size
    dict_size = ast.key_dict.size;

    /*
     * order:
     * 1. check b_before_c with (b.y = c.y) -> both sides refer
     *   1a. left references exact, so skip
     *   1b. right references exact, so skip
     * 2. check b_before_c with (a_x & (b.y = c.y)) -> left doesn't refer
     * 3. check b_before_c with (a.y = b.y) -> left doesn't refer
     * 4. check b_before_c with (a.x & (b.y = c.y)) & (a.y = b.y) -> neither side refers
     * 5. check a_before_x1 with (b.y = c.y) -> reference already set
     * 6. check a_before_x1 with (a_x & (b.y = c.y)) -> refers
     *   6a. left references exact, so skip
     *   6b. right doesn't reference exact, so call --  it should do nothing because reference is set
     * 7. check a_before_x1 with (a.y = b.y) -> refers
     *   7a. left references exact, so skip
     *   7b. right doesn't reference exact, so call -- it should remap b.y
     * 8. check a_before_x1 with (a.x & (b.y = c.y)) & (a.y = b.y) -> neither side refers
     */

    // call remap for 6b.
    // this should result in nothing being remapped, since b.y = c.y is already mapped to a nested bie
    remap_field_or_time_mappings(ast.a_before_x1, ast.b_y_eq_c_y, &ast.key_dict, true);

    // verify that the key_dict did not increase
    assert_int_equals("Key dict should not have been changed", dict_size, ast.key_dict.size);

    // check the results
    assert_int_equals("Should not have remapped a.x", ast.x_key, ast.a_x->map_field.resulting_map_key);
    assert_int_equals("Should not have remapped b.y", ast.y_key, ast.b_y1->map_field.resulting_map_key);
    assert_int_equals("Should not have remapped c.y", ast.y_key, ast.c_y->map_field.resulting_map_key);

    // we also need to check that the map was left unchanged
    assert_true("Map should not have been altered", is_map_empty(&ast.b_before_c->binary_interval_expr.left_field_map));

    // call remap for 7b.
    // this should result in b.y being remapped for use in a before x1
    remap_field_or_time_mappings(ast.a_before_x1, ast.b_y2, &ast.key_dict, true);

    // verify that the key_dict was increased by one
    assert_int_equals("Key dict should have been added to", dict_size + 1, ast.key_dict.size);

    // check the results
    mapped_word = find_word(&ast.key_dict, "F_y-0");
    assert_int_equals("Should have remapped b.y to x1.F_y-0", mapped_word, ast.b_y2->map_field.resulting_map_key);
    // we also need to check that the map was updated on the lower bie
    map_get(&ast.b_before_c->binary_interval_expr.left_field_map, mapped_word, &value);
    assert_int_equals("A mapping should be created for b.y", string_type, value.type);
    assert_int_equals("y field should be mapped on b before c", ast.y_key, value.value.string);

    teardown_mock_ast(&ast);
}

void test_expr_references_ie(void) {
    mock_parse(&ast);
    mock_determine_labels(&ast);
    mock_set_subfields(&ast);
    mock_determine_fields(&ast);

    // test all combinations of expr_references_bie for the rule "a before (b before c) where a.foo = b.foo"
    assert_true("a.foo should reference a before x1", expr_references_ie(ast.a_before_x1, ast.a_foo));
    assert_true("b.foo should reference a before x1", expr_references_ie(ast.a_before_x1, ast.b_foo));
    assert_false("a.foo = b.foo should not reference a before x1", expr_references_ie(ast.a_before_x1, ast.a_foo_eq_b_foo));
    assert_false("a.foo should not reference b before c", expr_references_ie(ast.b_before_c, ast.a_foo));
    assert_true("b.foo should reference b before c", expr_references_ie(ast.b_before_c, ast.b_foo));
    assert_false("a.foo = b.foo should not reference b before c", expr_references_ie(ast.b_before_c, ast.a_foo_eq_b_foo));

    // test all combinations of expr_references_bie for the rule "a before (b before c) where a.bar = b.begin"
    assert_true("a.bar should reference a before x1", expr_references_ie(ast.a_before_x1, ast.a_bar));
    assert_true("b.begin should reference a before x1", expr_references_ie(ast.a_before_x1, ast.b_begin));
    assert_false("a.bar = b.begin should not reference a before x1", expr_references_ie(ast.a_before_x1, ast.a_bar_eq_b_begin));
    assert_false("a.bar should not reference b before c", expr_references_ie(ast.b_before_c, ast.a_bar));
    assert_true("b.begin should reference b before c", expr_references_ie(ast.b_before_c, ast.b_begin));
    assert_false("a.bar = b.begin should not reference b before c", expr_references_ie(ast.b_before_c, ast.a_bar_eq_b_begin));

    // test all combinations of expr_references_bie for the rule "a before (b before c) where (a.x & (b.y = c.y)) & (a.y = b.y)"
    assert_true("a.x should reference a before x1", expr_references_ie(ast.a_before_x1, ast.a_x));
    assert_true("b.y1 should reference a before x1", expr_references_ie(ast.a_before_x1, ast.b_y1));
    assert_true("c.y should reference a before x1", expr_references_ie(ast.a_before_x1, ast.c_y));
    assert_true("a.y should reference a before x1", expr_references_ie(ast.a_before_x1, ast.a_y));
    assert_true("b.y2 should reference a before x1", expr_references_ie(ast.a_before_x1, ast.b_y2));
    assert_true("b.y = c.y should reference a before x1", expr_references_ie(ast.a_before_x1, ast.b_y_eq_c_y));
    assert_false("a.x & (b.y = c.y) should not reference a before x1", expr_references_ie(ast.a_before_x1, ast.a_x_and_eq));
    assert_false("a.y = b.y should not reference a before x1", expr_references_ie(ast.a_before_x1, ast.a_y_eq_b_y));
    assert_false("(a.x & (b.y = c.y)) & (a.y = b.y) should not reference a before x1", expr_references_ie(ast.a_before_x1, ast.and_and_eq));

    assert_false("a.x should not reference b before c", expr_references_ie(ast.b_before_c, ast.a_x));
    assert_true("b.y1 should reference b before c", expr_references_ie(ast.b_before_c, ast.b_y1));
    assert_true("c.y should reference b before c", expr_references_ie(ast.b_before_c, ast.c_y));
    assert_false("a.y should not reference b before c", expr_references_ie(ast.b_before_c, ast.a_y));
    assert_true("b.y2 should reference b before c", expr_references_ie(ast.b_before_c, ast.b_y2));
    assert_true("b.y = c.y should reference b before c", expr_references_ie(ast.b_before_c, ast.b_y_eq_c_y));
    assert_false("a.x & (b.y = c.y) should not reference b before c", expr_references_ie(ast.b_before_c, ast.a_x_and_eq));
    assert_false("a.y = b.y should not reference b before c", expr_references_ie(ast.b_before_c, ast.a_y_eq_b_y));
    assert_false("(a.x & (b.y = c.y)) & (a.y = b.y) should not reference b before c", expr_references_ie(ast.b_before_c, ast.and_and_eq));

    // test all combinations of expr_references_bie for the rule "a before (b before c) where b.boo"
    assert_true("b.boo should reference a before x1", expr_references_ie(ast.a_before_x1, ast.b_boo));
    assert_true("b.boo should reference b before c", expr_references_ie(ast.b_before_c, ast.b_boo));

    teardown_mock_ast(&ast);
}

void test_set_map_boolean_type(void) {
    mock_parse(&ast);
    mock_determine_labels(&ast);

    // verify that all the is_subfield fields are set correctly

    // first for a.foo = b.foo
    set_map_boolean_type(ast.a_foo_eq_b_foo, false);
    // check the fields
    assert_true("a.foo is a subfield of a non-Boolean", ast.a_foo->map_field.is_non_boolean);
    assert_true("b.foo is a subfield of a non-Boolean", ast.b_foo->map_field.is_non_boolean);

    // then for a.bar = b.begin
    set_map_boolean_type(ast.a_bar_eq_b_begin, false);
    // check the fields
    assert_true("a.bar is a subfield of a non-Boolean", ast.a_bar->map_field.is_non_boolean);

    // then for (a.x & (b.y = c.y)) & (a.y = b.y)
    set_map_boolean_type(ast.and_and_eq, false);
    // check the fields
    assert_false("a.x is not a subfield of a non-Boolean", ast.a_x->map_field.is_non_boolean);
    assert_true("b.y1 is a subfield of a non-Boolean", ast.b_y1->map_field.is_non_boolean);
    assert_true("c.y is a subfield of a non-Boolean", ast.c_y->map_field.is_non_boolean);
    assert_true("a.y is a subfield of a non-Boolean", ast.a_y->map_field.is_non_boolean);
    assert_true("b.y2 is a subfield of a non-Boolean", ast.b_y2->map_field.is_non_boolean);

    // then for b.boo
    set_map_boolean_type(ast.b_boo, false);
    // check the fields
    assert_false("b.boo is not a subfield of a non-Boolean", ast.b_boo->map_field.is_non_boolean);

    teardown_mock_ast(&ast);
}

void test_set_imported(void) {
    mock_parse(&ast);

    // the mock parser sets up two modules and the first imports the second
    set_imported(ast.b_rule_module);

    assert_true("Imported should be set on first module", ast.d_rule_module->module_list.imported);
    assert_true("Imported should be set on imported module", ast.b_rule_module->module_list.imported);
    // reset
    ast.d_rule_module->module_list.imported = false;
    ast.b_rule_module->module_list.imported = false;

    // now remove the import list
    free_node(ast.imports);
    ast.d_rule_module->module_list.imports = NULL;

    // make sure only the first flag is set
    set_imported(ast.b_rule_module);
    assert_true("Imported should be set on first module", ast.d_rule_module->module_list.imported);
    assert_false("Imported should not be set", ast.b_rule_module->module_list.imported);
    // reset the true one
    ast.d_rule_module->module_list.imported = false;

    // finally check that the imported flag is set correctly with only one module

    set_imported(ast.d_rule_module);
    assert_true("Imported should be set with only one module", ast.d_rule_module->module_list.imported);

    teardown_mock_ast(&ast);
}
