/*
 * test_static.c
 *
 *  Created on: December 13, 2021
 *      Author: skauffma
 *
 *   nfer - a system for inferring abstractions of event streams
 *   Copyright (C) 2017  Sean Kauffman
 *
 *   This file is part of nfer.
 *   nfer is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdlib.h>

#include "types.h"
#include "ast.h"
#include "static.h"
#include "nfer.h"

#include "test_static.h"
#include "dsl.tab.h"
#include "dsl_test_helper.h"
#include "test.h"
#include "log.h"

static mock_ast ast = { .a_str = "a", .b_str = "b", .c_str = "c", .d_str = "d", .x1_str = "x1",
    .foo_str = "foo", .bar_str = "bar", .x_str = "x", .y_str = "y", .boo_str = "boo" };

static location_type EMPTY_LOCATION = {0, 0, 0, 0};
void test_check_computes_ts(void) {
    bool result;
    ast_node *one, *a_plus_one;
    setup_mock_ast(&ast);

    // the ast root has begin and end set using just inputs, so that's fine
    result = check_computes_ts(ast.root);
    assert_false("AST Root should not compute ts", result);

    // now change a rule to compute a new end point
    one = new_int_literal(1L, &EMPTY_LOCATION);
    a_plus_one = new_binary_expr(PLUS, ast.a_begin, one);
    a_plus_one->binary_expr.belongs_in = ast.a_before_x1;
    // update the end point
    ast.d_rule->rule.end_points->end_points.begin_expr = a_plus_one;
    // now it should be implicit instead of explicit finite
    result = check_computes_ts(ast.root);
    assert_true("Adding one should mean it computes a new ts", result);

    // clean up
    // put back the end points expr
    ast.d_rule->rule.end_points->end_points.begin_expr = ast.a_begin;
    // delete the nodes we allocated
    a_plus_one->binary_expr.left = NULL;
    free_node(a_plus_one);

    teardown_mock_ast(&ast);
}

void test_fold_constants(void) {
    typed_value result;
    ast_node *int_node7, *int_node9, *bool_node_t, *bool_node_f, *real_node_pi, *real_node_e;
    ast_node *bin_node_plus, *bin_node_times, *bin_node_and;

    // first, check for safety when trying to fold things that don't need folding
    setup_mock_ast(&ast);

    // try folding
    fold_constants(ast.root, &result);
    // check that the result is null
    assert_int_equals("Result should be null type", null_type, result.type);
    // try for a sub expression
    fold_constants(ast.and_and_eq, &result);
    // again it should be null
    assert_int_equals("Result should be null type", null_type, result.type);

    // tear it down
    teardown_mock_ast(&ast);

    // set up a mock AST with some constants that need folding
    // first, some constant values
    int_node7 = new_int_literal(7, &EMPTY_LOCATION);
    int_node9 = new_int_literal(9, &EMPTY_LOCATION);
    bool_node_t = new_boolean_literal(true, &EMPTY_LOCATION);
    bool_node_f = new_boolean_literal(false, &EMPTY_LOCATION);
    real_node_pi = new_float_literal(3.14, &EMPTY_LOCATION);
    real_node_e = new_float_literal(2.72, &EMPTY_LOCATION);

    // then join them
    bin_node_plus = new_binary_expr(PLUS, int_node7, int_node9);
    bin_node_times = new_binary_expr(MUL, real_node_e, real_node_pi);
    bin_node_and = new_binary_expr(AND, bool_node_t, bool_node_f);

    // check the constants
    fold_constants(int_node7, &result);
    assert_int_equals("Result should be int type", integer_type, result.type);
    assert_int_equals("Result value is wrong", 7, result.value.integer);
    fold_constants(int_node9, &result);
    assert_int_equals("Result should be int type", integer_type, result.type);
    assert_int_equals("Result value is wrong", 9, result.value.integer);

    fold_constants(bool_node_t, &result);
    assert_int_equals("Result should be bool type", boolean_type, result.type);
    assert_true("Result value is wrong", result.value.boolean);
    fold_constants(bool_node_f, &result);
    assert_int_equals("Result should be bool type", boolean_type, result.type);
    assert_false("Result value is wrong", result.value.boolean);

    fold_constants(real_node_pi, &result);
    assert_int_equals("Result should be real type", real_type, result.type);
    assert_float_equals("Result value is wrong", 3.14, result.value.real);
    fold_constants(real_node_e, &result);
    assert_int_equals("Result should be real type", real_type, result.type);
    assert_float_equals("Result value is wrong", 2.72, result.value.real);

    fold_constants(bin_node_plus, &result);
    assert_int_equals("Result should be int type", integer_type, result.type);
    assert_int_equals("Result should be the sum", 16, result.value.integer);
    fold_constants(bin_node_times, &result);
    assert_int_equals("Result should be real type", real_type, result.type);
    assert_float_equals("Result value is wrong", 8.5408, result.value.real);
    fold_constants(bin_node_and, &result);
    assert_int_equals("Result should be bool type", boolean_type, result.type);
    assert_false("Result value is wrong", result.value.boolean);
    // note that it will have freed the value nodes, but not the binary exprs
    free_node(bin_node_plus);
    free_node(bin_node_times);
    free_node(bin_node_and);
}

