/*
 * test_types.c
 *
 *  Created on: June 29, 2021
 *      Author: skauffma
 *
 *    nfer - a system for inferring abstractions of event streams
 *   Copyright (C) 2017  Sean Kauffman
 *
 *   This file is part of nfer.
 *   nfer is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "types.h"
#include "test.h"

void test_equals(void) {
    typed_value itype1, itype2, rtype1, rtype2, btype1, btype2, ptype1, ptype2, stype1, stype2, ntype;

    itype1.type = integer_type;
    itype1.value.integer = 1;
    itype2.type = integer_type;
    itype2.value.integer = 2;

    rtype1.type = real_type;
    rtype1.value.real = 1.1;
    rtype2.type = real_type;
    rtype2.value.real = 2.2;

    btype1.type = boolean_type;
    btype1.value.boolean = true;
    btype2.type = boolean_type;
    btype2.value.boolean = false;

    ptype1.type = pointer_type;
    ptype1.value.pointer = &ptype1;
    ptype2.type = pointer_type;
    ptype2.value.pointer = &ptype2;

    stype1.type = string_type;
    stype1.value.string = 1;
    stype2.type = string_type;
    stype2.value.string = 2;

    ntype.type = null_type;
    ntype.value.boolean = false;

    // check for equivalence between the same types
    assert_true("int typed values should be equal", equals(&itype1, &itype1));
    assert_false("int typed values should not be equal", equals(&itype1, &itype2));
    assert_true("real typed values should be equal", equals(&rtype1, &rtype1));
    assert_false("real typed values should not be equal", equals(&rtype1, &rtype2));
    assert_true("boolean typed values should be equal", equals(&btype1, &btype1));
    assert_false("boolean typed values should not be equal", equals(&btype1, &btype2));
    assert_true("pointer typed values should be equal", equals(&ptype1, &ptype1));
    assert_false("pointer typed values should not be equal", equals(&ptype1, &ptype2));
    assert_true("string typed values should be equal", equals(&stype1, &stype1));
    assert_false("string typed values should not be equal", equals(&stype1, &stype2));
    assert_true("null typed values should be equal", equals(&ntype, &ntype));

    // check that different types are not equal
    assert_false("different types should not be equal", equals(&itype1, &rtype1));
    assert_false("different types should not be equal", equals(&itype1, &btype1));
    assert_false("different types should not be equal", equals(&itype1, &ptype1));
    assert_false("different types should not be equal", equals(&itype1, &stype1));
    assert_false("different types should not be equal", equals(&itype1, &ntype));

    assert_false("different types should not be equal", equals(&rtype1, &btype1));
    assert_false("different types should not be equal", equals(&rtype1, &ptype1));
    assert_false("different types should not be equal", equals(&rtype1, &stype1));
    assert_false("different types should not be equal", equals(&rtype1, &ntype));

    assert_false("different types should not be equal", equals(&btype1, &ptype1));
    assert_false("different types should not be equal", equals(&btype1, &stype1));
    assert_false("different types should not be equal", equals(&btype1, &ntype));

    assert_false("different types should not be equal", equals(&ptype1, &stype1));
    assert_false("different types should not be equal", equals(&ptype1, &ntype));

    assert_false("different types should not be equal", equals(&stype1, &ntype));

    // check for null cases
    assert_true("null pointers should be equal", equals(NULL, NULL));
    assert_false("one null pointer should not be equal", equals(NULL, &itype2));
    assert_false("one null pointer should not be equal", equals(&itype2, NULL));
}

void test_real_equals(void) {
    // check that reals that are very close are still considered equal
    typed_value rtype1, rtype2;

    // this is a classic case
    rtype1.type = real_type;
    rtype1.value.real = 0.1 + 0.2;
    rtype2.type = real_type;
    rtype2.value.real = 0.3;

    // first, ensure that the two values aren't equal
    assert_false("The two floats should not be equal for the test", rtype1.value.real == rtype2.value.real);
    // now ensure that they are considered equal by our code
    assert_true("The two floats should be considered equal", equals(&rtype1, &rtype2));
}

void test_compare_typed_values(void) {
    typed_value left, right;

    // check that one being null makes it less
    left.type = null_type;
    left.value.boolean = false;
    assert_true("Left should be less because it is NULL", compare_typed_values(NULL, &left) < 0);
    assert_true("Right should be less because it is NULL", compare_typed_values(&left, NULL) > 0);

    // check that different types are unequal
    left.type = null_type;
    left.value.boolean = false;

    right.type = boolean_type;
    right.value.boolean = 0;
    assert_true("Left should be less than right (type)", compare_typed_values(&left, &right) < 0);
    assert_true("Left should be more than right (type)", compare_typed_values(&right, &left) > 0);

    right.type = integer_type;
    right.value.integer = 0;
    assert_true("Left should be less than right (type)", compare_typed_values(&left, &right) < 0);
    assert_true("Left should be more than right (type)", compare_typed_values(&right, &left) > 0);

    right.type = real_type;
    right.value.real = 0;
    assert_true("Left should be less than right (type)", compare_typed_values(&left, &right) < 0);
    assert_true("Left should be more than right (type)", compare_typed_values(&right, &left) > 0);

    right.type = string_type;
    right.value.string = 0;
    assert_true("Left should be less than right (type)", compare_typed_values(&left, &right) < 0);
    assert_true("Left should be more than right (type)", compare_typed_values(&right, &left) > 0);

    right.type = pointer_type;
    right.value.pointer = NULL;
    assert_true("Left should be less than right (type)", compare_typed_values(&left, &right) < 0);
    assert_true("Left should be more than right (type)", compare_typed_values(&right, &left) > 0);

    left.type = boolean_type;
    left.value.boolean = false;

    right.type = integer_type;
    right.value.integer = 0;
    assert_true("Left should be less than right (type)", compare_typed_values(&left, &right) < 0);
    assert_true("Left should be more than right (type)", compare_typed_values(&right, &left) > 0);

    right.type = real_type;
    right.value.real = 0;
    assert_true("Left should be less than right (type)", compare_typed_values(&left, &right) < 0);
    assert_true("Left should be more than right (type)", compare_typed_values(&right, &left) > 0);

    right.type = string_type;
    right.value.string = 0;
    assert_true("Left should be less than right (type)", compare_typed_values(&left, &right) < 0);
    assert_true("Left should be more than right (type)", compare_typed_values(&right, &left) > 0);

    right.type = pointer_type;
    right.value.pointer = NULL;
    assert_true("Left should be less than right (type)", compare_typed_values(&left, &right) < 0);
    assert_true("Left should be more than right (type)", compare_typed_values(&right, &left) > 0);

    left.type = integer_type;
    left.value.integer = 0;

    right.type = real_type;
    right.value.real = 0;
    assert_true("Left should be less than right (type)", compare_typed_values(&left, &right) < 0);
    assert_true("Left should be more than right (type)", compare_typed_values(&right, &left) > 0);

    right.type = string_type;
    right.value.string = 0;
    assert_true("Left should be less than right (type)", compare_typed_values(&left, &right) < 0);
    assert_true("Left should be more than right (type)", compare_typed_values(&right, &left) > 0);

    right.type = pointer_type;
    right.value.pointer = NULL;
    assert_true("Left should be less than right (type)", compare_typed_values(&left, &right) < 0);
    assert_true("Left should be more than right (type)", compare_typed_values(&right, &left) > 0);

    left.type = real_type;
    left.value.real = false;

    right.type = string_type;
    right.value.string = 0;
    assert_true("Left should be less than right (type)", compare_typed_values(&left, &right) < 0);
    assert_true("Left should be more than right (type)", compare_typed_values(&right, &left) > 0);

    right.type = pointer_type;
    right.value.pointer = NULL;
    assert_true("Left should be less than right (type)", compare_typed_values(&left, &right) < 0);
    assert_true("Left should be more than right (type)", compare_typed_values(&right, &left) > 0);

    left.type = string_type;
    left.value.string = false;

    right.type = pointer_type;
    right.value.pointer = NULL;
    assert_true("Left should be less than right (type)", compare_typed_values(&left, &right) < 0);
    assert_true("Left should be more than right (type)", compare_typed_values(&right, &left) > 0);

    // then check that the same type with different values are unequal
    left.type = boolean_type;
    left.value.boolean = false;
    right.type = boolean_type;
    right.value.boolean = true;
    assert_true("Left should be less than right (value)", compare_typed_values(&left, &right) < 0);
    assert_true("Left should be more than right (value)", compare_typed_values(&right, &left) > 0);

    left.type = integer_type;
    left.value.integer = 1;
    right.type = integer_type;
    right.value.integer = 2;
    assert_true("Left should be less than right (value)", compare_typed_values(&left, &right) < 0);
    assert_true("Left should be more than right (value)", compare_typed_values(&right, &left) > 0);

    left.type = real_type;
    left.value.real = 0.2;
    right.type = real_type;
    right.value.real = 0.3;
    assert_true("Left should be less than right (value)", compare_typed_values(&left, &right) < 0);
    assert_true("Left should be more than right (value)", compare_typed_values(&right, &left) > 0);

    left.type = string_type;
    left.value.string = 0;
    right.type = string_type;
    right.value.string = 3;
    assert_true("Left should be less than right (value)", compare_typed_values(&left, &right) < 0);
    assert_true("Left should be more than right (value)", compare_typed_values(&right, &left) > 0);

    left.type = pointer_type;
    left.value.pointer = NULL;
    right.type = pointer_type;
    right.value.pointer = &right;
    assert_true("Left should be less than right (value)", compare_typed_values(&left, &right) < 0);
    assert_true("Left should be more than right (value)", compare_typed_values(&right, &left) > 0);

    // then check that equal values are equal
    assert_int_equals("NULL values should be equal", 0, compare_typed_values(NULL, NULL));

    // values should not matter for null type
    left.type = null_type;
    left.value.boolean = false;
    right.type = null_type;
    right.value.boolean = true;
    assert_int_equals("Null types should always be equal", 0, compare_typed_values(&left, &right));

    // now set values to the same
    left.type = boolean_type;
    left.value.boolean = true;
    right.type = boolean_type;
    right.value.boolean = true;
    assert_int_equals("Equal values should be equal", 0, compare_typed_values(&left, &right));

    left.type = integer_type;
    left.value.integer = 5;
    right.type = integer_type;
    right.value.integer = 5;
    assert_int_equals("Equal values should be equal", 0, compare_typed_values(&left, &right));

    left.type = real_type;
    left.value.real = 0.2;
    right.type = real_type;
    right.value.real = 0.2;
    assert_int_equals("Equal values should be equal", 0, compare_typed_values(&left, &right));

    left.type = string_type;
    left.value.string = 3;
    right.type = string_type;
    right.value.string = 3;
    assert_int_equals("Equal values should be equal", 0, compare_typed_values(&left, &right));

    left.type = pointer_type;
    left.value.pointer = &left;
    right.type = pointer_type;
    right.value.pointer = &left;
    assert_int_equals("Equal values should be equal", 0, compare_typed_values(&left, &right));
}
