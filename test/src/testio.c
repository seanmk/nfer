/*
 * testio.c
 *
 *  Created on: June 29, 2021
 *      Author: skauffma
 *
 *    nfer - a system for inferring abstractions of event streams
 *   Copyright (C) 2017  Sean Kauffman
 *
 *   This file is part of nfer.
 *   nfer is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdlib.h>

/**
 * Very important!  Don't #include testio.h!!!
 * IF you do then there will be circular references around malloc/realloc/free.
 **/

// this is a global to store the total allocated space - for now all we do is track this
size_t allocated = 0;

void * test_malloc(size_t size) {
    size_t * returned;
    
    // call malloc and allocate extra space to store how big it is
    returned = (size_t *)malloc(size + sizeof(size_t));
    // store the size
    if (returned != NULL) {
        returned[0] = size;
        // add to the global allocation size
        allocated += size;
        // then return the space after the size
        return &returned[1];
    }
    return NULL;
}

void * test_realloc(void *p, size_t size) {
    size_t *original, *returned, prior_size;

    if (p != NULL) {
        // point to the original pointer returned by malloc
        original = &((size_t *)p)[-1];
        // store the old size
        prior_size = original[0];
        // realloc now
        returned = (size_t *)realloc(original, size + sizeof(size_t));
        if (returned != NULL) {
            // first update the allocated global
            allocated += (size - prior_size);
            // now store the new size
            returned[0] = size;
            // then return the space after the size
            return &returned[1];
        }
    }
    return NULL;
}

void test_free(void *p) {
    size_t *original;

    if (p != NULL) {
        // point to the original pointer returned by malloc
        original = &((size_t *)p)[-1];
        // reduce the allocated global
        allocated -= original[0];
        // free it
        free(original);
    }
}
